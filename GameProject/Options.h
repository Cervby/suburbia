#pragma once
#include "stdafx.h"
#include "GameMaster.h"

using namespace GameEngine;

class Options : public Master
{
	bool fromGame;
	GameData gameData;
	
	virtual void update() override;

public:
	Options(std::string lastMasterName);
	Options(std::string lastMasterName, GameData& gameData);
};