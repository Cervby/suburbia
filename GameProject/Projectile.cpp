#include "stdafx.h"
#include "Projectile.h"

using namespace GameEngine;

Projectile::Projectile(Vector2& toPos) : GameObject("Projectile")
{
	addSprite();
	addCollider();
	animatedSprite->setSprite("Bullet sprite sheet.png");
	animatedSprite->setAnimation(Vector2(64, 64), 0.1, 18);
	Vector2 pos = GameObject::find("Barrel")->getPosition();
	setPosition(pos);
	collider->set(ColliderType::COLLIDER_CIRCLE, Rect(-29, -30, 29, 41));
	collider->colliderTag = "Friendly";
	toDir = toPos - pos;
	deathCounter = 8;
	explodeCounter = 1;
	exploding = false;
	lookAt(toPos);
	setRotation(getRotation() + 180);
}

void Projectile::update()
{
	if (exploding)
	{
		explodeCounter -= Engine::smootherDeltaTime;
		if (collider != nullptr)
		{
			delete collider;
			collider = nullptr;
		}
	}
	else
	{
		moveDir(toDir, Engine::smootherDeltaTime * 2000);
		deathCounter -= Engine::smootherDeltaTime;
	}

	if (deathCounter <= 0 || explodeCounter <= 0)
	{
		destroy();
	}
}

Trail::Trail(Vector2& toPos) : GameObject("BulletTrail")
{
	animatedSprite->setSprite("BulletTrail.png");
	animatedSprite->setAnimation(Vector2(102, 241), 0.05, 2);
	setPosition(GameObject::find("Projectile")->getPosition());
	setRotation(GameObject::find("Projectile")->getRotation());
	Vector2 pos = GameObject::find("Barrel")->getPosition();
	toDir = toPos - pos;
	deathCounter = 8;
	explodeCounter = 1;
	exploding = false;
}
void Trail::onCollision(GameObject* other)
	
	{
		exploding = true;
		animatedSprite->setSprite("Explode_dynamic_test.png");
	};


void Trail::update()
{
	if (exploding)
	{
		explodeCounter -= Engine::smootherDeltaTime;
		if (collider != nullptr)
		{
			delete collider;
			collider = nullptr;
		}
	}
	else
	{
		moveDir(toDir, Engine::smootherDeltaTime * 2000);
		deathCounter -= Engine::smootherDeltaTime;
	}

	if (deathCounter <= 0 || explodeCounter <= 0)
	{
		destroy();
	}
}
