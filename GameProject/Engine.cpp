

#include "stdafx.h"

namespace GameEngine
{
	sf::Clock Engine::clock;
	uint32_t Engine::lastTime;
	std::deque<double> Engine::lastDeltaTimes;
	Settings Engine::settings;
	bool Engine::bMainGameLoop;
	double Engine::deltaTime;
	double Engine::smootherDeltaTime;
	double Engine::lastDrawTime;

	void Engine::update()
	{
		updateDeltaTime();
		InputManager::frameInputUpdate();
		Collider::updateCollisions();
		GameObject::updateGameObjects();
		GUI::update();
		if (lastDrawTime >= (1 / 60) * 1000)
		{
			lastDrawTime -= (1 / 60) * 1000;
			RenderManager::render();
		}
	}

	void Engine::updateDeltaTime()
	{
		uint32_t now = clock.getElapsedTime().asMilliseconds();
		if (now > lastTime)
		{
			deltaTime = static_cast<double>(now - lastTime) / 1000;
			if (lastDeltaTimes.size() >= 20)
			{
				lastDeltaTimes.pop_front();
			}
			lastDeltaTimes.push_back(deltaTime);
			double sum = 0;
			for (double num : lastDeltaTimes)
			{
				sum += num;
			}
			smootherDeltaTime = sum / lastDeltaTimes.size();
		}
		lastTime = now;
		lastDrawTime += deltaTime;
	}

	void Engine::setScale(Vector2& scale)
	{
		settings.scale = scale;
		settings.targetResolution = settings.logicalResolution;
	}


	void Engine::init(std::string title, Settings& ssettings)
	{
		Utility::init();
		settings = ssettings;
		if (settings.targetResolution == Vector2( 0, 0 ))
		{
			settings.targetResolution = settings.logicalResolution;

		}
		sf::ContextSettings contextSettings;
		contextSettings.antialiasingLevel = 8;

		RenderManager::window.create(sf::VideoMode(settings.targetResolution.x * settings.scale.x, settings.targetResolution.y * settings.scale.y), title, settings.fullscreen ? sf::Style::Fullscreen : sf::Style::Default, contextSettings);

		lastTime = clock.restart().asMilliseconds();

		RenderManager::window.setKeyRepeatEnabled(false);

		bMainGameLoop = true;

		AudioManager::init();

		deltaTime = 0;
		smootherDeltaTime = 0;

		GameObject::objectList = {};
	}

	void Engine::setFullscreen(bool set)
	{
		Settings settings = getSettings();
		sf::ContextSettings contextSettings;
		contextSettings.antialiasingLevel = 8;
		RenderManager::window.create(sf::VideoMode(settings.targetResolution.x * settings.scale.x, settings.targetResolution.y * settings.scale.y), settings.title, set ? sf::Style::Fullscreen : sf::Style::Default, contextSettings);
	}


	void Engine::run()
	{
		while (bMainGameLoop) {
			update();
		}
		update();
		quit();
	}

	void Engine::quit()
	{
		AudioManager::quit();
		//GameObject::quit(); //extra cleanup that shouldn't be necessary if all is well.
		AnimatedSprite::quit();
		InputManager::quit();
		GUI::quit();
	}
}
