#pragma once

#include "stdafx.h"

using namespace GameEngine;


class PickUp : public GameObject
{
private:
	PlayerPowerUp powerUp;
	double time;
	
	bool gottenCancel;
	int alpha;
	Vector2 position;


	virtual void update() override;

public:
	bool gotten;
	PickUp(Vector2& startPos, PlayerPowerUp powerUp);
};
