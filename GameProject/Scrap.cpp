#include "stdafx.h"

using namespace GameEngine;

Scrap::Scrap() : GameObject("Scrap")
{
	addSprite();
	setPosition(Vector2(Utility::getRandInt(0, Engine::getLogicalResolution().x), Utility::getRandInt(0, Engine::getLogicalResolution().y)));
	setRotation(Utility::getRandInt(0, 360));
	rotationalFactor = Utility::getRandInt(-10, 10);
	movementFactor = Vector2(Utility::getRandInt(-10, 10), Utility::getRandInt(-10, 10)) / 10;
	switch (Utility::getRandInt(0, 8))
	{
	case 0:
		animatedSprite->setSprite("scrap.png");
		break;
	case 1:
		animatedSprite->setSprite("scrap2.png");
		break;
	case 2:
		animatedSprite->setSprite("Metall.png");
		break;
	case 3:
		animatedSprite->setSprite("scraps.png");
		break;
	case 4:
		animatedSprite->setSprite("brokencog.png");
		break;
	case 5:
		animatedSprite->setSprite("window.png");
		break;
	case 6:
		animatedSprite->setSprite("skruv.png");
		break;
	case 7:
		animatedSprite->setSprite("screw2.png");
		break;
	case 8:
		animatedSprite->setSprite("cord.png");
		break;
	}
	animatedSprite->setZIndex(0.5);
}

void Scrap::update()
{
	setRotation(getRotation() + rotationalFactor * Engine::smootherDeltaTime);
	setPosition(getPosition() + movementFactor * Engine::smootherDeltaTime * 10);
	if (getPosition().x > Engine::getLogicalResolution().x)
	{
		setPosition(Vector2{ getPosition().x - Engine::getLogicalResolution().x, getPosition().y });
	}
	else if (getPosition().y > Engine::getLogicalResolution().y)
	{
		setPosition(Vector2{ getPosition().x, getPosition().y - Engine::getLogicalResolution().y });
	}
	else if (getPosition().x < 0)
	{
		setPosition(Vector2{ getPosition().x + Engine::getLogicalResolution().x, getPosition().y });
	}
	else if (getPosition().y < 0)
	{
		setPosition(Vector2{ getPosition().x, getPosition().y + Engine::getLogicalResolution().y });
	}
}