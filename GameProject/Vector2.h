

#pragma once

#include "stdafx.h"



namespace GameEngine
{
	class Vector2
	{
	public:
		double x, y;

		Vector2(double x, double y);
		Vector2();
		Vector2(const Vector2 &v2);

		std::string to_str();
		Vector2 normalize();
		double magnitude(Vector2& v2);
		double distance(Vector2& v2); //magnitude and distance are the same.
		double dotProduct(Vector2& v2);
		void setPositionAround(Vector2 pos, const double distance, const double angle);
		void setPositionAround(Vector2 pos, const double angle);
		static Vector2 lerp(Vector2 start, Vector2 end, const double t);
		double Vector2::lookAt(Vector2 v2);
		sf::Vector2f to_sfF();
		sf::Vector2i to_sfI();
		static Vector2 from_sfF(sf::Vector2f v2f);
		static Vector2 from_sfI(sf::Vector2i v2i);
		static Vector2 from_double(const double otherDouble);

		Vector2 operator=(Vector2& v2);
		Vector2 operator+(Vector2& v2);
		Vector2 operator-(Vector2& v2);
		Vector2 operator*(Vector2& v2);
		Vector2 operator/(Vector2& v2);
		Vector2 operator%(Vector2& v2);
		Vector2& operator+=(Vector2& v2);
		Vector2& operator-=(Vector2& v2);
		Vector2& operator*=(Vector2& v2);
		Vector2& operator/=(Vector2& v2);
		Vector2& operator%=(Vector2& v2);
		Vector2 operator-();
		bool operator==(Vector2& v2);
		bool operator!=(Vector2& v2);
		Vector2 operator*(const double& otherDouble);
		Vector2 operator/(const double& otherDouble);
		Vector2 operator+(const double& otherDouble);
		Vector2 operator-(const double& otherDouble);
		Vector2& operator*=(const double& otherDouble);
		Vector2& operator/=(const double& otherDouble);
		Vector2& operator+=(const double& otherDouble);
		Vector2& operator-=(const double& otherDouble);
		Vector2& operator%=(const double& otherDouble);
		bool operator<(Vector2& v2);
		bool operator<(const double& otherDouble);
	};
}
