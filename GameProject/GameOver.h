#pragma once
#include "stdafx.h"

using namespace GameEngine;

class GameOver : public Master, public SaveScore
{
	unsigned long long score;

	virtual void update() override;

public:
	GameOver(std::string lastMasterName, unsigned long long score);
};
