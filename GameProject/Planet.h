#pragma once

#include "stdafx.h"

#define MAXPLAYERHEALTH 40

using namespace GameEngine;

class Planet : public GameObject
{
private:
	
	double dyingCounter;
	virtual void update() override;
	virtual void onCollision(GameObject* other) final;
public:
	bool isDying;
	int health;
	std::array<unsigned long long, 4> sectorHealth;
	Planet();
};

class PlanetShieldCollision : GameObject
{
private:
	virtual void onOverlap(GameObject* other) final;
public:
	PlanetShieldCollision(Vector2& planetPos);

};

class ShieldSparks : GameObject
{
private:
	virtual void update() override;
	double Counter;
public:
	ShieldSparks(GameObject* other);
};
