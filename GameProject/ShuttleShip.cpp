#include "stdafx.h"

using namespace GameEngine;

ShuttleShip::ShuttleShip(Vector2& toPos, std::string spriteName, int health) : AlienShip(toPos, spriteName, health)
{
	this->health = health;
	escapePodTimer = 0;
}

void ShuttleShip::shipMovement()
{


	deathCounter = 5;
	if (health == 2)
	{
		animatedSprite->setSprite("Shuttleship damaged state 1 256.png");
	}
	else if (health == 1)
	{
		animatedSprite->setSprite("Shuttleship damaged state 2 256.png");
	}

	escapePodTimer += Engine::smootherDeltaTime * SlowTime::speedFactor;

	moveTo(toPos, Engine::smootherDeltaTime * SlowTime::speedFactor * 7);

	Angle += Engine::smootherDeltaTime * SlowTime::speedFactor * 20;
	setPositionAround(toPos, getPosition().distance(toPos), Angle);


	if (escapePodTimer >= 2)
	{
		escapePodTimer = 0;


		new EscapePod(Vector2(Angle, getPosition().distance(toPos)), "Escapepod_85.png", 1);
	}
	if (EMPcounter <= 1.7)
	{
		animatedSprite->setSprite("ShuttleShipSparks.png");
		animatedSprite->setAnimation(Vector2(200, 200), 0.06, 10);
	}

	else if (EMPcounter <= 1.7 && health == 2)
	{
		animatedSprite->setSprite("ShuttleshipDamageState1Sparks.png.png");
		animatedSprite->setAnimation(Vector2(200, 200), 0.06, 10);
	}

	else if (EMPcounter <= 1.7 && health == 1)
	{
		animatedSprite->setSprite("ShuttleshipDamageState2Sparks.png.png");
		animatedSprite->setAnimation(Vector2(200, 200), 0.06, 10);
	}

}

void ShuttleShip::deathMovement()
{

	moveTo(toPos, Engine::smootherDeltaTime * SlowTime::speedFactor * 300);

	setPositionAround(toPos, getPosition().distance(toPos), Angle);

	if (InputManager::getMousePositionFixed().x < Engine::getLogicalResolution().x / 2)
	{
		Angle += Engine::smootherDeltaTime * SlowTime::speedFactor * 20;

	}
	else {
		Angle -= Engine::smootherDeltaTime * SlowTime::speedFactor * 20;

	}


	if (deathAnimationTrigger == 0)
	{
		WhichSounds = 1;

		if (WhichSounds == 1)
		{
			AudioManager::playSound("shuttleship expl.wav");
		}
		deathAnimationTrigger = true;
		animatedSprite->setSprite("ShuttleshipDeathSpritesheet.png");
		animatedSprite->setAnimation(Vector2(550, 360), 0.05, 9);
	}
}
