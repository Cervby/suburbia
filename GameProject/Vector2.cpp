

#include "stdafx.h"
#include "Vector2.h"

namespace GameEngine
{
	Vector2::Vector2(double x, double y) : x(x), y(y) { }

	Vector2::Vector2() : x(0), y(0) { }

	Vector2::Vector2(const Vector2 &v2) : x(v2.x), y(v2.y) { }

	std::string Vector2::to_str() 
	{
		std::string Xstr = std::to_string(x);
		Xstr.erase(Xstr.find_last_not_of('0') + 1, std::string::npos);
		std::string::iterator it = Xstr.end() - 1;
		if (*it == '.')
		{
			Xstr.erase(it);
		}
		std::string Ystr = std::to_string(y);
		Ystr.erase(Ystr.find_last_not_of('0') + 1, std::string::npos);
		it = Ystr.end() - 1;
		if (*it == '.')
		{
			Ystr.erase(it);
		}

		return "{" + Xstr + ", " + Ystr + "}";
	}

	Vector2 Vector2::normalize() 
	{
		if (x == 0 && y == 0)
		{
			return Vector2(0, 0);
		}
		double z = sqrt(pow(x,2) + pow(y,2));
		return Vector2(x/z, y/z);
	}

	double Vector2::magnitude(Vector2& v2) 
	{
		return sqrt(pow(x- v2.x, 2) + pow(y - v2.y, 2));
	}

	double Vector2::distance(Vector2& v2) 
	{
		return sqrt(pow(x - v2.x, 2) + pow(y - v2.y, 2));
	}

	double Vector2::dotProduct(Vector2& v2) //I'm not sure if I really get dot product.
	{
		std::vector<double> a = { x, y };
		std::vector<double> b = { v2.x, v2.y };
		return std::inner_product(std::begin(a), std::end(a), std::begin(b), .0/*I don't know! this is confusing.*/);
	}

	void Vector2::setPositionAround(Vector2 pos, double distance, double angle)
	{
		angle *= (M_PI / 180);
		x = pos.x + cos(angle) * distance;
		y = pos.y + -sin(angle) * distance;
	}

	void Vector2::setPositionAround(Vector2 pos, double angle)
	{
		angle *= (M_PI / 180);
		double distance = magnitude(pos);
		x = pos.x + cos(angle) * distance;
		y = pos.y + -sin(angle) * distance;
	}

	Vector2 Vector2::lerp(Vector2 start, Vector2 end, double t)
	{
		return (Vector2(1, 1) - t) * start + end * t;
	}
	double Vector2::lookAt(Vector2 v2) 
	{
		Vector2 delta = { x - v2.x, v2.y - y };
		return atan2(delta.x, delta.y) * (180 / M_PI);
	}

	sf::Vector2f Vector2::to_sfF() 
	{
		return sf::Vector2f( x, y );
	}


	sf::Vector2i Vector2::to_sfI() 
	{
		return sf::Vector2i(x, y);
	}
	Vector2 Vector2::from_sfF(sf::Vector2f v2f)
	{
		return Vector2(v2f.x, v2f.y);
	}

	Vector2 Vector2::from_sfI(sf::Vector2i v2i)
	{
		return Vector2(v2i.x, v2i.y);
	}

	Vector2 Vector2::from_double(double otherDouble)
	{
		return Vector2(otherDouble, otherDouble);
	}

	Vector2 Vector2::operator=(Vector2& v2)
	{
		x = v2.x;
		y = v2.y;
		return *this;
	}

	Vector2 Vector2::operator+(Vector2& v2) 
	{
		return { x + v2.x, y + v2.y };
	}

	Vector2 Vector2::operator-(Vector2& v2) 
	{
		return { x - v2.x, y - v2.y };
	}

	Vector2 Vector2::operator*(Vector2& v2) 
	{
		return { x * v2.x, y * v2.y };
	}

	Vector2 Vector2::operator/(Vector2& v2) 
	{
		return { x / v2.x, y / v2.y };
	}

	Vector2 Vector2::operator%(Vector2& v2) 
	{
		return{ fmod(x, v2.x), fmod(y, v2.y) };
	}

	Vector2& Vector2::operator+=(Vector2& v2)
	{
		x += v2.x;
		y += v2.y;
		return *this;
	}

	Vector2& Vector2::operator-=(Vector2& v2)
	{
		x -= v2.x;
		y -= v2.y;
		return *this;
	}

	Vector2& Vector2::operator*=(Vector2& v2)
	{
		x *= v2.x;
		y *= v2.y;
		return *this;
	}

	Vector2& Vector2::operator/=(Vector2& v2)
	{
		x /= v2.x;
		y /= v2.y;
		return *this;
	}

	Vector2& Vector2::operator%=(Vector2& v2)
	{
		x = fmod(x, v2.x);
		y = fmod(y, v2.y);
		return *this;
	}

	Vector2 Vector2::operator-() 
	{
		return { -x, -y };
	}

	bool Vector2::operator==(Vector2& v2) 
	{
		return x == v2.x && y == v2.y;
	}

	bool Vector2::operator!=(Vector2& v2) 
	{
		return !(x == v2.x && y == v2.y);
	}

	Vector2 Vector2::operator*(const double& other)
	{
		return { x * other, y * other };
	}

	Vector2 Vector2::operator/(const double& otherDouble)
	{
		return { x / otherDouble, y / otherDouble };
	}

	Vector2 Vector2::operator+(const double& other)
	{
		return{ x + other, y + other };
	}

	Vector2 Vector2::operator-(const double& otherDouble)
	{
		return{ x - otherDouble, y - otherDouble };
	}

	Vector2& Vector2::operator*=(const double& otherDouble)
	{
		x *= otherDouble;
		y *= otherDouble;
		return *this;
	}

	Vector2& Vector2::operator/=(const double& otherDouble)
	{
		x /= otherDouble;
		y /= otherDouble;
		return *this;
	}

	Vector2& Vector2::operator+=(const double& otherDouble)
	{
		x += otherDouble;
		y += otherDouble;
		return *this;
	}

	Vector2& Vector2::operator-=(const double& otherDouble)
	{
		x -= otherDouble;
		y -= otherDouble;
		return *this;
	}

	Vector2& Vector2::operator%=(const double& otherDouble)
	{
		x = fmod(x, otherDouble);
		y = fmod(y, otherDouble);
		return *this;
	}

	bool Vector2::operator<(Vector2& v2) 
	{
		return x < v2.x && y < v2.y;
	}

	bool Vector2::operator<(const double& otherDouble)
	{
		return x < otherDouble && y < otherDouble;
	}
}
