

#include "stdafx.h"
#include "RenderManager.h"

namespace GameEngine
{
	std::multimap<float, GameObject*> RenderManager::renderOrder;//Maps & multimaps are automatically sorted.
	sf::RenderWindow RenderManager::window;
	std::vector<sf::RectangleShape> RenderManager::debugSprites;

	void RenderManager::render()
	{
		window.clear();
		for (auto item : renderOrder)
		{
			if (item.second == nullptr)
			{
				std::cout << "error";
			}
			AnimatedSprite* animatedSprite = item.second->animatedSprite;
			if (animatedSprite != nullptr)
			{
				animatedSprite->render();
			}
		}
		GUI::renderGUI();
		for (auto item : renderOrder)
		{
			AnimatedSprite* animatedSprite = item.second->animatedSprite;
			if (animatedSprite != nullptr && animatedSprite->getAboveAll())
			{
				animatedSprite->render();
			}
		}
	
		window.display();
		GUI::clear();
		debugSprites.clear();
	}
}
