#include "stdafx.h"

using namespace GameEngine;

GameOver::GameOver(std::string lastMasterName, unsigned long long score) : Master("Game Over"), score(score)
{
	AudioManager::stopMusic("gameplaytheme tempo.ogg");
	AudioManager::stopMusic("gameplaytheme.ogg");
	AudioManager::playMusic("end theme FINAL.ogg");
	GUI::setStyle("null.png", "null.png", "null.png", "null.png", "ethnocentric rg.ttf");
	for (GameObject* gameObject : GameObject::objectList)
	{
		if (gameObject->name != "Game Over" && gameObject->name != "Cursor")
		{
			gameObject->destroy();
		}
	}
}

void GameOver::update()
{
	GUI::image(GUIImage{ "GameOverScreen.png", Vector2(0, 0)});
	SaveScore::savescore(score);
}