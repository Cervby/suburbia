#include "stdafx.h"

using namespace GameEngine;

double SlowTime::speedFactor;

SlowTime::SlowTime(Vector2& toPos, Camera& camera) : GameObject("SlowTime"), camera(camera)
{
	addSprite();
	animatedSprite->setSprite("slowtimeanimationspritesheet.png");
	animatedSprite->setAnimation(Vector2(1024, 1024), 0.1, 32);
	Vector2 pos = getPosition();
	setPosition(pos);

	toDir = toPos - pos;
	cooldown = 60;
	deathCounter = 5;
	explodeCounter = 0;
	pitch = 1;
	exploding = false;
	speedFactor = 1;
	setPositionAround(GameObject::find("Planet")->getPosition(), 1200, camera.currentRotation + 45);
	lookAt(GameObject::find("Planet")->getPosition());
}

void SlowTime::update()
{
	setPositionAround(GameObject::find("Planet")->getPosition(), 1200, camera.currentRotation + 45);
	lookAt(GameObject::find("Planet")->getPosition());

		
	deathCounter -= Engine::smootherDeltaTime;
		

	if (deathCounter <= 3)
	{
		pitch = Utility::lerp(pitch, 1, Engine::smootherDeltaTime * 3);
		speedFactor = Utility::lerp(speedFactor, 1, Engine::smootherDeltaTime);
	}
	else
	{
		pitch = Utility::lerp(pitch, 0.3, Engine::smootherDeltaTime * 3);
		speedFactor = Utility::lerp(speedFactor, 0.05, Engine::smootherDeltaTime);
	}
	AudioManager::pitchAll(pitch);

	if (deathCounter <= 0)
	{
		destroy();
	}
};