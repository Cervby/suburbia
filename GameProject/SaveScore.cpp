#include "stdafx.h"

using namespace GameEngine;

std::string SaveScore::input;

void SaveScore::savescore(unsigned long long score)
{
	char inpCh = InputManager::getText();
	if (inpCh != '\0' && std::string("QWERTYUIOP�ASDFGHJKL��ZXCVBNMqwertyuiop�asdfghjkl��zxcvbnm\b").find(inpCh) != std::string::npos)
	{
		if (inpCh == '\b')
		{
			input = input.substr(0, input.size() - 1);
		}
		else if (input.length() < 3)
		{
			input += inpCh;
		}
	}
	GUI::image(GUIImage{ "null.png", Rect(0, Engine::getLogicalResolution().y / 4, Engine::getLogicalResolution().x, Engine::getLogicalResolution().y), "Your score is\n" + std::to_string(score) });
	GUI::image(GUIImage{ "null.png", Rect(0, 0, Engine::getLogicalResolution().x, Engine::getLogicalResolution().y), "Type your initials:\n" + input });

	sf::Event::KeyEvent ekevnt;
	ekevnt.code = sf::Keyboard::Return;
	if (((InputManager::getKeyDown(ekevnt) || InputManager::getButtonDown("Fire")) && input.length() >= 2) == false)
	{
		return;
	}

	if (input.length() == 2)
	{
		input.append(" ");
	}

	std::fstream file;
	file.open("../assets/HIGHSCORE", std::ios::in);
	std::multimap<unsigned long long, std::string> scores;
	if (file.good())
	{
		std::string line;
		scores.insert(std::pair<unsigned long long, std::string>(score, input));
		while (std::getline(file, line))
		{
			scores.insert(std::pair<unsigned long long, std::string>(std::stoull(line.substr(0, line.size())), line.substr(line.size() - 3, 3)));
		}
		file.close();
	}
	std::fstream file2;
	file2.open("../assets/HIGHSCORE", std::ios::out | std::fstream::trunc);
	if (file2.good())
	{
		std::multimap<unsigned long long, std::string>::reverse_iterator rit;
		for (rit = scores.rbegin(); rit != scores.rend(); ++rit)
		{
			file2 << std::to_string(rit->first) << " " << rit->second << std::endl;
		}
		file2.close();
	}
	score = 0;
	input = "";
	if (Master::master->name == "Victory Screen")
	{
		static_cast<President*>(GameObject::find("President"))->moveOut = true;
	}
	else
	{
		Master::master->changeMaster<MainMenu>();
	}

}
