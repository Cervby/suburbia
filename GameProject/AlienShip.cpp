#include "stdafx.h"
#include "AlienShip.h"

using namespace GameEngine;

AlienShip::AlienShip(Vector2& toPos, std::string spriteName, int health, PlayerPowerUp powerUp) : GameObject("AlienShip")
{
	addCollider();
	addSprite();

	animatedSprite->setSprite(spriteName);
	setPositionAround(GameObject::find("Planet")->getPosition(), toPos.y, toPos.x);
	static_cast<Minimap*>(GameObject::find("Minimap"))->spawnPositions.push_back(getPosition());

	collider->set(ColliderType::COLLIDER_CIRCLE);
	collider->radius /= 2;

	deathCounter = 0.5;
	explodingCounter = 0.5;
	Angle = toPos.x;
	Ypos = toPos.y;
	WhichSounds = 0;
	EnemyHit = false;
	exploding = false;
	rotation = 0;
	this->health = health;
	CollPos = Vector2();
	deathSource = AlienShipDeathSource::ALIENSHIPDEATHSOURCE_INVALID;

	this->PowerUp = powerUp;
	yAcc = 80;
	alpha = 255;
	EMPcounter = 2;
	checkedForShieldCollision = false;
}

void AlienShip::update()
{


	//lookAt(behaviour->vectors.at("toPos"));

	if (deathSource == AlienShipDeathSource::ALIENSHIPDEATHSOURCE_PLANET_SHIELD)
	{
		yAcc -= Engine::smootherDeltaTime * 20;
		double distance = GameObject::find("Planet")->getPosition().distance(getPosition());
		setPositionAround(GameObject::find("Planet")->getPosition(), distance + yAcc * Engine::smootherDeltaTime * 4, Angle);
		alpha -= Engine::smootherDeltaTime * 40;

		if (alpha <= 0)
		{
			destroy();
		}
		animatedSprite->setTint(sf::Color(255, static_cast<int>(alpha), static_cast<int>(alpha), static_cast<int>(alpha)));
	}
	else if (exploding == true)
	{
		GameObject::find("Camera")->setPosition(GameObject::find("Camera")->getPosition() + Vector2(Utility::getRandInt(-1, 1), Utility::getRandInt(-1, 1)));
		explodingCounter -= Engine::smootherDeltaTime;
	}
	else if (EnemyHit == true)
	{
		deathMovement();
		deathCounter -= Engine::smootherDeltaTime;

		animatedSprite->setTint(sf::Color(127 + deathCounter * 64, 0, 0));

		setRotation(rotation += Engine::smootherDeltaTime * 200);
	}
	else
	{
		shipMovement();
		lookAt(GameObject::find("Planet")->getPosition());
	}

	if (deathCounter <= 0 && exploding == false)
	{
		exploding = true;
		animatedSprite->setTint(sf::Color(255, 255, 255));
		if (deathSource != AlienShipDeathSource::ALIENSHIPDEATHSOURCE_PLANET && deathSource != AlienShipDeathSource::ALIENSHIPDEATHSOURCE_PLANET_SHIELD)
		{
			static_cast<GameMaster*>(Master::master)->gameData.score++;
		}
	}
	if (explodingCounter <= 0)
	{
		if (PowerUp != PlayerPowerUp::POWER_UP_NONE)
		{
			new PickUp(getPosition(), PowerUp);
		}
		destroy();
	}
}

void AlienShip::onCollision(GameObject* other)
{
		if (other->name == "Planet")
		{
			//Deathsource set in Planet
			yAcc /= 2;
		}
		else if (EnemyHit == true)
		{
			return;
		}

		if (other->name == "Projectile")
		{
			health--;
			if (health <= 0)
			{
				deathSource = AlienShipDeathSource::ALIENSHIPDEATHSOURCE_PROJECTILE;
				rotation = getRotation();
				EnemyHit = true;
				collider->colliderTag = "";
				CollPos = other->getPosition();
				new Combo(getPosition());
			}
		}
		else if (other->name == "AlienShip" && (static_cast<AlienShip*>(other)->EnemyHit || EnemyHit))
		{
			deathSource = AlienShipDeathSource::ALIENSHIPDEATHSOURCE_ALIENSHIP;
			rotation = getRotation();
			EnemyHit = true;
			static_cast<AlienShip*>(other)->CollPos = other->getPosition();
			new Combo(getPosition());
		}
		else if (other->name == "EMP")
		{
			EMPcounter -= Engine::smootherDeltaTime;
			if (EMPcounter <= 0)
			{
				EnemyHit = true;
			}
		}
}