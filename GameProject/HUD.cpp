#include "stdafx.h"
#include "HUD.h"

using namespace GameEngine;

HUD::HUD() : GameObject("HUD")
{

}

void HUD::update() {
	toHealth = Utility::lerp(toHealth, static_cast<Planet*>(GameObject::find("Planet"))->health, Engine::smootherDeltaTime);
	if (static_cast<Planet*>(GameObject::find("Planet"))->health >= 0)
	{
		
		GUI::image(GUIImage{ "FinalHUD.png", Vector2(0, Engine::getLogicalResolution().y - 191) });
		PowerUpBar::update();

		int maxHealth = 0;
		if (GameObject::find("Sector Shield: 0") == nullptr)
		{
			maxHealth += MAXPLAYERHEALTH / 4;
		}
		if (GameObject::find("Sector Shield: 1") == nullptr)
		{
			maxHealth += MAXPLAYERHEALTH / 4;
		}
		if (GameObject::find("Sector Shield: 2") == nullptr)
		{
			maxHealth += MAXPLAYERHEALTH / 4;
		}
		if (GameObject::find("Sector Shield: 3") == nullptr)
		{
			maxHealth += MAXPLAYERHEALTH / 4;
		}

		GUI::image(GUIImage{ "healthbarback.png", Rect(0, Engine::getLogicalResolution().y - ((256 / maxHealth) * toHealth), 256, 256), Vector2(0, 256 - ((256 / maxHealth) * toHealth)) });
		GUI::image(GUIImage{ "Healthbar_best.png", Vector2(0, Engine::getLogicalResolution().y - 256) });
		static_cast<ComboMeter*>(GameObject::find("Combo Meter"))->update();
		static_cast<Minimap*>(GameObject::find("Minimap"))->update();
	}
}
