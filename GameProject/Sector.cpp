#include "stdafx.h"
#include "Sector.h"

using namespace GameEngine;

Sector::Sector(int num) : GameObject("Sector: " + std::to_string(num))
{
	addSprite();
	animatedSprite->setZIndex(1000);
	if (num == 1)
	{
		animatedSprite->setSprite("Sector2Damaged.png");//sector 4
	}
	else if (num == 2)
	{
		animatedSprite->setSprite("Sector3Damaged.png");//sector 3
	}
	else if (num == 3)
	{
		animatedSprite->setSprite("sector2damaged.png");//sector 2
	}
	else if (num == 0)
	{
		animatedSprite->setSprite("Sector1Damaged.png"); //sector 1
	}
	else if (num == 4)
	{
		AudioManager::playSound("sector lost2.wav");
		animatedSprite->setSprite("Sector1Destroyed.png");
		animatedSprite->setZIndex(1001);
	}
	else if (num == 5)
	{
		AudioManager::playSound("sector lost2.wav");
		animatedSprite->setSprite("Sector4Destroyed.png");
		animatedSprite->setZIndex(1001);
	}
	else if (num == 6)
	{
		AudioManager::playSound("sector lost2.wav");
		animatedSprite->setSprite("Sector3Destroyed.png");
		animatedSprite->setZIndex(1001);
	}
	else if (num == 7)
	{
		AudioManager::playSound("sector lost2.wav");
		animatedSprite->setSprite("Sector2Destroyed.png");
		animatedSprite->setZIndex(1001);
	}
	

	setPositionAround(GameObject::find("Planet")->getPosition(), 515, 45 + num * 90);
	lookAt(GameObject::find("Planet")->getPosition());
	setRotation(getRotation() +  0);
}
