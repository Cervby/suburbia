

#pragma once

#include "stdafx.h"



namespace GameEngine
{
	class Vector2;

	class Button {
	public:
		char* id;
		sf::Event eventP;

		Button(char* iid, sf::Event eevent);
	};

	class InputManager
	{
	private:
		static std::vector<sf::Event> eventList;
		static std::vector<sf::Event> eventPressedList;
		static char textChar;

		static std::vector<Button> buttonList;

		static Vector2 lastMousePosition;

		static sf::Event event;

	public:
		static void frameInputUpdate();

		static void quit();

		static bool getQuit();

		static char getText();

		static bool getKeyDown(sf::Event::KeyEvent& keyEvent);
		static bool getKeyUp(sf::Event::KeyEvent& keyEvent);
		static bool getKeyPressed(sf::Event::KeyEvent& keyEvent);

		static bool getMouseDown(sf::Event::MouseButtonEvent& button);
		static bool getMouseUp(sf::Event::MouseButtonEvent& button);
		static bool getMousePressed(sf::Event::MouseButtonEvent& button);

		static bool getButtonDown(std::string id);
		static bool getButtonUp(std::string id);
		static bool getButtonPressed(std::string id);

		static void registerButton(char* id, sf::Event& inputEvent);

		static Vector2 getMousePosition();
		static Vector2 getMousePositionFixed();

		static Vector2 getMouseDelta();

		static bool getAnyKeyDown(sf::Event& event = sf::Event());
	};
}
