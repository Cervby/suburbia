#pragma once

#include "stdafx.h"

using namespace GameEngine;

class ShuttleShip : public AlienShip
{
private:
	int health;
	double escapePodTimer;

	virtual void shipMovement() override;
	virtual void deathMovement() override;
public:
	ShuttleShip(Vector2& toPos, std::string spriteName, int health);
};
