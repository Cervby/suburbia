#include "stdafx.h"
#include "Barrel.h"

using namespace GameEngine;

Barrel::Barrel() : GameObject("Barrel", DisplayPosition::DISPLAYPOSITION_RELATIVE)
{
	addSprite();
	animatedSprite->setSprite("Turretsprite.png");
	animatedSprite->setAnimation(Vector2(256, 256), 1, 1);
	animatedSprite->setZIndex(2000);
	//See Camera


}

void Barrel::update()
{
	setPositionAround(GameObject::find("Planet")->getPosition(), 579, 180 - static_cast<Camera*>(GameObject::find("Camera"))->getRotation() - 90);
	setPositionAround(getPosition(), 128, 90 - InputManager::getMousePosition().lookAt(getPosition()));
	lookAt(GameObject::find("Cursor")->getPosition());
	setRotation(getRotation() + 180);

	if (GameObject::find("Cursor")->getPosition().distance(GameObject::find("Planet")->getPosition()) <=
		getPosition().distance(GameObject::find("Planet")->getPosition()))
	{
		setRotation(getRotation() + 180);
	}

	GameObject* sectortmp = GameObject::find("Sector Shield: " + std::to_string((int)fmod(static_cast<Camera*>(GameObject::find("Camera"))->toRotation / 90, 4)));
	if (sectortmp == nullptr)
	{
		static_cast<Camera*>(GameObject::find("Camera"))->pos = Utility::lerp(static_cast<Camera*>(GameObject::find("Camera"))->pos, 0, Engine::smootherDeltaTime * 5);
	}
	else
	{
		static_cast<Camera*>(GameObject::find("Camera"))->pos = Utility::lerp(static_cast<Camera*>(GameObject::find("Camera"))->pos, 325, Engine::smootherDeltaTime * 5);
	}
	Vector2 from = Utility::worldToScreen(getPosition());
	from.y += static_cast<Camera*>(GameObject::find("Camera"))->pos;
	setPosition(Utility::screenToWorld(from));
}