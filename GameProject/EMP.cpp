#include "stdafx.h"
#include "EMP.h"

using namespace GameEngine;

EMP::EMP(Vector2& toPos) : GameObject("EMP")
{
	addSprite();
	addCollider();
	collider->set(ColliderType::COLLIDER_CIRCLE);
	collider->colliderTag = "Friendly";
	animatedSprite->setSprite("empprojectile.png");
	Vector2 pos = GameObject::find("Barrel")->getPosition();
	setPosition(pos);

	scaleSize = 1;
	toDir = toPos - pos;
	coolDown = 60;
	deathCounter = 7;
	explodeCounter = 2;
	exploding = false;
	explodingTwo = false;

	setScale(Vector2(scaleSize, scaleSize));
	AudioManager::playSound("EMP.wav", static_cast<float>(Utility::getRandInt(80, 130)) / 100);
	lookAt(toPos);
}	

void EMP::update()
{
	if (exploding == true)
	{

		setScale(Vector2(scaleSize, scaleSize));
		scaleSize += Engine::smootherDeltaTime;
		deathCounter -= Engine::smootherDeltaTime;

	}
	else if (explodingTwo == true)
	{
		explodeCounter -= Engine::smootherDeltaTime;
		if (explodeCounter <= 0)
		{
			exploding = true;
			scaleSize = 0.001;
			setScale(Vector2(scaleSize, scaleSize));
			animatedSprite->setSprite("empsmaller.png");
			animatedSprite->setAnimation(Vector2(2048, 2048), 1, 21);
			collider->set(ColliderType::COLLIDER_CIRCLE);
			collider->colliderTag = "Friendly";
			collider->radius *= 0.9;
		}
	}
		
	else
	{
		moveDir(toDir, Engine::smootherDeltaTime * 200);
		explodeCounter -= Engine::smootherDeltaTime;
		if (explodeCounter < 0)
		{
				
			animatedSprite->setSprite("EMP Blue Spark Spritesheet.png");
			animatedSprite->setAnimation(Vector2(200, 200), 0.05, 9);
			explodingTwo = true;
			explodeCounter = 0.1;
				
		}
	}

	if (deathCounter <= 0)
	{
		destroy();
	}
};
