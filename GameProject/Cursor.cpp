#include "stdafx.h"
#include "Cursor.h"

using namespace GameEngine;

#define COOLDOWN 0.5

Cursor::Cursor() : GameObject("Cursor")
{
	addSprite();
	whichSound = 0;
	timeSinceLastProjectile = 0;
	timeSinceLastEMP = 0;
	rotation = 0;
	animatedSprite->setSprite("Crosshair2.png");
	animatedSprite->setAboveAll(true);
}

void Cursor::update()
{
	setPosition(InputManager::getMousePosition());
	setRotation(RenderManager::window.getView().getRotation());

	timeSinceLastProjectile += Engine::deltaTime;
	timeSinceLastEMP += Engine::deltaTime;
	if (Master::master->name == "Game Master")
	{
		Camera* camera = static_cast<Camera*>(GameObject::find("Camera"));
		double rot = camera->toRotation;
		GameObject* sectortmp = GameObject::find("Sector Shield: " + std::to_string((int)fmod(rot / 90, 4)));
		if (InputManager::getButtonDown("Fire") && sectortmp == nullptr)
		{
			if (timeSinceLastProjectile > COOLDOWN)
			{
					
				whichSound = Utility::getRandInt(1, 4);

				if (whichSound == 1)
				{
					AudioManager::playSound("shot start2.wav");
				}
					
				else if (whichSound == 2)
				{
					AudioManager::playSound("shot startA.wav");
				}

				else if (whichSound == 3)
				{
					AudioManager::playSound("shot startF.wav");
				}

				else if (whichSound == 4)
				{
					AudioManager::playSound("shot startG.wav");
				}
				new Projectile(getPosition());
				timeSinceLastProjectile = 0;
				GameObject::find("Barrel")->animatedSprite->setSprite("Turretsprite.png");
				GameObject::find("Barrel")->animatedSprite->setAnimation(Vector2(256, 256), COOLDOWN / 13, 13);
			}
		}

		if (timeSinceLastProjectile >= COOLDOWN && GameObject::find("Barrel") != nullptr)
		{
			GameObject::find("Barrel")->animatedSprite->setAnimation(Vector2(256, 256), 1, 1);
		}

		if (InputManager::getButtonDown("Power up") && timeSinceLastEMP > 0.5)
		{
			switch (static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.currentPowerUp)
			{
			case PlayerPowerUp::POWER_UP_BLACK_HOLE:
				new BlackHole(getPosition());
				static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.currentPowerUp = PlayerPowerUp::POWER_UP_NONE;
				break;
			case PlayerPowerUp::POWER_UP_EMP:
				new EMP(getPosition());
				static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.currentPowerUp = PlayerPowerUp::POWER_UP_NONE;
				break;
			case PlayerPowerUp::POWER_UP_SLOW_TIME:
				new SlowTime(getPosition(), *static_cast<Camera*>(GameObject::find("Camera")));
				static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.currentPowerUp = PlayerPowerUp::POWER_UP_NONE;
				break;
			}
			timeSinceLastEMP = 0;
		}
			
		animatedSprite->setSprite("Crosshair2.png");
		for (GameObject* obit : GameObject::objectList)
		{
			if (obit->name == "PickUp")
			{
				if (obit->collider->isHovering)
				{
					if (static_cast<PickUp*>(obit)->gotten == false)
					{
						animatedSprite->setSprite("TargetCrosshair.png");
					}
				}
			}
		}
	}
	else
	{
		if (GUI::hovering)
		{
			animatedSprite->setSprite("Crosshair4.png");
		}
		else
		{
			animatedSprite->setSprite("Crosshair3.png");
		}

	}
}
