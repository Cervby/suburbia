#include "stdafx.h"
#include "SectorShield.h"

using namespace GameEngine;

SectorShield::SectorShield(int num, Vector2& planetPos) : GameObject("Sector Shield: " + std::to_string(num))
{
	addSprite();
	deathCounter = 0;
	
	animatedSprite->setSprite("PlanetShield.png");
	animatedSprite->setZIndex(1002);
	setPositionAround(planetPos, 700, 45 + num * 90);
	lookAt(planetPos);
	setRotation(getRotation() + 135);
}
