#include "stdafx.h"

using namespace GameEngine;

HighScore::HighScore(std::string lastMasterName) : Master("High gameData.score")
{
	GUI::setStyle("null.png", "null.png", "null.png", "null.png", "ethnocentric rg.ttf");
	std::ifstream file;
	file.open("../assets/HIGHSCORE");
	if (file.good())
	{
		std::string line;
		while (std::getline(file, line))
		{
			highscores.push_back(line);
		}
		file.close();
	}
}

HighScore::~HighScore()
{
	GUI::setStyle("null.png", "null.png", "null.png", "null.png", "Maya Culpa.ttf");
	highscores.clear();
}

void HighScore::update()
{
	GUI::image(GUIImage{ "Bakgrund.png", Vector2(0, 0) });
	if (GUI::hovering)
	{
		GUI::image(GUIImage{ "HighScoreBack.png", Vector2(0, 0) });
	}
	else
	{
		GUI::image(GUIImage{ "HighScoreScreen.png", Vector2(0, 0) });
	}
	Vector2 resD40 = Engine::getLogicalResolution() / 40;
	for (int i = 0; i < (highscores.size() >= 12 ? 12 : highscores.size()); i++)
	{
		GUI::box(GUIBox{ Rect(0, resD40.y * 8 + resD40.y * i * 2, Engine::getLogicalResolution().x, resD40.y), highscores[i] });
	}
	if (GUI::button(GUIButton{ Rect(Engine::getLogicalResolution().x / 4, resD40.y * 35, Engine::getLogicalResolution().x / 2, resD40.y * 4), "", Vector2(0, 0) }))
	{
		Master::master->changeMaster<MainMenu>();
	}
	if (InputManager::getQuit() || InputManager::getButtonDown("Exit"))
	{
		Master::master->changeMaster<MainMenu>();
	}
}