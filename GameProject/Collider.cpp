#include "stdafx.h"
#include "Collider.h"

namespace GameEngine
{
	Collider::Collider(GameObject* gameObject) : gameObject(gameObject)
	{
		bNoCollision = false;
		region.x = region.y = region.w = region.h = 0;
		colliderType = ColliderType::INVALID_COLLIDER_TYPE;
		isClicked = false;
		colliderTag = "";
	}

	bool Collider::overlap(Collider* other)
	{
		if (other == this)
		{
			return false;
		}
		
		if (colliderType == ColliderType::COLLIDER_CIRCLE && other->colliderType == ColliderType::COLLIDER_CIRCLE)
		{
			double radiusA = radius * gameObject->getScale().x;
			double radiusB = other->radius * other->gameObject->getScale().x;
			if (gameObject->getPosition().distance(other->gameObject->getPosition()) < radiusA + radiusB)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		else if (other->colliderType == ColliderType::COLLIDER_SQUARE && other->colliderType == ColliderType::COLLIDER_SQUARE)
		{
			Vector2 centerA = gameObject->getPosition();// + Vector2(region.x + region.w / 2, region.y + region.h / 2);
			Vector2 centerB = other->gameObject->getPosition();// + Vector2(other->region.x + other->region.w / 2, other->region.y + other->region.h / 2);
			int centerDeltaX = centerA.x - centerB.x;
			int centerDeltaY = centerA.y - centerB.y;

			if (abs(centerDeltaX) < (region.w / 2 + other->region.w / 2))
			{
				if (abs(centerDeltaY) < (region.h / 2 + other->region.h / 2))
				{
					return true;
				}
			}
		}

		return false;
	}

	void Collider::setRegionFromSprite()
	{
		if (region.w == 0 && region.h == 0)
		{
			AnimatedSprite* animatedSprite = gameObject->animatedSprite;
			if (animatedSprite != nullptr)
			{
				Rect tmpRect = animatedSprite->getRegion();
				region.w = tmpRect.w;
				region.h = tmpRect.h;
				radius = region.w / 2;
			}
		}
	}

	void Collider::set(ColliderType type, Rect region)
	{
		this->colliderType = type;
		this->region = region;
		this->radius = region.w / 2;
		setRegionFromSprite();
	}

	void Collider::set(ColliderType type, double radius)
	{
		this->colliderType = type;
		this->region = Rect(-radius, -radius, radius, radius);
		this->radius = radius;
	}

	void Collider::updateCollisions()
	{
		std::unordered_map<int, bool> collided;
		for (unsigned int i = 0; i < GameObject::objectList.size(); i++)
		{
			Collider* collider = GameObject::objectList[i]->collider;
			if (collider != nullptr)
			{
				if (collider->colliderType == ColliderType::COLLIDER_SQUARE || collider->colliderType == ColliderType::COLLIDER_CIRCLE)
				{
					if (collider->colliderType == ColliderType::COLLIDER_CIRCLE)
					{
						sf::Event::MouseButtonEvent ev;
						ev.button = sf::Mouse::Left;
						collider->isHovering = InputManager::getMousePosition().distance(collider->gameObject->getPosition()) < collider->radius;
						collider->isClicked = collider->isHovering && InputManager::getMouseDown(ev);
					}

					for (unsigned int ii = 0; ii < GameObject::objectList.size(); ii++)
					{
						Collider* otherCollider = GameObject::objectList[ii]->collider;
						if (GameObject::objectList[i] == GameObject::objectList[ii] || otherCollider == nullptr || (collider->colliderTag == otherCollider->colliderTag && collider->colliderTag != ""))
						{
							continue;
						}
						if (collider->overlap(otherCollider))
						{
							collider->gameObject->onOverlap(GameObject::objectList[ii]);
							if (collider->bNoCollision == false && otherCollider->bNoCollision == false)
							{
								collider->gameObject->onCollision(GameObject::objectList[ii]);
								
								collided[i] = true;
							}
						}
					}
				}
				else if (collider->colliderType != ColliderType::COLLIDER_SQUARE)
				{
					std::cout << "Invalid collider type for game object : " << GameObject::objectList[i]->name << std::endl;
				}
			}
		}
		for (auto item : collided)
		{
			GameObject* gameObject = GameObject::objectList[item.first];
			if (gameObject->_revertBackPosition != Vector2())
			{
				gameObject->setPosition(gameObject->_revertBackPosition);
			}
		}
		for (GameObject* gameObject : GameObject::objectList)
		{
			gameObject->_revertBackPosition = Vector2();
		}
	}
}
