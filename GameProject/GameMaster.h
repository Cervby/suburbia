#pragma once

#include "stdafx.h"

using namespace GameEngine;

enum class EnemyType
{
	ENEMYTYPE_NONE,
	ENEMYTYPE_TORNADO,
	ENEMYTYPE_SHUTTLESHIP,
	ENEMYTYPE_ESCAPEPOD,
	ENEMYTYPE_PORTER,
	ENEMYTYPE_ZAPDOSER,
	ENEMYTYPE_INTERCEPTOR_LEFT,
	ENEMYTYPE_INTERCEPTOR_RIGHT,
	PAUSE_UNTIL_ENTER
};

enum class PlayerPowerUp
{
	POWER_UP_NONE,
	POWER_UP_EMP,
	POWER_UP_BLACK_HOLE,
	POWER_UP_SLOW_TIME,
};

struct WaveStep
{
	EnemyType enemyType;
	double spawnPosX;
	double timeUntilSpawn;
	bool isFirstOfWave;
	int unlock;
	std::string music;
	std::string button;
	std::string msg;
	std::string path;

	WaveStep(EnemyType enemyType, double spawnPosX, double timeUntilSpawn = 0, bool isFirstOfWave = false, std::string msg = "", std::string path = "Commander5.png") : enemyType(enemyType), spawnPosX(spawnPosX), timeUntilSpawn(timeUntilSpawn), isFirstOfWave(isFirstOfWave), unlock(-1), button(""), msg(msg), path(path), music("") {};
	WaveStep(int unlock) : enemyType(EnemyType::ENEMYTYPE_NONE), spawnPosX(0), timeUntilSpawn(0), isFirstOfWave(false), unlock(unlock), button(""), msg(""), path(""), music("") {};
	WaveStep(std::string button) : enemyType(EnemyType::ENEMYTYPE_NONE), spawnPosX(0), timeUntilSpawn(0), isFirstOfWave(false), unlock(-1), button(button), msg(""), path(""), music("") {};
	WaveStep(std::string music, bool b) : music(music), enemyType(EnemyType::ENEMYTYPE_NONE), spawnPosX(0), timeUntilSpawn(0), isFirstOfWave(false), unlock(-1), button(""), msg(""), path("") {};

private:
	WaveStep(EnemyType enemyType) {}; //Disallowing usage, otherwise clash with the unlock constructor.
};

struct GameData
{
	double waveStepCounter;
	double originalPlanetPosition;
	bool NewWave;

	PlayerPowerUp currentPowerUp;
	unsigned long long score;
	int waveIterator;
};

class Star : public GameObject
{
public:
	Star();
	~Star();
};

class GameMaster : public Master
{
	std::string music;

	virtual void update() override;

public:
	GameData gameData;

	GameMaster(std::string lastMasterName);
	GameMaster(std::string lastMasterName, GameData& gameData);

	static std::vector<WaveStep> listOfTheEnemyWaves;
};
