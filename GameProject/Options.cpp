#include "stdafx.h"

using namespace GameEngine;


Options::Options(std::string lastMasterName) : Master("Options")
{
	fromGame = false;
}

Options::Options(std::string lastMasterName, GameData& gameData) : Master("Options"), gameData(gameData)
{
	fromGame = true;
}

void Options::update()
{
	GUI::image(GUIImage{ "Bakgrund.png", Vector2(0, 0) });
	GUI::image(GUIImage{ "Options menu detaljer.png", Vector2(0, 0) });
	GUI::image(GUIImage{ "Options menu text.png", Vector2(0, 0) });
	Vector2 resD10 = Engine::getLogicalResolution() / 10;
	if (GUI::button(GUIButton{ Rect(resD10.x * 4, resD10.y * 2.75, resD10.x * 0.5, resD10.y * 1), "", Vector2(0, 0) }))
	{
		AudioManager::muteAllMusic(!AudioManager::getAllMusicMuted());
	}
	if (GUI::button(GUIButton{ Rect(resD10.x * 6.4, resD10.y * 4.25, resD10.x * 0.5, resD10.y * 1), "", { 0, 1 } }))
	{
		AudioManager::muteAllSound(!AudioManager::getAllSoundMuted());
	}
	if (GUI::button(GUIButton{ Rect(resD10.x * 5, resD10.y * 6, resD10.x * 2, resD10.y * 1), "", { 0, 2 } }))
	{
		Engine::getSettings().fullscreen = true;
		Engine::setFullscreen(Engine::getSettings().fullscreen);
		RenderManager::window.setMouseCursorVisible(false);
	}
	if (GUI::button(GUIButton{ Rect(resD10.x * 8, resD10.y * 6, resD10.x * 2, resD10.y * 1), "", { 1, 2 } }))
	{
		Engine::getSettings().fullscreen = false;
		Engine::setFullscreen(Engine::getSettings().fullscreen);
		RenderManager::window.setMouseCursorVisible(false);
	}
	if (GUI::button(GUIButton{ Rect(resD10.x * 2, resD10.y * 8, resD10.x * 6, resD10.y * 1), "", { 0, 3 } }))
	{
		if (fromGame)
		{
			Master::master->changeMaster<PauseMenu>(gameData);
		}
		else
		{
			Master::master->changeMaster<MainMenu>();
		}
	}
	if (InputManager::getQuit() || InputManager::getButtonDown("Exit"))
	{
		if (fromGame)
		{
			Master::master->changeMaster<PauseMenu>(gameData);
		}
		else
		{
			Master::master->changeMaster<MainMenu>();
		}
	}

	if (AudioManager::getAllMusicMuted() == false)
	{
		GUI::image(GUIImage{ "musicfxcheck.png", Vector2(0, 0) });
	}

	if (AudioManager::getAllSoundMuted() == false)
	{
		GUI::image(GUIImage{ "soundfxcheck.png", Vector2(0, 1) });
	}

	if (GUI::hovering)
	{
		if (GUI::selection == Vector2(0, 2))
		{
			GUI::image(GUIImage{ "Options menu text fullscreen gr�n1.png", Vector2(0, 0) });
		}
		else if (GUI::selection == Vector2(1, 2))
		{
			GUI::image(GUIImage{ "Options menu text windowed gr�n1.png", Vector2(0, 0) });
		}
		else if (GUI::selection == Vector2(0, 3))
		{
			GUI::image(GUIImage{ "Options menu text back gr�n.png", Vector2(0, 0) });
		}
	}
}