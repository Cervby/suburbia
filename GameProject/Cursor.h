#pragma once

#include "stdafx.h"

using namespace GameEngine;

struct Cursor : public GameObject
{
private:
	double whichSound;
	double timeSinceLastProjectile;
	double timeSinceLastEMP;
	double rotation;


public:
	virtual void update() override;///

	Cursor();
};
