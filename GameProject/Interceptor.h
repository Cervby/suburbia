#pragma once

#include "stdafx.h"

using namespace GameEngine;


class Interceptor : public AlienShip
{
private:
	double K;
	double J;
	double A;
	double accY;
	double accX;

	void shipMovement();
	void deathMovement();
public:
	Interceptor(Vector2& toPos, std::string spriteName, int health);
};
