#include "stdafx.h"

namespace GameEngine
{
	std::mt19937 Utility::rndGenerator;

	int Utility::getRandInt(int min, int max)
	{
		std::uniform_int_distribution<int> uni(min, max);
		return uni(rndGenerator);
	}

	Vector2 Utility::worldToScreen(Vector2& v2)
	{
		return Vector2::from_sfI(RenderManager::window.mapCoordsToPixel(sf::Vector2f(v2.x, v2.y)));
	}

	Vector2 Utility::screenToWorld(Vector2& v2)
	{
		return Vector2::from_sfF(RenderManager::window.mapPixelToCoords(sf::Vector2i(v2.x, v2.y)));
	}

	void Utility::init()
	{
		rndGenerator = std::mt19937(std::random_device()());
	}
}
