#include "stdafx.h"

using namespace GameEngine;

Zapdoser::Zapdoser(Vector2& toPos, std::string spriteName, int health, PlayerPowerUp powerUp) : AlienShip(toPos, spriteName, health, powerUp)
{
	this->health = health;
	sideWays = 3;
}

void Zapdoser::shipMovement()
{

	moveTo(toPos, Engine::smootherDeltaTime * SlowTime::speedFactor * 60);
	for (GameObject* iter : GameObject::objectList)
	{
		if (health == 1)
		{
			animatedSprite->setSprite("zapdoser damaged 128.png");
		}


		if (iter->name == "Projectile")
		{

			double distance = getPosition().distance(iter->getPosition());
			if (distance < 250)
			{
				moveTo(toPos, Engine::smootherDeltaTime * SlowTime::speedFactor * 200);

				if (InputManager::getMousePositionFixed().x < Engine::getLogicalResolution().x / 2)
				{
					Angle -= Engine::smootherDeltaTime * SlowTime::speedFactor * 20;
				}
				else
				{
					Angle += Engine::smootherDeltaTime * SlowTime::speedFactor * 20;
				}
				setPositionAround(toPos, getPosition().distance(toPos), Angle);

				sideWays -= Engine::smootherDeltaTime * SlowTime::speedFactor;
			}
			if (sideWays < 0)
			{
				moveTo(toPos, Engine::smootherDeltaTime * SlowTime::speedFactor * 60);
				sideWays = 3;
			}
		}



	}

	if (EMPcounter <= 1.7)
	{
		animatedSprite->setSprite("ZapdoserSparks.png");
		animatedSprite->setAnimation(Vector2(200, 200), 0.06, 10);
	}
	else if (EMPcounter <= 1.7 && health == 1)
	{
		animatedSprite->setSprite("ZapdoserDamagedSparks.png");
		animatedSprite->setAnimation(Vector2(200, 200), 0.06, 10);
	};
}

void Zapdoser::deathMovement()
{
	if (deathAnimationTrigger == 0)
	{
		WhichSounds = 1;

		if (WhichSounds == 1)
		{
			AudioManager::playSound("zapdoser expl2.wav");
		}


		deathAnimationTrigger = true;
		animatedSprite->setSprite("ZapdoserDeathSheet.png");
		animatedSprite->setAnimation(Vector2(180, 181), 0.05, 9);
	}

	if (InputManager::getMousePositionFixed().x < Engine::getLogicalResolution().x / 2)
	{
		Angle -= Engine::smootherDeltaTime * SlowTime::speedFactor * 20;

	}
	else {
		Angle += Engine::smootherDeltaTime * SlowTime::speedFactor * 20;

	}

	setPositionAround(toPos, getPosition().distance(toPos), Angle);
}
