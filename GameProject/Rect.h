

#pragma once

#include "stdafx.h"



namespace GameEngine
{
	struct Rect
	{
		int x, y, w, h;

		Rect(int x, int y, int w, int h);
		Rect();
		Rect(const Rect &rect);

		const std::string to_str() const;

		Rect operator=(const Rect& rect);
		Rect operator+(const Rect& rect) const;
		Rect operator-(const Rect& rect) const;
		Rect operator*(const Rect& rect) const;
		Rect operator/(const Rect& rect) const;
		Rect& operator+=(const Rect& rect);
		Rect& operator-=(const Rect& rect);
		Rect& operator*=(const Rect& rect);
		Rect& operator/=(const Rect& rect);
		Rect operator-() const;
		bool operator==(const Rect& rect) const;
		bool operator!=(const Rect& rect) const;
		Rect operator*(const int& otherint) const;
		Rect operator/(const int& otherint) const;
		Rect& operator*=(const int& otherint);
		Rect& operator/=(const int& otherint);
		bool operator<(const Rect& rect) const; //To be able use with std::map<>
	};
}
