#include "stdafx.h"
#include "Commander.h"

using namespace GameEngine;

Commander::Commander() : GameObject("Commander")
{
	alphaB = 0;
	setPosition(Vector2{ 1920, 0 });
}

void Commander::update()
{
	if (static_cast<GameMaster*>(GameObject::find("Game Master"))->listOfTheEnemyWaves[static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.waveIterator].isFirstOfWave)
	{
		alphaB = Utility::lerp(alphaB, 255, Engine::smootherDeltaTime);
		if (static_cast<GameMaster*>(GameObject::find("Game Master"))->listOfTheEnemyWaves[static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.waveIterator].path != "")
		{
			imagePath = static_cast<GameMaster*>(GameObject::find("Game Master"))->listOfTheEnemyWaves[static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.waveIterator].path;
			setPosition(Vector2{ alphaB, getPosition().y });
			if (imagePath == "AlienF.png")
			{
				GUI::image(GUIImage{ imagePath, Rect(1920 - 400, 250, 0, 0), "", alphaB });
			}
			else
			{
				GUI::image(GUIImage{ imagePath, Rect(1920 - 400, 500, 0, 0), "", alphaB });
			}
			GUI::image(GUIImage{ "CommanderTalkChatbar (1).png", Rect(1920 - 512, 800, 0, 0), static_cast<GameMaster*>(GameObject::find("Game Master"))->listOfTheEnemyWaves[static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.waveIterator].msg, alphaB
		});
			if (GUI::button(GUIButton{ { (1920 / 4) * 3 - 256, (1080 / 5) * 4 - 32, 512, 64 }, "Click here to continue", Vector2(0, 0) }))
			{
				static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.waveIterator++;
			}
		}
	}
	else
	{
		alphaB = Utility::lerp(alphaB, 0, Engine::smootherDeltaTime);
		if (imagePath != "")
		{
			setPosition(Vector2{ alphaB, getPosition().y });
			if (imagePath == "AlienF.png")
			{
				GUI::image(GUIImage{ imagePath, Rect(1920 - 400, 250, 0, 0), "", alphaB
			});
			}
			else
			{
				GUI::image(GUIImage{ imagePath, Rect(1920 - 400, 500, 0, 0), "", alphaB
			});
			}
			GUI::image(GUIImage{ "NewChatBar.png", Rect(1920 - 512, 800, 0, 0), static_cast<GameMaster*>(GameObject::find("Game Master"))->listOfTheEnemyWaves[static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.waveIterator].msg, alphaB
		});
		}
	}
}
