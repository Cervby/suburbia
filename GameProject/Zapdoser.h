#pragma once

#include "stdafx.h"

using namespace GameEngine;

class Zapdoser : public AlienShip
{
private:
	int health;
	double sideWays;
	virtual void shipMovement()override;
	virtual void deathMovement()override;
public:
	Zapdoser(Vector2& toPos, std::string spriteName, int health, PlayerPowerUp powerUp);
};
