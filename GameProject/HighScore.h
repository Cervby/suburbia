#pragma once
#include "stdafx.h"

using namespace GameEngine;

class HighScore : public Master
{
	std::vector<std::string> highscores;
	
	virtual void update() override;
	
public:
	HighScore(std::string lastMasterName);
	virtual ~HighScore();
};

