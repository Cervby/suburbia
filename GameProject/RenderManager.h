

#pragma once

#include "stdafx.h"



namespace GameEngine
{
	class RenderManager
	{
	public:
		static std::multimap<float, GameObject*> renderOrder;//Maps & multimaps are automatically sorted.
		static std::vector<sf::RectangleShape> debugSprites;
		static sf::RenderWindow window;

		static void render();
	};
}
