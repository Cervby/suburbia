#include "stdafx.h"
#include "President.h"

using namespace GameEngine;


President::President() : GameObject("President")
{
	setPosition(Vector2{ 1920, 0 });
	moveOut = false;


}

void President::update()
{
	double xPos = getPosition().x;

	if (moveOut == true)
	{
		xPos = Utility::lerp(xPos, 1930, Engine::smootherDeltaTime);
		if (xPos > 1915)
		{
			destroy();
			Master::master->changeMaster<MainMenu>();
		}
	}
	else
	{
		xPos -= Utility::lerp(0, xPos - 1400, Engine::smootherDeltaTime);
	}
	GUI::image(GUIImage{ "President.png", Vector2(xPos + 13, 400) });
	GUI::image(GUIImage{ "NewChatBar.png", Vector2(xPos - 220, 500) });
	setPosition(Vector2{ xPos, getPosition().y });
}
