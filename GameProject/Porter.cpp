#include "stdafx.h"

using namespace GameEngine;

Porter::Porter(Vector2& toPos, std::string spriteName, int health) : AlienShip(toPos, spriteName, health)
{
}

void Porter::shipMovement()
{
	if (A == 0)
	{
		A = 0;
		B = 0;
		AnimationOn = false;
		animatedSprite->setSprite("PortalSpritesheet.png");

		std::vector<Rect> regions;
		getPorterRegions(regions, true);
		animatedSprite->setAnimationFree(regions, 0.0625);
	}
	A += Engine::smootherDeltaTime * SlowTime::speedFactor;
	if (A >= 2)
	{
		A = 0;
		B += 5;
		std::vector<Rect> regions;
		getPorterRegions(regions, true);
		animatedSprite->setAnimationFree(regions, 0.0625);
		AnimationOn = false;
	}
	else if (A >= 1 && !AnimationOn)
	{
		AnimationOn = true;
		std::vector<Rect> regions;
		getPorterRegions(regions, false);
		animatedSprite->setAnimationFree(regions, 0.0625, Flip::FLIP_HORIZONTAL);
	}

	if (EMPcounter <= 1.7)
	{
		animatedSprite->setSprite("PorterSparks.png");
		animatedSprite->setAnimation(Vector2(200, 200), 0.06, 10);
	}

	moveTo(toPos, Engine::smootherDeltaTime * SlowTime::speedFactor * 100);
	Angle = B + fmod(A * 10, 40);
	setPositionAround(toPos, getPosition().distance(toPos), Angle);
}

void Porter::deathMovement()
{


	if (deathAnimationTrigger == 0)
	{
		WhichSounds = Utility::getRandInt(1, 3);

		if (WhichSounds == 1)
		{
			AudioManager::playSound("PORTER expl short B.wav");
		}

		else if (WhichSounds == 2)
		{
			AudioManager::playSound("PORTER expl short C.wav");
		}

		else if (WhichSounds == 3)
		{
			AudioManager::playSound("PORTER expl short E.wav");
		}

		deathAnimationTrigger = true;
		animatedSprite->setSprite("PorterDeathSpiteSheet.png");
		animatedSprite->setAnimation(Vector2(128, 128), 0.05, 9);
	}
	if (A == 0)
	{

		A = 0;
		B = 0;
	}
	A += Engine::smootherDeltaTime * SlowTime::speedFactor;
	if (A >= 2)
	{
		A = 0;
		B -= 10;
	}
	moveTo(toPos, Engine::smootherDeltaTime * SlowTime::speedFactor * 100);
	Angle = B - fmod(A * 10, 40);
	setPositionAround(toPos, getPosition().distance(toPos), Angle);
}

void Porter::getPorterRegions(std::vector<Rect>& regions, bool backwards)
{
	regions.clear();
	if (backwards)
	{
		for (int y = 3; y >= 0; y--)
		{
			for (int x = 3; x >= 0; x--)
			{
				regions.push_back({ x * 194, y * 128, 194, 128 });
			}
		}
	}
	else
	{
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				regions.push_back({ x * 194, y * 128, 194, 128 });
			}
		}
	}
}
