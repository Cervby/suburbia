#include "stdafx.h"
#include "ComboMeter.h"

using namespace GameEngine;

ComboMeter::ComboMeter() : GameObject("Combo Meter")
{
	comboCountShow = comboCount = 0;
	showTime = 0;
}

void ComboMeter::update()
{
	comboCountShow = Utility::lerp(comboCountShow, comboCount, Engine::smootherDeltaTime);
	GUI::box(GUIBox{ Rect(1410, Engine::getLogicalResolution().y - 105, 256, 128), std::to_string(static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.score) });
	if (comboCountShow + 127 + 120 < 512)
	{
		GUI::image(GUIImage{ "ComboBackgroundcolor.png", Rect(1265 - 256, Engine::getLogicalResolution().y - 30 - 256, comboCountShow + 127, 0), Vector2(0, 0) });
		GUI::image(GUIImage{ "ComboFront.png", Vector2(1020, Engine::getLogicalResolution().y - 290), false, "" });
		GUI::image(GUIImage{ "ComboText.png", Vector2(1020, Engine::getLogicalResolution().y - 290 + 9), false, "" });
	}
	else
	{
		if (showTime == 0)
		{
			AudioManager::playSound("combo ready.wav");
		}
		showTime += Engine::smootherDeltaTime;
		GUI::image(GUIImage{ "ComboBackgroundcolor.png", Rect(1265 - 256, Engine::getLogicalResolution().y - 30 - 256, 512, 0), Vector2(0, 0) });
		GUI::image(GUIImage{ "ComboBackgroundcolorReady.png", Rect(1265 - 256, Engine::getLogicalResolution().y - 30 - 256, comboCountShow + 127, 0), Vector2(0, 0), "", 127 + 127 * sin(Engine::clock.getElapsedTime().asSeconds() * 3) });
		GUI::image(GUIImage{ "ComboFront.png", Vector2(1020, Engine::getLogicalResolution().y - 285), false, "" });
		GUI::image(GUIImage{ "ComboText2.png", Vector2(1020, Engine::getLogicalResolution().y - 285 + 9), false, "" });
		
		if (showTime < 2)
		{
			GUI::image(GUIImage{ "null.png", Vector2(Engine::getLogicalResolution().x / 2, Engine::getLogicalResolution().y / 2), true, "COMBO POWER UP READY" });
			
		}
		if (InputManager::getButtonDown("Combo"))
		{
			new SlowTime(GameObject::find("Cursor")->getPosition(), *static_cast<Camera*>(GameObject::find("Camera")));
			comboCount = 0; 
			showTime = 0;
		}
	}
	GUI::image(GUIImage{ "ComboFront.png", Vector2(1300, Engine::getLogicalResolution().y - 290) });
}
