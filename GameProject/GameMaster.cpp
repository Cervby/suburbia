#include "stdafx.h"
#include "Master.h"

using namespace GameEngine;

std::vector<WaveStep> GameMaster::listOfTheEnemyWaves;

GameMaster::GameMaster(std::string lastMasterName) : Master("Game Master")
{
	if (lastMasterName == "Intro")
	{
		new Space;
		new Planet;
		new Turret;
		new Barrel;
		new Camera;
		new Commander;
		new HUD;
		new ComboMeter();
		new Minimap();
		gameData.currentPowerUp = PlayerPowerUp::POWER_UP_NONE;
		gameData.score = 0;

		gameData.waveStepCounter = gameData.waveIterator = 0;
		GUI::setStyle("null.png", "null.png", "null.png", "null.png", "ethnocentric rg.ttf");
		gameData.originalPlanetPosition = -1;
		AudioManager::playMusic("gameplaytheme tempo.ogg");
		AudioManager::setVolumeMusic("gameplaytheme tempo.ogg", 0);
		AudioManager::playMusic("gameplaytheme.ogg");

		for (int i = 0; i < 20; i++)
		{
			new Star;	
		}

		SlowTime::speedFactor = 1;
	}
}

GameMaster::GameMaster(std::string lastMasterName, GameData& gameData) : Master("Game Master"), gameData(gameData)
{
	
}

void GameMaster::update()
{
	if (gameData.waveIterator < listOfTheEnemyWaves.size() - 1)
	{
		if (listOfTheEnemyWaves[gameData.waveIterator].isFirstOfWave)
		{
			if (gameData.NewWave == false)
			{
				gameData.NewWave = true;
				AudioManager::playSound("new wave.wav");
			}
		}
		else { gameData.NewWave = false; }

		if (listOfTheEnemyWaves[gameData.waveIterator].enemyType != EnemyType::ENEMYTYPE_NONE)
		{
			if (GameObject::find("Sector: 5") != nullptr && fmod(listOfTheEnemyWaves[gameData.waveIterator].spawnPosX, 360) >= 90 && fmod(listOfTheEnemyWaves[gameData.waveIterator].spawnPosX, 360) < 180)
			{
				gameData.waveIterator++;
				return;
			}
			else if (GameObject::find("Sector: 6") != nullptr && fmod(listOfTheEnemyWaves[gameData.waveIterator].spawnPosX, 360) >= 180 && fmod(listOfTheEnemyWaves[gameData.waveIterator].spawnPosX, 360) < 270)
			{
				gameData.waveIterator++;
				return;
			}
			else if (GameObject::find("Sector: 7") != nullptr && fmod(listOfTheEnemyWaves[gameData.waveIterator].spawnPosX, 360) >= 270)
			{
				gameData.waveIterator++;
				return;
			}
			else if (GameObject::find("Sector: 4") != nullptr && fmod(listOfTheEnemyWaves[gameData.waveIterator].spawnPosX, 360) < 90)
			{
				gameData.waveIterator++;
				return;
			}
		}
			
		if (listOfTheEnemyWaves[gameData.waveIterator].unlock != -1) //Always put { ENEMYTYPE_NONE, 0, 0 } before sector unlock :|
		{
			SectorShield* sectortmp = static_cast<SectorShield*>(GameObject::find("Sector Shield: " + std::to_string(listOfTheEnemyWaves[gameData.waveIterator].unlock)));
				
			if (sectortmp != nullptr)
			{
				if (gameData.originalPlanetPosition == -1)
				{
					AudioManager::playSound("new sector unlocked.wav");

					gameData.originalPlanetPosition = 100000 * 90 + fmod(static_cast<Camera*>(GameObject::find("Camera"))->toRotation, 360);
					static_cast<Camera*>(GameObject::find("Camera"))->currentRotation = 100000 * 90 + fmod(static_cast<Camera*>(GameObject::find("Camera"))->currentRotation, 360);
					static_cast<Camera*>(GameObject::find("Camera"))->toRotation = 100000 * 90 + listOfTheEnemyWaves[gameData.waveIterator].unlock * 90;
						
					if (static_cast<Camera*>(GameObject::find("Camera"))->toRotation == 100000 * 90 + 270 && gameData.originalPlanetPosition == 100000 * 90)
					{
						static_cast<Camera*>(GameObject::find("Camera"))->currentRotation += 360;
						gameData.originalPlanetPosition += 360;
					}
					else if (static_cast<Camera*>(GameObject::find("Camera"))->toRotation == 100000 * 90 && gameData.originalPlanetPosition == 100000 * 90 + 270)
					{
						static_cast<Camera*>(GameObject::find("Camera"))->currentRotation -= 360;
						gameData.originalPlanetPosition -= 360;
					}
				}
				if (fabs(static_cast<Camera*>(GameObject::find("Camera"))->currentRotation - static_cast<Camera*>(GameObject::find("Camera"))->toRotation) < 0.1)
				{
					if (sectortmp->deathCounter == 0)
					{
						sectortmp->animatedSprite->setSprite("planetshieldanim4.png");
						sectortmp->animatedSprite->setAnimation(Vector2(1028, 1024), 0.175, 6);
					}
					sectortmp->deathCounter += Engine::smootherDeltaTime;
					sectortmp->animatedSprite->setTint(sf::Color(255, 255, 255, 255 - sectortmp->deathCounter * 256));
					if (sectortmp->deathCounter > 1)
					{
						sectortmp->destroy();
					}
				}
			}
			else
			{
				static_cast<Camera*>(GameObject::find("Camera"))->toRotation = gameData.originalPlanetPosition;
				if (abs(static_cast<Camera*>(GameObject::find("Camera"))->currentRotation - static_cast<Camera*>(GameObject::find("Camera"))->toRotation) < 0.1)
				{
					gameData.waveIterator++;
					gameData.originalPlanetPosition = -1;
				}
			}
			gameData.waveIterator--;
		}

		if (listOfTheEnemyWaves[gameData.waveIterator].music != "")
		{
			music = listOfTheEnemyWaves[gameData.waveIterator].music;
		}
		if (music == "gameplaytheme.ogg")
		{
			AudioManager::setVolumeMusic("gameplaytheme.ogg", AudioManager::getVolumeMusic("gameplaytheme.ogg") - Engine::smootherDeltaTime);
			AudioManager::setVolumeMusic("gameplaytheme tempo.ogg", AudioManager::getVolumeMusic("gameplaytheme tempo.ogg") + Engine::smootherDeltaTime);
			if (AudioManager::getVolumeMusic("gameplaytheme.ogg") <= 0 && AudioManager::getVolumeMusic("gameplaytheme tempo.ogg") >= 100)
			{
				AudioManager::setVolumeMusic("gameplaytheme.ogg", 0);
				AudioManager::setVolumeMusic("gameplaytheme tempo.ogg", 100);
			}
		}
		else if (music == "gameplaytheme tempo.ogg")
		{
			AudioManager::setVolumeMusic("gameplaytheme.ogg", AudioManager::getVolumeMusic("gameplaytheme.ogg") + Engine::smootherDeltaTime);
			AudioManager::setVolumeMusic("gameplaytheme tempo.ogg", AudioManager::getVolumeMusic("gameplaytheme tempo.ogg") - Engine::smootherDeltaTime);
			if (AudioManager::getVolumeMusic("gameplaytheme.ogg") >= 100 && AudioManager::getVolumeMusic("gameplaytheme tempo.ogg") <= 0)
			{
				AudioManager::setVolumeMusic("gameplaytheme.ogg", 100);
				AudioManager::setVolumeMusic("gameplaytheme tempo.ogg", 0);
			}
		}

		if (gameData.waveStepCounter >= listOfTheEnemyWaves[gameData.waveIterator].timeUntilSpawn)
		{
			gameData.waveStepCounter = 0;
			if (listOfTheEnemyWaves[gameData.waveIterator].button != "")
			{
				new Tutorial(listOfTheEnemyWaves[gameData.waveIterator].button);
			}

			switch (listOfTheEnemyWaves[gameData.waveIterator].enemyType)
			{
			case EnemyType::ENEMYTYPE_TORNADO:
				new Cyclos(Vector2(listOfTheEnemyWaves[gameData.waveIterator].spawnPosX, 1800), "cyclos_128.png", 1, Utility::getRandInt(0, 10) == 1 ? PlayerPowerUp::POWER_UP_BLACK_HOLE : PlayerPowerUp::POWER_UP_NONE);
				break;
			case EnemyType::ENEMYTYPE_SHUTTLESHIP:
				new ShuttleShip(Vector2(listOfTheEnemyWaves[gameData.waveIterator].spawnPosX, 1800), "Shuttleship256.png", 3);
				break;
			case EnemyType::ENEMYTYPE_ZAPDOSER:
				new Zapdoser(Vector2(listOfTheEnemyWaves[gameData.waveIterator].spawnPosX, 1800), "zapdoser_128.png", 2, Utility::getRandInt(0, 1) == 1 ? PlayerPowerUp::POWER_UP_EMP : PlayerPowerUp::POWER_UP_NONE);
				break;
			case EnemyType::ENEMYTYPE_PORTER:
				new Porter(Vector2(listOfTheEnemyWaves[gameData.waveIterator].spawnPosX, 1800), "NY porter 128.png", 1);
				break;
			case EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT:
			case EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT:
				new Interceptor(Vector2(listOfTheEnemyWaves[gameData.waveIterator].spawnPosX, 1800), "interceptor128.png", 1);
				break;
			case EnemyType::PAUSE_UNTIL_ENTER:
				sf::Event::KeyEvent event;
				event.alt = false;
				event.control = false;
				event.shift = false;
				event.system = false;
				event.code = sf::Keyboard::Return;
				if (!InputManager::getKeyDown(event))
				{
					gameData.waveIterator--;
				}
				break;
			default:
				break;
			}

			gameData.waveIterator++;
		}

#ifdef DEBUG
		sf::Event::KeyEvent kEvent;
		kEvent.code = sf::Keyboard::Tab;
		if (InputManager::getKeyDown(kEvent))
		{
		
		}
		else if (InputManager::getButtonDown("Test"))
		{
			gameData.waveIterator++;
		}
#endif // DEBUG
	}
	else
	{
		Master::master->changeMaster<Victory>(gameData.score);
	}
	gameData.waveStepCounter += Engine::smootherDeltaTime;
	if (InputManager::getQuit() || InputManager::getButtonDown("Exit"))
	{
		Master::master->changeMaster<PauseMenu>(gameData);
	}
}

Star::Star() : GameObject("Star", DisplayPosition::DISPLAYPOSITION_FIXED)
{	
	addSprite();
	animatedSprite->setSprite("staranimation.png");
	animatedSprite->setAnimation(Vector2(28, 25), 0.15, 9);
	setPosition(Vector2(Utility::getRandInt(0, Engine::getLogicalResolution().x), Utility::getRandInt(0, Engine::getLogicalResolution().y)));
	animatedSprite->setZIndex(-999);
}

Star::~Star()
{

}


