#include "stdafx.h"

using namespace GameEngine;

std::vector<WaveStep> Intro::listOfTheIntroText;

Intro::Intro(std::string lastMasterName) : Master("Intro")
{
	GUI::setStyle("null.png", "null.png", "null.png", "null.png", "visitor2.ttf");
	textIterator = 0;
	std::cout << "Intro start" << std::endl;
}

Intro::~Intro()
{
	int n = 0; n++;
}

void Intro::update()
{
	GUI::image(GUIImage{ "CommanderTalkBackground.png", Vector2(0, 0) });
	GUI::image(GUIImage{ "CommanderTalk.png", Vector2(0, 0) });
	GUI::image(GUIImage{ "CommanderTalkChatbar.png", { Rect(Engine::getLogicalResolution().x / 2, Engine::getLogicalResolution().y / 2 + 200, 0, 0) }, listOfTheIntroText[textIterator].msg });
	GUI::image(GUIImage{ "Arrow.png", Rect((1920 / 4) * 3 - 256, (1080 / 5) * 4 - 32, 512, 64) });
	if (GUI::hovering)
	{
		GUI::image(GUIImage{ "ArrowMarked.png", Vector2((1920 / 4) * 3 - 256, (1080 / 5) * 4 - 32)	});
	}
	else
	{
		GUI::image(GUIImage{ "Arrow.png", Vector2((1920 / 4) * 3 - 256, (1080 / 5) * 4 - 32) });
	}

	if (GUI::button(GUIButton{ { (1920 / 4) * 3 - 256, (1080 / 5) * 4 - 32, 512, 64 }, "", Vector2(0, 0) }))
	{
		textIterator++;
	}
	if (listOfTheIntroText.size() <= textIterator)
	{
		AudioManager::stopMusic("main menu theme 1a.ogg");
		destroy();
		Master::master->changeMaster<GameMaster>();
	}

}