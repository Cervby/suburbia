#pragma once

#include "stdafx.h"
#include "GameMaster.h"

using namespace GameEngine;

class Victory : public Master, public SaveScore
{
	double FireworksTimer;
	
	virtual void update() override;

public:
	unsigned long long score;

	Victory(std::string lastMasterName, unsigned long long score);
};
