#pragma once

#include "stdafx.h"

using namespace GameEngine;

class EMP : GameObject
{
private:
	double scaleSize;
	Vector2 toDir;
	double coolDown;
	double deathCounter;
	double explodeCounter;
	bool exploding;
	bool explodingTwo;

	virtual void update() override;

public:
	EMP(Vector2& toPos);
};
