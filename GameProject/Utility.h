#pragma once

#include "stdafx.h"

namespace GameEngine
{
	class Utility
	{
	private:
		static std::mt19937 rndGenerator;

	public:
		static int getRandInt(int min, int max);
		static double lerp(double start, double end, double t) { return (1 - t) * start + t * end; };
		static Vector2 worldToScreen(Vector2& v2);
		static Vector2 screenToWorld(Vector2& v2);
		static void init();
	};
}
