 

#include "stdafx.h"
#include "AudioManager.h"


namespace GameEngine
{
	std::unordered_map<std::string, sf::SoundBuffer*> AudioManager::sounds;
	std::unordered_map<std::string, sf::Music*> AudioManager::musics;
	std::deque<sf::Sound*> AudioManager::sfSounds;
	std::string AudioManager::currentMusic;
	bool AudioManager::mutedMusic;
	bool AudioManager::mutedSound;

	void AudioManager::muteAllMusic(bool setMuted)
	{
		mutedMusic = setMuted;
		if (setMuted)
		{
			for (auto music : musics)
			{
				music.second->setVolume(0);
			}
		}
		else
		{
			for (auto music : musics)
			{
				music.second->setVolume(100);
			}
		}
	}

	void AudioManager::muteAllSound(bool setMuted)
	{
		mutedSound = setMuted;
		if (setMuted)
		{
			for (auto sound : sfSounds)
			{
				sound->setVolume(0);
			}
		}
		else
		{
			for (auto sound : sfSounds)
			{
				sound->setVolume(100);
			}
		}
	}

	void AudioManager::init()
	{
		mutedMusic = false;
		mutedSound = false;

	}

	void AudioManager::playSound(std::string path, double pitch)
	{
		if (sounds.count(path) == 0) {
			sounds.insert(std::pair<std::string, sf::SoundBuffer*>(path, new sf::SoundBuffer()));
			if (!sounds.at(path)->loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + path))
			{
				std::cout << "Image: " + path + " not found.";
			}
		}
		if (sfSounds.size() > 16)
		{
			delete sfSounds.front();
			sfSounds.pop_front();
		}
		sfSounds.push_back(new sf::Sound(*sounds.at(path)));
		sfSounds.back()->setPitch(pitch);
		sfSounds.back()->play();
		if (mutedSound)
		{
			sfSounds.back()->setVolume(0);
		}
	}

	void AudioManager::pitchAll(double pitch)
	{
		for (auto music : musics)
		{
			music.second->setPitch(pitch);
		}
		for (auto sound : sfSounds)
		{
			sound->setPitch(pitch);
		}
	}

	void AudioManager::playMusic(std::string path)
	{
		if (musics.count(path) == 0) {
			musics.insert(std::pair<std::string, sf::Music*>(path, new sf::Music()));
			if (!musics.at(path)->openFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + path))
			{

				std::cout << "image: " + path + " not found.";
			}
		}
		musics.at(path)->setLoop(true);
		musics.at(path)->play();
		if (mutedMusic)
		{
			musics.at(path)->setVolume(0);
		}
	}

	void AudioManager::stopMusic(std::string path)
	{
		musics.at(path)->stop();
	}

	void AudioManager::pitchMusic(std::string path, double pitch)
	{
		musics.at(path)->setPitch(pitch);
	}

	void AudioManager::pauseMusic(std::string path)
	{
		musics.at(path)->pause();
	}
	
	double AudioManager::getVolumeMusic(std::string path)
	{
		return musics.at(path)->getVolume();

	}
	void AudioManager::setVolumeMusic(std::string path, double volume)
	{
		if (mutedMusic)
		{
			musics.at(path)->setVolume(0);
		}
		else
		{
			musics.at(path)->setVolume(volume);
		}
	}

	void AudioManager::stop()
	{
		for (auto music : musics)
		{
			music.second->stop();
		}
	}

	void AudioManager::quit()
	{
		for (auto sound : sounds)
		{
			delete sound.second;
		}
		for (auto music : musics)
		{
			delete music.second;
		}
		while (sfSounds.size() > 0)
		{
			delete sfSounds.front();
			sfSounds.pop_front();
		}
		sounds.clear();
		musics.clear();
	}
}
