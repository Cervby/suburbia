

#include "stdafx.h"
#include "InputManager.h"

namespace GameEngine
{
	Button::Button(char* iid, sf::Event eeventP)
	{
		id = iid;
		eventP = eeventP;
	}

	std::vector<sf::Event> InputManager::eventList;
	std::vector<sf::Event> InputManager::eventPressedList;
	char InputManager::textChar;

	std::vector<Button> InputManager::buttonList;

	Vector2 InputManager::lastMousePosition;

	sf::Event InputManager::event;

	void InputManager::frameInputUpdate()
	{
		textChar = '\0';
		lastMousePosition = getMousePosition();
		eventList.clear();
		while (RenderManager::window.pollEvent(event))
		{
			eventList.push_back(event);
			if (event.type == sf::Event::TextEntered)
			{
				if (event.text.unicode < 128)
				{
					textChar = static_cast<char>(event.text.unicode);
				}
			}
			if (event.type == sf::Event::KeyPressed || event.type == sf::Event::MouseButtonPressed)
			{
				eventPressedList.push_back(event);
			}
			else if (event.type == sf::Event::KeyReleased || event.type == sf::Event::MouseButtonReleased)
			{
				auto it = find_if(eventPressedList.begin(), eventPressedList.end(), [](sf::Event e){ return event.key.code == e.key.code; });
				if (it != eventPressedList.end())
				{
					eventPressedList.erase(it);
				}
			}
		}
	}

	void InputManager::quit() 
	{
		eventList.clear();
		eventPressedList.clear();
		buttonList.clear();
	}

	bool InputManager::getQuit()
	{
		for (unsigned int i = 0; i < eventList.size(); i++)
		{
			if (eventList[i].type == sf::Event::Closed)
			{
				return true;
			}
		}
		return false;
	}

	char InputManager::getText()
	{
		return textChar;
	}

	bool InputManager::getKeyDown(sf::Event::KeyEvent& keyEvent)
	{
		for (unsigned int i = 0; i < eventList.size(); i++)
		{
			if (
				(
					eventList[i].key.code == keyEvent.code ||
					(eventList[i].key.alt && keyEvent.alt) ||
					(eventList[i].key.control && keyEvent.control) ||
					(eventList[i].key.shift && keyEvent.shift) ||
					(eventList[i].key.system && keyEvent.system)
				) && eventList[i].type == sf::Event::KeyPressed)
			{
				return true;
			}
		}
		return false;
	}

	bool InputManager::getKeyUp(sf::Event::KeyEvent& keyEvent)
	{
		for (unsigned int i = 0; i < eventList.size(); i++)
		{
			if (
				(
				eventList[i].key.code == keyEvent.code ||
				(eventList[i].key.alt && keyEvent.alt) ||
				(eventList[i].key.control && keyEvent.control) ||
				(eventList[i].key.shift && keyEvent.shift) ||
				(eventList[i].key.system && keyEvent.system)
				) && eventList[i].type == sf::Event::KeyReleased)
			{
				return true;
			}
		}
		return false;
	}

	bool InputManager::getKeyPressed(sf::Event::KeyEvent& keyEvent) {
		for (unsigned int i = 0; i < eventPressedList.size(); i++)
		{
			if (
				eventPressedList[i].key.code == keyEvent.code ||
				(eventPressedList[i].key.alt && keyEvent.alt) ||
				(eventPressedList[i].key.control && keyEvent.control) ||
				(eventPressedList[i].key.shift && keyEvent.shift) ||
				(eventPressedList[i].key.system && keyEvent.system)
				)
			{
				return true;
			}
		}
		return false;
	}

	bool InputManager::getMouseDown(sf::Event::MouseButtonEvent& button)
	{
		for (unsigned int i = 0; i < eventList.size(); i++)
		{
			if (eventList[i].mouseButton.button == button.button && eventList[i].type == sf::Event::MouseButtonPressed)
			{
				return true;
			}
		}
		return false;
	}

	bool InputManager::getMouseUp(sf::Event::MouseButtonEvent& button)
	{
		for (unsigned int i = 0; i < eventList.size(); i++)
		{
			if (eventList[i].mouseButton.button == button.button && eventList[i].type == sf::Event::MouseButtonReleased)
			{
				return true;
			}
		}
		return false;
	}

	bool InputManager::getMousePressed(sf::Event::MouseButtonEvent& button)
	{
		for (unsigned int i = 0; i < eventPressedList.size(); i++)
		{
			if (eventPressedList[i].mouseButton.button == button.button)
			{
				return true;
			}
		}
		return false;
	}

	bool InputManager::getButtonDown(std::string id)
	{
		for (unsigned int i = 0; i < buttonList.size(); i++)
		{
			if (buttonList[i].id == id)
			{
				if (buttonList[i].eventP.type == sf::Event::KeyPressed)
				{
					return getKeyDown(buttonList[i].eventP.key);
				}
				else if (buttonList[i].eventP.type == sf::Event::MouseButtonPressed)
				{
					return getMouseDown(buttonList[i].eventP.mouseButton);
				}
			}
		}
		std::cout << "Error: Button ID \"" + id + "\" does not exist";
	}

	bool InputManager::getButtonUp(std::string id)
	{
		for (unsigned int i = 0; i < buttonList.size(); i++)
		{
			if (buttonList[i].id == id)
			{
				if (buttonList[i].eventP.type == sf::Event::KeyPressed)
				{
					return getKeyUp(buttonList[i].eventP.key);
				}
				else if (buttonList[i].eventP.type == sf::Event::MouseButtonPressed)
				{
					return getMouseUp(buttonList[i].eventP.mouseButton);
				}
			}
		}
		std::cout << "Error: Button ID \"" + id + "\" does not exist";
	}

	bool InputManager::getButtonPressed(std::string id)
	{
		for (unsigned int i = 0; i < buttonList.size(); i++)
		{
			if (buttonList[i].id == id)
			{
				if (buttonList[i].eventP.type == sf::Event::KeyPressed)
				{
					return getKeyPressed(buttonList[i].eventP.key);
				}
				else if (buttonList[i].eventP.type == sf::Event::MouseButtonPressed)
				{
					return getMousePressed(buttonList[i].eventP.mouseButton);
				}
			}
		}
		std::cout << "Error: Button ID \"" + id + "\" does not exist";
	}

	void InputManager::registerButton(char* id, sf::Event& inputEvent)
	{
		buttonList.push_back(Button(id, inputEvent));
	}

	Vector2 InputManager::getMousePosition()
	{
		sf::Vector2f v2 = RenderManager::window.mapPixelToCoords(sf::Mouse::getPosition(RenderManager::window));
		return Vector2(v2.x, v2.y);
	}

	Vector2 InputManager::getMousePositionFixed()
	{
		sf::Vector2i v2 = sf::Mouse::getPosition(RenderManager::window);
		return Vector2(v2.x, v2.y);
	}

	Vector2 InputManager::getMouseDelta()
	{
		Vector2 v2 = InputManager::getMousePosition();
		return Vector2(lastMousePosition.x - v2.x, lastMousePosition.y - v2.y);
	}

	bool InputManager::getAnyKeyDown(sf::Event& event)
	{
		for (unsigned int i = 0; i < eventList.size(); i++)
		{
			if (eventList[i].type == sf::Event::KeyPressed || eventList[i].type == sf::Event::MouseButtonPressed)
			{
				event = eventList[i];
				return true;
			}
		}
		return false;
	}
}
