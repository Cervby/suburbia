#pragma once

#include "stdafx.h"

using namespace GameEngine;

class Porter : public AlienShip
{
private:
	double A;
	double B;
	bool AnimationOn;

	void getPorterRegions(std::vector<Rect>& regions, bool backwards);
	virtual void shipMovement()override;
	virtual void deathMovement()override;

public:
	Porter(Vector2& toPos, std::string spriteName, int health);
};
