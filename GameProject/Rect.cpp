#include "stdafx.h"
#include "Rect.h"

namespace GameEngine
{
	Rect::Rect(int x, int y, int w, int h) : x(x), y(y), w(w), h(h) { }

	Rect::Rect() : x(0), y(0), w(0), h(0) { }

	Rect::Rect(const Rect &rect) : x(rect.x), y(rect.y), w(rect.w), h(rect.h) { }

	const std::string Rect::to_str() const
	{
		std::string Xstr = std::to_string(x);
		Xstr.erase(Xstr.find_last_not_of('0') + 1, std::string::npos);
		std::string::iterator it = Xstr.end() - 1;
		if (*it == '.')
		{
			Xstr.erase(it);
		}
		std::string Ystr = std::to_string(y);
		Ystr.erase(Ystr.find_last_not_of('0') + 1, std::string::npos);
		it = Ystr.end() - 1;
		if (*it == '.')
		{
			Ystr.erase(it);
		}
		std::string Wstr = std::to_string(w);
		Wstr.erase(Wstr.find_last_not_of('0') + 1, std::string::npos);
		it = Wstr.end() - 1;
		if (*it == '.')
		{
			Wstr.erase(it);
		}
		std::string Hstr = std::to_string(h);
		Hstr.erase(Hstr.find_last_not_of('0') + 1, std::string::npos);
		it = Hstr.end() - 1;
		if (*it == '.')
		{
			Hstr.erase(it);
		}

		return "{" + Xstr + ", " + Ystr + ", " + Wstr + ", " + Hstr + "}";
	}

	Rect Rect::operator=(const Rect& rect)
	{
		x = rect.x;
		y = rect.y;
		w = rect.w;
		h = rect.h;
		return *this;
	}

	Rect Rect::operator+(const Rect& rect) const
	{
		return{ x + rect.x, y + rect.y, w + rect.w, h + rect.h };
	}

	Rect Rect::operator-(const Rect& rect) const
	{
		return{ x - rect.x, y - rect.y, w - rect.w, h - rect.h };
	}

	Rect Rect::operator*(const Rect& rect) const
	{
		return{ x * rect.x, y - rect.y, w - rect.w, h - rect.h };
	}

	Rect Rect::operator/(const Rect& rect) const
	{
		return{ x / rect.x, y / rect.y, w / rect.w, h / rect.h };
	}

	Rect& Rect::operator+=(const Rect& rect)
	{
		x += rect.x;
		y += rect.y;
		w += rect.w;
		h += rect.h;
		return *this;
	}

	Rect& Rect::operator-=(const Rect& rect)
	{
		x -= rect.x;
		y -= rect.y;
		w -= rect.w;
		h -= rect.h;
		return *this;
	}

	Rect& Rect::operator*=(const Rect& rect)
	{
		x *= rect.x;
		y *= rect.y;
		w *= rect.w;
		h *= rect.h;
		return *this;
	}

	Rect& Rect::operator/=(const Rect& rect)
	{
		x /= rect.x;
		y /= rect.y;
		w /= rect.w;
		h /= rect.h;
		return *this;
	}

	Rect Rect::operator-() const
	{
		return { -x, -y, -w, -h };
	}

	bool Rect::operator==(const Rect& rect) const
	{
		return x == rect.x && y == rect.y && w == rect.w && h == rect.h;
	}

	bool Rect::operator!=(const Rect& rect) const
	{
		return !(x == rect.x && y == rect.y && w == rect.w && h == rect.h);
	}

	Rect Rect::operator*(const int& other) const
	{
		return{ x * other, y * other, w * other, h * other };
	}
	Rect Rect::operator/(const int& otherInt) const
	{
		return{ x / otherInt, y / otherInt, w / otherInt, h / otherInt };
	}
	Rect& Rect::operator*=(const int& otherInt)
	{
		x *= otherInt;
		y *= otherInt;
		w *= otherInt;
		h *= otherInt;
		return *this;
	}
	Rect& Rect::operator/=(const int& otherInt)
	{
		x /= otherInt;
		y /= otherInt;
		w /= otherInt;
		h /= otherInt;
		return *this;
	}

	bool Rect::operator<(const Rect& rect) const
	{
		return x < rect.x && y < rect.y && w < rect.w && h < rect.h;
	}
}
