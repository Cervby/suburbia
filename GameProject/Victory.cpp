#include "stdafx.h"

using namespace GameEngine;

Victory::Victory(std::string lastMasterName, unsigned long long score) : Master("Victory Screen"), score(score)
{
	AudioManager::stopMusic("gameplaytheme tempo.ogg");
	AudioManager::stopMusic("gameplaytheme.ogg");
	AudioManager::playMusic("win theme FINAL.ogg");
	GUI::setStyle("null.png", "null.png", "null.png", "null.png", "ethnocentric rg.ttf");
	for (GameObject* gameObject : GameObject::objectList)
	{
		if (gameObject->name != "Victory Screen" && gameObject->name != "Cursor")
		{
			gameObject->destroy();
		}

	}

	FireworksTimer = 0;
	new President();
	addSprite();
	animatedSprite->setSprite("WinScreen.png");
	setPosition(Utility::screenToWorld(Engine::getLogicalResolution() / 2));
	setRotation(static_cast<Camera*>(GameObject::find("Camera"))->currentRotation + 45);
}

void Victory::update()
{
	FireworksTimer += Engine::smootherDeltaTime * 20;
	if (FireworksTimer >= 4)
	{
		FireworksTimer = 0;
		GameObject* fireworks = new GameObject("Fireworks");
		fireworks->addSprite();
		fireworks->animatedSprite->setSprite("Fireworks sprite.png");
		fireworks->animatedSprite->setAnimation(Vector2(1040, 940), 0.06, 43);
		fireworks->setPosition(Vector2(Utility::getRandInt(0, Engine::getLogicalResolution().x), Utility::getRandInt(0, Engine::getLogicalResolution().y)));

		GameObject* fireworks2 = new GameObject("Fireworks2");
		fireworks2->addSprite();
		fireworks2->animatedSprite->setSprite("FireworkSprite2.png");
		fireworks2->animatedSprite->setAnimation(Vector2(1040, 940), 0.06, 43);
		fireworks2->setPosition(Vector2(Utility::getRandInt(0, Engine::getLogicalResolution().x), Utility::getRandInt(0, Engine::getLogicalResolution().y)));
	}
	SaveScore::savescore(score);
};