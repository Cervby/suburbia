#pragma once

#include "stdafx.h"

using namespace GameEngine;

struct Projectile : public GameObject
{
private:
	Vector2 toDir;
	double deathCounter;
	double explodeCounter;

	virtual void update() override;

public:
	bool exploding;

	Projectile(Vector2& toPos);
};

struct Trail : public GameObject
{
private:
	Vector2 toDir;
	double deathCounter;
	double explodeCounter;
	bool exploding;

	virtual void update() override;
	virtual void onCollision(GameObject* other) final;

public:
	Trail(Vector2& toPos);
};
