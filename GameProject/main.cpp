#include "stdafx.h"

using namespace GameEngine;

void setKeyBindings()
{
	sf::Event event;
	event.key.alt = false;
	event.key.control = false;
	event.key.shift = false;
	event.key.system = false;
	event.type = sf::Event::KeyPressed;
	event.key.code = sf::Keyboard::Escape;
	InputManager::registerButton("Exit", event);
	event.key.code = sf::Keyboard::A;
	InputManager::registerButton("Left", event);
	event.key.code = sf::Keyboard::D;
	InputManager::registerButton("Right", event);
	event.key.code = sf::Keyboard::W;
	InputManager::registerButton("Up", event);
	event.key.code = sf::Keyboard::S;
	InputManager::registerButton("Down", event);
	event.key.code = sf::Keyboard::Space;
	InputManager::registerButton("Power up", event);
	event.key.code = sf::Keyboard::Space;
	InputManager::registerButton("Menu Select", event);
	event.key.code = sf::Keyboard::Add;
	InputManager::registerButton("Test", event);
	event.key.code = sf::Keyboard::Unknown;
	event.type = sf::Event::MouseButtonPressed;
	event.mouseButton.button = sf::Mouse::Button::Left;
	InputManager::registerButton("Fire", event);
	event.mouseButton.button = sf::Mouse::Button::Right;
	InputManager::registerButton("Combo", event);
}

int main()//(int argc, char *argv[])
{
	Engine::init("Title", Settings{ Vector2(1920, 1080), Vector2(1, 1), true, Vector2(0, 0), "Planet", "Planet Suburbia" });
	GUI::setButtonBindings("Up", "Down", "Left", "Right", "Menu Select");
	GUI::setStyle("null.png", "null.png", "null.png", "null.png", "ethnocentric rg.ttf");

	RenderManager::window.setMouseCursorVisible(false);
	setKeyBindings();


	GUI::preloadAssets(
		std::vector<std::string>{
		"FinalHUD.png",
		"FirePower.png",
		"Shoot2.png",
		"blackholeprojectile.png",
		"null.png",
		"ComboBackgroundcolor.png",
		"ComboFront.png",
		"ComboBackgroundcolorReady.png",
		"Commander5.png",
		"NewChatBar.png",
		"TargetCrosshair.png",
		"empprojectile.png",
		"FinalHUD.png",
		"healthbarback.png",
		"Healthbar_best.png",
		"cyclos_128.png",
		"Shuttleship256.png",
		"zapdoser_128.png",
		"NY porter 128.png",
		"interceptor128.png",
		"Main menu background.png",
		"Logga main menu pause menu options menu.png",

		"Logotype256x192.png",
		"PauseMenu.png",
		"PauseMenu1.png",
		"PauseOptions.png",
		"PauseQuit.png",
		"PauseResume.png",


		
		"GameOverScreen.png",
		"WinScreen.png",
		"Escapepod_85.png",
		"Minimapunder.png",
		"location2.png",
		"location1.png",
		"location3.png",
		"location4.png",
		"Danger1.png",
		"Danger2.png",
		"Danger3.png",
		"Danger4.png",
		"MinimapFront3.png",
		"GPBicon.png",
		"empicon.png",
		"Linus_pickup.png",

		"PowerUpReady1.png",
		"PowerUpReady2.png",
		"GPBReady.png",
		"EmpReady.png",
		"NoPowerUp.png",
		"President.png",
		"Sector3Damaged.png",
		"sector2damaged .png",
		"Sector1Damaged.png",


		"Explode_dynamic_test.png",
		"Sector1Destroyed.png",
		
		"ComboPower.png",
		"PowerUp.png",
		"PlanetRoationButton.png",
		"Shuttleship damaged state 1 256.png",
		"Shuttleship damaged state 2 256.png",
		"AlienF.png",
		"ComboText.png",

		"Background.png",
		"logofinal.png",
		"CommanderTalkBackground.png",
		"CommanderTalkChatbar.png",
		"CommanderTalk.png",
		"CommanderTalkChatbar (1).png",
		"Bakgrund.png",
		"location2black.png"

	});
	AnimatedSprite::preloadAssets(
		std::vector<std::string>{
		"PortalSpritesheet.png",
		"Sector3Damaged.png",
		"sector2damaged .png",
		"Sector1Damaged.png",

		"Turretsprite.png",
		"image1.png",
		"BLACKHOLEPurpleSparkSpritesheet.png",
		"EMPfinal.png",
		"EMP Blue Spark Spritesheet.png",
		"BalloonsSpriteSheet normal.png",
		"EscapePodDeathSpritesheet.png",
		"ShuttleshipDeathSpritesheet.png",
		"CyclosDeathSprite.png",
		"ZapdoserDeathSheet.png",
		"PortalSpritesheet.png",
		"PorterDeathSpiteSheet.png",
		"InterceptorDeathSpritesheet.png",
		"Bullet sprite sheet.png",
		"Explode_dynamic.png",
		
		"slowtimeanimationspritesheet.png",
		"Crosshair2.png",
		"Crosshair3.png",
		"Crosshair4.png",
		"BackgroundBack.png",
		"Whiteatmosphere2.png",
		"CannonFront.png",
		"PlanetShield.png",
		"PlanetEarth.png",

		"GPBicon.png",
		"empicon.png",
		"Linus_pickup.png",
		"cyclos_128.png",
		"Shuttleship256.png",
		"zapdoser_128.png",
		"NY porter 128.png",
		"interceptor128.png",

		"empsmaller.png",
		"CyclosSparks.png",
		"InterceptorSparks.png",
		"PorterSparks.png",
		"ZapdoserSparks.png",
		"planetshieldanim4.png",

		"kugghjul plates and text.png",
		"Fireworks sprite.png",
		"FireworkSprite2.png",
		"Bakgrund.png",
		"Kugghjul.png",
		"TextUnmarked.png",
		"Metall.png",
		"cord.png",
		"skruv.png",
		"screw2.png",
		"scrap2.png",
		"window.png",
		"scrap.png",
		"Play.png",
		"staranimation.png",
		"BlueEMPsprite.png",
		"PurpleAnimaitonPowerupSpritesheet.png",
		"TargetCrosshair.png",
		"BulletTrail.png",
		"blackholeprojectile.png",
		"Explode_dynamic_test.png",
		"Sector1Destroyed.png"
	});

	Intro::listOfTheIntroText = 
	{
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "Sergeant, you are just in time.\nI trust you have been informed of our current situation?" },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "What is that now? You have not been briefed? \nI swear they get worse every time we make these rounds.\nI mean, where do they keep finding these people..." },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "No matter. Sergeant; The Mites are finally on the move. \nThey are preparing for an attack on a massive scale, \nright as we speak. \nThey have us surrounded." },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "All of our data so far points to this being their \nfinal push. Our intel has provided us \nwith a visual of our enemy.\nBe warned, it is not pretty." },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "Judging by that dumbfounded look on your face, \nyou have obviously got no idea what the mites are, do you?" },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "Of course not, you have been happily protected under our \nshield. Well guess what, the shield has been breached,\na large chunk of our planet has been fully exposed.\n" },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "The most destructive, malevolent alien race\nthat you can imagine are coming for us;\nAll of us." },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "This is the chance they have been waiting for.\nIt is up to us now Sergeant.\nThis is the moment we take charge of our own destiny." },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "This is where humanity and mites alike,\nmake our last stance.\nWe either come out of this as victors or not at all." },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "I have given you access to the most powerful and effective \nweaponof destruction the human race can muster:\nExterminator 3000X." },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "You are our last hope!" },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "Not because you are the most suitable\nBasically because there is nobody else" },


	};
	
	GameMaster::listOfTheEnemyWaves =
	{
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "Good luck Sergeant!" },


		{ "Fire" },
		{ "Left" },
		{ "Combo" },
		{ "Right" },
		{ "Power up" },
		{ EnemyType::ENEMYTYPE_NONE, 0, 14 },
		
		{ EnemyType::ENEMYTYPE_NONE, 0, 5 },


		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "bzz.. Sno ru! Ke..bzz..", "AlienF.png" },
	


		
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40, 5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 45, 0.1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 53, 0.5 },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 30, 4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 35, 0.1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 32, 0.5 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 5, },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 7 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 43, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 47, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 51, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 6 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 43, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 42, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 46, 0.3 },


		{ EnemyType::ENEMYTYPE_NONE, 0, 5 },

		{ EnemyType::ENEMYTYPE_TORNADO, 53, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 53, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 53, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50, 0.5 },
		

		{ EnemyType::ENEMYTYPE_NONE, 50, 1 },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 30, 10 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 35, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 30, 0.2 },

		{ EnemyType::ENEMYTYPE_NONE, 50, 1 },

		{ EnemyType::ENEMYTYPE_TORNADO, 45, 4 },
		{ EnemyType::ENEMYTYPE_TORNADO, 42, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 42, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 45, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 42, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 42, 0.5 },

		{ EnemyType::ENEMYTYPE_TORNADO, 65, 7 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 65, 0.5 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 6 },


		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "Excellent work Sergeant!\nIn fact, better than I could\never have dared hoped for." },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "There has been another breach\nin our defensive shield!" },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "Protect Earth, no matter the cost!" },

		{ EnemyType::ENEMYTYPE_NONE, 0, 1 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "Hexcyueaff�.dsxzxyyyxz�.bbzztt!", "AlienF.png" },

		//-------------- SECTOR 2 UNLOCK ----------------
		{ EnemyType::ENEMYTYPE_NONE, 0, 10 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 1 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 0 }, //Always put this before sector unlock :|
		{ 3 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 5 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 5 },
		
		
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 50, 2 },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 30, 4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 32, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 36, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 43, 0.3 },

		{ EnemyType::ENEMYTYPE_TORNADO, 55, 7 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 55, 1 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 55, 5 },

		{ EnemyType::ENEMYTYPE_TORNADO, 55 - 90, 7 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50 - 90, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 90, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 55 - 90, 1 },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 43, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 46, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 50, 0.3 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 5, },

		{ EnemyType::ENEMYTYPE_PORTER, 55 - 90, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 65 - 90, 2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 44, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 48, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 51, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55, 0.3 },
		


		{ EnemyType::ENEMYTYPE_NONE, 0, 10 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "You fight with honor Sergeant." },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "I hope you are ready for more!\nOur shield protecting sector 3\nis failing!" },
		{ EnemyType::ENEMYTYPE_NONE, 0, 4 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "BZZ...ka..ppa..bzz..", "AlienF.png" },
		
		{ EnemyType::ENEMYTYPE_NONE, 0, 2 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 0 }, //Always put this before sector unlock :|
		{ 2 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 45, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 60, 1 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 55 - 90, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 55 - 90, 7 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50 - 90, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 90, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 55 - 90, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40 - 90, 2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 46 - 90, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 54 - 90, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 59 - 90, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 65 - 90, 0.3 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 5 },

		{ EnemyType::ENEMYTYPE_PORTER, 45 - 180, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 60, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50, 3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 60, 1 },
		
		
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40 - 180, 4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55 - 180, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55 - 180, 1 },
		
		{ EnemyType::ENEMYTYPE_TORNADO, 45, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 35, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50, 2 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 30 - 90, 4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 35 - 90, 0.1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40 - 90, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 32 - 90, 0.5 },
		
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 40, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 45 - 180, 4 },
		{ EnemyType::ENEMYTYPE_TORNADO, 35 - 180, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50 - 180, 2 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50 - 180, 1 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 15 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "Wait! YOU ARE the sergeant?\nOur last hope?", "President.png" },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "Never mind, this planet is going to hell!\nScrew this!", "President.png" },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "They are coming Sergeant!\nGive em hell!" },
		{ EnemyType::ENEMYTYPE_NONE, 0, 2 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "Marklar..bzz..", "AlienF.png" },
		{ EnemyType::ENEMYTYPE_NONE, 0, 4 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 0 }, //Always put this before sector unlock :|
		{ 1 },
		{ "gameplaytheme.ogg", true },
		{ EnemyType::ENEMYTYPE_NONE, 0, 10 },
		

		{ EnemyType::ENEMYTYPE_SHUTTLESHIP, 25 + 90, 1 },

		
		
		{ EnemyType::ENEMYTYPE_TORNADO, 45 + 90, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 35 + 90, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50 + 90, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50 + 90, 0.5 },
		


		{ EnemyType::ENEMYTYPE_NONE, 0, 3 },

		{ EnemyType::ENEMYTYPE_TORNADO, 20, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 25, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 18, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 23, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 15, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 25, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 45 - 180, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 55 - 180, 0.4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 50 - 180, 0.4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 60 - 180, 0.4 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 75, 1 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 3 },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0.5 },
		{ EnemyType::ENEMYTYPE_PORTER, 30 - 90, 0 },
		{ EnemyType::ENEMYTYPE_PORTER, 35 - 90, 0.5 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 60, 0 },
		
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 20, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 30 - 90, 0 },
		{ EnemyType::ENEMYTYPE_PORTER, 35 - 90, 0.5 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 60, 0 },
		
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 20, 4 },
		{ EnemyType::ENEMYTYPE_SHUTTLESHIP, 40 - 90, 2 },
		{ EnemyType::ENEMYTYPE_NONE, 0, 3 },

		{ EnemyType::ENEMYTYPE_PORTER, 40, 2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40 + 90, 5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40 + 90, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 45 + 90, 0.1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 53 + 90, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 20 + 90, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 25 + 90, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 18 + 90, 1 },
		{ EnemyType::ENEMYTYPE_SHUTTLESHIP, 50 - 180, 7 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 20 + 90, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 25 + 90, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 45 + 90, 1 },
		{ EnemyType::ENEMYTYPE_SHUTTLESHIP, 50 - 180, 7 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 4 },

		{ EnemyType::ENEMYTYPE_ZAPDOSER, 30, 2 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 30, 5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 30, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40, 0.1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40, 0.5 },
		{ EnemyType::ENEMYTYPE_PORTER, 50, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 50, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 60, 1 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 60, 3 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 4 },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 35, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 45, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 30, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 50, 2 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 4 },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 85, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 80, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 75, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 70, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 65, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 60, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 55, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 50 + 90, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 45 + 90, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40 + 90, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 35 + 90, 0.2 },
		{ EnemyType::ENEMYTYPE_TORNADO, 64 + 180, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 59 + 180, 0.2 },
		{ EnemyType::ENEMYTYPE_TORNADO, 54 + 180, 0.2 },
		{ EnemyType::ENEMYTYPE_TORNADO, 49 + 180, 0.3 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 5, },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 30, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 25, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 20, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 15, 0 },
		{ EnemyType::ENEMYTYPE_SHUTTLESHIP, 40 - 90, 2 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 90, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 90, 2 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 90, 3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 21, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 25, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 20, 1.5 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 4, },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 85 - 180, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 80 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 75 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_PORTER, 50 - 90, 4 },
		{ EnemyType::ENEMYTYPE_PORTER, 45 - 90, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 40 - 90, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 35 - 90, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 70 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 65 - 180, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 60 - 180, 2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 55 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 50, 2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 45, 1 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 5, },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 50, 3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 45, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 50, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 43, 2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 60, 0.5 },
		{ EnemyType::ENEMYTYPE_PORTER, 50 - 90, 4 },
		{ EnemyType::ENEMYTYPE_PORTER, 45 - 90, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 40 - 90, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 35 - 90, 1 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 30 - 180, 4 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 30 - 180, 4 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 30 - 180, 2 },
		{ EnemyType::ENEMYTYPE_SHUTTLESHIP, 50 - 180, 5 },
		
		
		{ EnemyType::ENEMYTYPE_NONE, 0, 6, },
		
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1.5 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1.5 },
		{ EnemyType::ENEMYTYPE_PORTER, 25, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 35, 2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 30 + 90, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 35 + 90, 0.1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40 + 90, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 32 + 90, 0.5 },
		{ EnemyType::ENEMYTYPE_PORTER, 35 - 90, 5 },
		{ EnemyType::ENEMYTYPE_PORTER, 45 - 90, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 35 - 90, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 45 - 90, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 50, 3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 45, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 50, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55, 0.5 },
		{ EnemyType::ENEMYTYPE_PORTER, 35 + 90, 5 },
		{ EnemyType::ENEMYTYPE_PORTER, 45 + 90, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 35 + 90, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 45 + 90, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 35 - 180, 5 },
		{ EnemyType::ENEMYTYPE_PORTER, 45 - 180, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 25 - 180, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 45 - 180, 1 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 6, },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 85 + 90, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 80 + 90, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 75 + 90, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 70 + 90, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 65 + 90, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 60 + 90, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 55, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 50, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 45, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0.5 },
		{ EnemyType::ENEMYTYPE_SHUTTLESHIP, 40 + 90, 15 },
		{ "gameplaytheme.ogg", true },
		{ EnemyType::ENEMYTYPE_NONE, 0, 5, },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 35, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 30, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 25, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 20, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 15, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 90, 3 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 90, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 90, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 21 - 90, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 26 - 90, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 27 - 90, 0.5 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 6, },

		{ EnemyType::ENEMYTYPE_PORTER, 45, 2 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 2 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 35, 2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 85 - 180, 4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 80 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 75 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 70 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 65 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 60 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 55 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_PORTER, 40, 2 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 2 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 35, 2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 35 - 180, 4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 55 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 45 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 30 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 55 - 180, 0.5 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 5, },

		
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 30 - 180, 10 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 30 - 180, 3 },
		{ EnemyType::ENEMYTYPE_SHUTTLESHIP, 20 - 180, 7 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 40 + 90, 4 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 40 + 90, 4 },
		{ EnemyType::ENEMYTYPE_PORTER, 40, 2 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 2 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 35, 2 },
		{ EnemyType::ENEMYTYPE_SHUTTLESHIP, 40 - 90, 1 },
		
		{ EnemyType::ENEMYTYPE_NONE, 0, 6, },

		{ EnemyType::ENEMYTYPE_ZAPDOSER, 20, 3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 65, 2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 60, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 45, 0.5 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 40, 3 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 20, 3 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 + 90, 0 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 + 90, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 + 90, 0.3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 65, 2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 60, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 45, 0.5 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 40, 3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 65 - 180, 0.5 },
		
	

		{ EnemyType::ENEMYTYPE_NONE, 0, 5, },

		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 65, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 60, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 45, 0.5 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 60, 3 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 90, 5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 90, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 90, 2 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 30 - 180, 4 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 3, },

		{ EnemyType::ENEMYTYPE_PORTER, 45, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 2 },
		{ EnemyType::ENEMYTYPE_PORTER, 45, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 25, 2 },
		{ EnemyType::ENEMYTYPE_PORTER, 45 - 180, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 55 - 180, 2 },
		{ EnemyType::ENEMYTYPE_PORTER, 60 - 180, 1 },
		{ EnemyType::ENEMYTYPE_PORTER, 45 - 180, 2 },
		{ EnemyType::ENEMYTYPE_PORTER, 25 - 180, 1 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 6, },

		{ EnemyType::ENEMYTYPE_SHUTTLESHIP, 20 + 90, 5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 35, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 30, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 25, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 20, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 15, 0 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60, 3 },
		{ EnemyType::ENEMYTYPE_TORNADO, 40, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 35, 0.5 },
		{ EnemyType::ENEMYTYPE_SHUTTLESHIP, 20 - 90, 5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 56, 2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 50, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 45, 0 },
		
		{ EnemyType::ENEMYTYPE_NONE, 0, 5, },

		{ EnemyType::ENEMYTYPE_TORNADO, 60, 2 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60, 1 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 30 - 180, 5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 180, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 30 - 180, 3 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 180, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 - 180, 0.5 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60 + 90, 3 },
		{ EnemyType::ENEMYTYPE_TORNADO, 40 + 90, 3 },
		{ EnemyType::ENEMYTYPE_SHUTTLESHIP, 40, 3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 35, 0.4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 30, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 25, 0.2 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 20, 0.4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 25, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 50 + 90, 3 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 5, },
		
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0.5 },
		{ EnemyType::ENEMYTYPE_PORTER, 30 - 90, 0 },
		{ EnemyType::ENEMYTYPE_PORTER, 35 - 90, 0.5 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 60, 0 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 30, 4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 20, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0.5 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 40, 0.5 },
		{ EnemyType::ENEMYTYPE_PORTER, 30 - 90, 0 },
		{ EnemyType::ENEMYTYPE_PORTER, 35 - 90, 0.5 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 60, 0 },
		{ EnemyType::ENEMYTYPE_ZAPDOSER, 30, 4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 20, 1 },
		{ "gameplaytheme tempo.ogg", true },

		{ EnemyType::ENEMYTYPE_NONE, 0, 3, },

		{ EnemyType::ENEMYTYPE_ZAPDOSER, 55, 3 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_LEFT, 40 - 180, 4 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55 - 180, 1 },
		{ EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT, 55 - 180, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 45, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 35, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60, 2 },
		{ EnemyType::ENEMYTYPE_TORNADO, 53, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 45, 1 },

		{ EnemyType::ENEMYTYPE_TORNADO, 45, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 35, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 60, 2 },
		{ EnemyType::ENEMYTYPE_TORNADO, 53, 1 },
		{ EnemyType::ENEMYTYPE_TORNADO, 45, 1 },

		{ EnemyType::ENEMYTYPE_NONE, 0, 10, },
		


		{ EnemyType::ENEMYTYPE_NONE, 0, 10000000, true, "Dunken master biatch", "AlienF.png" }

	};

	Master::master->changeMaster<MainMenu>();
	new Cursor;

	GUI::setStyle("null.png", "null.png", "null.png", "null.png", "Maya Culpa.ttf");
	Engine::run();
	return 0;
}
