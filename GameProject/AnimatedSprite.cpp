

#include "stdafx.h"
#include "AnimatedSprite.h"

namespace GameEngine
{
	std::unordered_map<std::string, sf::Texture> AnimatedSprite::spriteTextures;


	AnimatedSprite::AnimatedSprite(GameObject* gameObject) : gameObject(gameObject)
	{
		RenderManager::renderOrder.insert(std::pair<signed int, GameObject*>(0, gameObject));
		sprite = nullptr;
		aboveAll = false;
		color = sf::Color::White;
		flip = Flip::FLIP_NONE;
		index = 0;
	}

	void AnimatedSprite::setZIndex(float zIndex)
	{
		for (auto it = RenderManager::renderOrder.begin(); it != RenderManager::renderOrder.end(); it++)
		{
			if (it->second == gameObject)
			{
				RenderManager::renderOrder.erase(it);
				break;
			}
		}
		RenderManager::renderOrder.insert(std::pair<float, GameObject*>(zIndex, gameObject));
	}

	void AnimatedSprite::setTint(sf::Color& color)
	{
		this->color = color;
	}

	AnimatedSprite::~AnimatedSprite()
	{
		regions.clear();
		for (std::multimap<float, GameObject*>::iterator it = RenderManager::renderOrder.begin(); it != RenderManager::renderOrder.end();)
		{
			std::multimap<float, GameObject*>::iterator erase_iter = it++;
			if (erase_iter->second == gameObject)
			{
				RenderManager::renderOrder.erase(erase_iter);
				break;
			}
		}
		delete sprite;
		sprite = nullptr;
	}

	void AnimatedSprite::preloadAssets(std::vector<std::string>& paths)
	{
		float i = 0;
		for (std::string path : paths)
		{
			InputManager::frameInputUpdate();
			i++;
			GUI::image(GUIImage{ "mainmenubackground.png", Vector2(0, 0) });
			float time = Engine::clock.getElapsedTime().asSeconds();
			GUI::image(GUIImage{ "Logotype.png", Vector2(0, 0) });
			GUI::box(GUIBox{ Rect(0, 300, 1920, 1080), "Loading.." });
			GUI::box(GUIBox{ Rect(0, 400, 1920, 1080), std::to_string(50 + static_cast<int>((i / paths.size()) * 50)) + '%'
		});
			RenderManager::render();
			if (spriteTextures.count(path) == 0) {
				spriteTextures.insert(std::pair<std::string, sf::Texture>(path, sf::Texture()));
				if (!spriteTextures.at(path).loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + path))
				{
					std::cout << "image: " + path + " not found.";
				}
			}
		}
		GUI::box(GUIBox{ { 0, 0, 1920, 1080 }, "Welcome to Planet Suburbia!" });
		RenderManager::render();
	}

	void AnimatedSprite::render()
	{
		Vector2 pos = gameObject->getPosition();
		if (gameObject->displayPosition == DisplayPosition::DISPLAYPOSITION_FIXED)
		{
			pos = Utility::screenToWorld(Vector2(pos.x, pos.y));
		}
		Vector2 realPos = Utility::worldToScreen(Vector2(pos.x, pos.y));
	

		if (regions.size() > 0)
		{
			if (!paused) counter += Engine::smootherDeltaTime;
			if (counter >= duration) {
				counter = 0;
				if (index < regions.size() - 1)
				{
					index++;
				}
				else
				{
					index = 0;
				}
			}
			region = regions.at(index);
		}

		sprite->setOrigin(region.w / 2, region.h / 2);
		sprite->setPosition(pos.x, pos.y);
		Vector2 scale = gameObject->getScale();
		sprite->setScale(scale.x, scale.y);
		if (gameObject->displayPosition == DisplayPosition::DISPLAYPOSITION_FIXED)
		{
			sprite->setRotation(RenderManager::window.getView().getRotation() + gameObject->getRotation());
		}
		else
		{
			sprite->setRotation(gameObject->getRotation());
		}
		sprite->setColor(color);

		sf::Vector2u size = spriteTextures.at(spritePath).getSize();
	

		if (flip == Flip::FLIP_NONE)
		{
			sprite->setTextureRect(sf::IntRect(region.x, region.y, region.w, region.h));
		}
		else if (flip == Flip::FLIP_HORIZONTAL)
		{
			sprite->setTextureRect(sf::IntRect(region.x + region.w, region.y, -region.w, region.h));
		}
		else if (flip == Flip::FLIP_VERTICAL)
		{
			sprite->setTextureRect(sf::IntRect(region.x, region.y + region.h, region.w, -region.h));
		}
		else if (flip == Flip::FLIP_BOTH)
		{
			sprite->setTextureRect(sf::IntRect(region.x + region.w, region.y + region.h, -region.w, -region.h));
		}
		else
		{

			std::cout << "Invalid flip";
		}

		if (gameObject->name == "Cursor")
		{
			int a = 0;
		}

		RenderManager::window.draw(*sprite);
	}

	void AnimatedSprite::setSprite(std::string path)
	{
		if (spriteTextures.count(path) == 0) {
			spriteTextures.insert(std::pair<std::string, sf::Texture>(path, sf::Texture()));
			if (!spriteTextures.at(path).loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + path))
			{

				std::cout << "image: " + path + " not found.";
			}
			std::cout << "Warning! Sprite \"" + path + "\" was not preloaded to AnimatedSprite" << std::endl;
		}
		if (sprite != nullptr)
		{
			delete sprite;
		}
		sprite = new sf::Sprite();
		sprite->setTexture(spriteTextures.at(path));
		sf::FloatRect wh = sprite->getGlobalBounds();
		sprite->setOrigin(wh.width, wh.height);
		sprite->setRotation(gameObject->getRotation());
		region.x = 0;
		region.y = 0;
		region.w = wh.width;
		region.h = wh.height;
		spritePath = path;
		Collider* collider = gameObject->collider;
		if (collider != nullptr)
		{
			collider->setRegionFromSprite();
		}
	}

	void AnimatedSprite::quit()
	{
		spriteTextures.clear();
	}

	void AnimatedSprite::setAnimationFree(std::vector<Rect>& newRegions, float newDuration, Flip flip)
	{
		index = 0;
		regions = newRegions;
		duration = newDuration;
		this->flip = flip;
		paused = false;
	}

	void AnimatedSprite::setAnimation(Vector2& spriteSize, float newDuration, unsigned int count, unsigned int startPos, Flip flip)
	{
		if (sprite == nullptr)
		{
			std::cout << "Sprite must be set before animation.";
		}
		index = 0;

		regions.clear();
		sf::IntRect wh = sprite->getTextureRect();
		unsigned int xCount = wh.width / spriteSize.x;
		unsigned int yCount = wh.height / spriteSize.y;
		for (unsigned int y = 0; y < yCount; y++)
		{
			for (unsigned int x = 0; x < xCount; x++)
			{
				if (x * y < startPos)
				{
					continue;
				}
				else if (x * y + startPos >= count)
				{
					break;
				}
				regions.push_back(Rect(x * spriteSize.x, y * spriteSize.y, spriteSize.x, spriteSize.y));
			}
		}

		duration = newDuration;
		this->flip = flip;
		paused = false;
	}
}
