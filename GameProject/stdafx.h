#pragma once

#define DEBUG

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#endif

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <iostream>

#include <stdint.h>
#include <map>
#include <unordered_map>
#include <string>
#include <vector>
#include <unordered_set>
#include <deque>
#include <queue>
#include <cmath>
#include <random>
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>
#include <array>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <fstream>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "CommonTools.h"

#include "Vector2.h"
#include "GameObject.h"
#include "InputManager.h"
#include "Rect.h"
#include "GUI.h"
#include "Collider.h"
#include "AnimatedSprite.h"
#include "AudioManager.h"
#include "RenderManager.h"
#include "Engine.h"
#include "Camera.h"
#include "Utility.h"

#include "Master.h"
#include "SaveScore.h"
#include "Victory.h"
#include "MainMenu.h"
#include "PauseMenu.h"
#include "Intro.h"
#include "Options.h"
#include "HighScore.h"
#include "GameOver.h"
#include "GameMaster.h"

#include "Scrap.h"
#include "Projectile.h"
#include "Cursor.h"
#include "Planet.h"
#include "Camera.h"
#include "AlienShip.h"
#include "HUD.h"
#include "Turret.h"
#include "Barrel.h"
#include "Minimap.h"
#include "Space.h"
#include "EMP.h"
#include "Sector.h"
#include "Commander.h"
#include "PowerUpBar.h"
#include "BlackHole.h"
#include "President.h"
#include "SlowTime.h"
#include "PickUp.h"
#include "ComboMeter.h"
#include "Combo.h"
#include "SectorShield.h"
#include "Tutorial.h"
#include "EscapePod.h"
#include "Interceptor.h"
#include "Porter.h"
#include "ShuttleShip.h"
#include "Porter.h"
#include "Cyclos.h"
#include "Zapdoser.h"
