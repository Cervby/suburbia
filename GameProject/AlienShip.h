#pragma once

enum class AlienShipDeathSource
{
	ALIENSHIPDEATHSOURCE_INVALID,
	ALIENSHIPDEATHSOURCE_PROJECTILE,
	ALIENSHIPDEATHSOURCE_PLANET,
	ALIENSHIPDEATHSOURCE_PLANET_SHIELD,
	ALIENSHIPDEATHSOURCE_ALIENSHIP
};

class AlienShip : public GameObject
{
private:
	double explodingCounter;
	double Ypos;

	bool exploding;

	double rotation;
	int health;
	Vector2 CollPos;

	PlayerPowerUp PowerUp;
	double yAcc;
	double alpha;


	virtual void update() final;
	virtual void onCollision(GameObject* other) final;


	virtual void shipMovement() = 0;
	virtual void deathMovement() = 0;

protected:

	double Angle;
	double WhichSounds;
	Vector2 toPos;
	bool deathAnimationTrigger;
	double EMPcounter;


public:
	bool EnemyHit;
	AlienShipDeathSource deathSource;
	double deathCounter;
	bool checkedForShieldCollision;

	AlienShip(Vector2& toPos, std::string spriteName, int health, PlayerPowerUp powerUp = PlayerPowerUp::POWER_UP_NONE);
};
