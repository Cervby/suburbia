#pragma once

#include "stdafx.h"

using namespace GameEngine;


class Cyclos : public AlienShip
{
private:
	virtual void shipMovement() override;
	virtual void deathMovement() override;

public:
	Cyclos(Vector2& toPos, std::string spriteName, int health, PlayerPowerUp powerUp);
};
