#pragma once

#include "stdafx.h"

namespace GameEngine
{
	enum class Flip
	{
		FLIP_NONE,
		FLIP_HORIZONTAL,
		FLIP_VERTICAL,
		FLIP_BOTH
	};

	class AnimatedSprite
	{
	private:
		std::string spritePath;
		Rect region;
		std::vector<Rect> regions;
		float duration;
		unsigned int index;
		double counter;
		Flip flip;
		sf::Color color;
		GameObject* gameObject;
	public:
		bool paused;

	private:
		sf::Sprite* sprite;
		bool aboveAll;

		static std::unordered_map<std::string, sf::Texture> spriteTextures;

	public:
		AnimatedSprite(GameObject* gameObject);
		~AnimatedSprite();
		static void preloadAssets(std::vector<std::string>& paths);
		void setZIndex(float zIndex);
		void setTint(sf::Color& color);
		void setAboveAll(bool aboveAll) { this->aboveAll = aboveAll; }
		bool getAboveAll() { return aboveAll; }
		void render();
		void setSprite(std::string path);
		static void quit();
		void setAnimationFree(std::vector<Rect>& newRegions, float newDuration, Flip flip = Flip::FLIP_NONE);
		void setAnimation(Vector2& spriteSize, float newDuration, unsigned int count, unsigned int startPos = 0, Flip flip = Flip::FLIP_NONE);
		Rect& getRegion() { return region; }
		sf::Sprite* getSfSprite() { return sprite; }
		void clearRegions() { regions.clear(); };
	};
}
