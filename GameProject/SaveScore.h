#pragma once

#include "stdafx.h"

using namespace GameEngine;

class SaveScore
{
private:
	static std::string input;

public:
	static void savescore(unsigned long long score);
};
