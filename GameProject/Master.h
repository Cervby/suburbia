#pragma once
#include "stdafx.h"

using namespace GameEngine;

enum class PlayerPowerUp;

class Master : public GameObject
{
public:
	std::string lastMasterName;
	std::string nextMasterName;

	static Master* master;

	Master(std::string name) : GameObject(name) {};
	virtual ~Master() {};

	static void changeMaster();

	template<class Type, typename... Args> void changeMaster(Args... newMaster);
};

template<class Type, typename... Args> void Master::changeMaster(Args... args)
{
	Type* newMaster;
	if (Master::master != nullptr)
	{
		newMaster = new Type(name, args...);
		newMaster->lastMasterName = name;
		newMaster->nextMasterName = "";
		nextMasterName = newMaster->name;
		destroy();
	}
	else
	{
		newMaster = new Type("", args...);
		newMaster->lastMasterName = "";
	}
	Master::master = newMaster;
}
