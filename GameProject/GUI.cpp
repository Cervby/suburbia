

#include "stdafx.h"
#include "GUI.h"

namespace GameEngine
{
	sf::Texture GUI::boxTexture;
	sf::Texture GUI::buttonTexture;
	sf::Texture GUI::buttonTexturePress;
	sf::Texture GUI::buttonTextureSelect;
	sf::Texture GUI::fontTexture;
	std::vector<sf::Sprite> GUI::sprites;
	std::unordered_map<std::string, sf::Texture> GUI::textures;

	GUIFont GUI::spriteFont;
	sf::Font* GUI::sfFont;
	std::vector<GUIBox> GUI::boxes;
	std::vector<GUIButton> GUI::buttons;
	std::vector<GUIImage> GUI::images;
	std::string GUI::bindingSelectUp;
	std::string GUI::bindingSelectDown;
	std::string GUI::bindingSelectLeft;
	std::string GUI::bindingSelectRight;
	std::string GUI::bindingSelectPress;
	bool GUI::bPressOn;

	Vector2 GUI::selection;
	bool GUI::hovering;
	bool GUI::dontUseMouse;

	void GUI::renderFont(Rect& rect, std::string value)
	{
		Vector2 textOffset = Vector2(0, 0);
		int fontTextureWidth;
		fontTextureWidth = fontTexture.getSize().x;
		int fontXCount = fontTextureWidth / (spriteFont.fontSize.x + spriteFont.fontPadding.x);

		Vector2 max = { 0, 1 };
		int curMaxX = 0;
		for (char ch : value)
		{
			if (ch == '\n')
			{
				max.x = curMaxX > max.x ? curMaxX : max.x;
				curMaxX = 0;
				max.y++;
			}
			else {
				curMaxX++;
			}
		}
		max.x = curMaxX > max.x ? curMaxX : max.x;
		Vector2 fontBoxCenterOffset = ((spriteFont.fontSize + spriteFont.destinationPadding) * max / 2) - Vector2(rect.w, rect.h) / 2;

		for (char ch : value)
		{
			if (ch == '\n')
			{
				textOffset.x = 0;
				textOffset.y += spriteFont.fontSize.y + spriteFont.destinationPadding.y;
				continue;
			}
			else if (ch == '\r')
			{
				textOffset.x = 0;
				continue;
			}
			if (spriteFont.isAllUpper)
			{
				ch = toupper(ch);
			}
			int fontIndex = spriteFont.layout.find(ch);
			if (spriteFont.layout.find(ch) == std::string::npos)
			{
				std::cout << "Character: '" + ch + ("' in the string \"" + value + "\" is not allowed.");
			}
			Rect tmpsource = Rect(fontIndex % fontXCount * spriteFont.fontSize.x + fontIndex % fontXCount * spriteFont.fontPadding.x, fontIndex / fontXCount * spriteFont.fontSize.y + fontIndex / fontXCount * spriteFont.fontPadding.y, spriteFont.fontSize.x, spriteFont.fontSize.y);
			Rect tmpDest = Rect((rect.x + textOffset.x) * spriteFont.scale.x - fontBoxCenterOffset.x, (rect.y + textOffset.y) * spriteFont.scale.y - fontBoxCenterOffset.y, spriteFont.fontSize.x * spriteFont.scale.x, spriteFont.fontSize.y * spriteFont.scale.x);

			sprites.push_back(sf::Sprite());
			sprites.back().setTexture(fontTexture);
			sprites.back().setTextureRect(sf::IntRect(tmpsource.x, tmpsource.y, tmpsource.w, tmpsource.h));
			sprites.back().setScale(sf::Vector2f(spriteFont.scale.x, spriteFont.scale.y));
			sprites.back().setPosition(sf::Vector2f(tmpDest.x, tmpDest.y));
			RenderManager::window.draw(sprites.back());

			textOffset.x += spriteFont.fontSize.x + spriteFont.destinationPadding.x;
			if (textOffset.x > rect.w - (spriteFont.fontSize.y))
			{
				textOffset.x = 0;
				textOffset.y += spriteFont.fontSize.y + spriteFont.destinationPadding.y;
			}
		}
	}

	void GUI::preloadAssets(std::vector<std::string>& paths)
	{
		float i = 0;
		for (std::string path : paths)
		{
			InputManager::frameInputUpdate();
			i++;
			GUI::image(GUIImage{ "mainmenubackground.png", Vector2(0, 0) });
			float time = Engine::clock.getElapsedTime().asSeconds();
			GUI::image(GUIImage{ "Logotype.png", Vector2(0, 0) });
			GUI::box(GUIBox{ { 0, 300, 1920, 1080 }, "Loading.." });
			GUI::box(GUIBox{ { 0, 400, 1920, 1080 }, std::to_string(static_cast<int>((i / paths.size()) * 50)) + '%' });
			RenderManager::render();
			if (textures.count(path) == 0)
			{
				textures[path] = sf::Texture();
				if (!textures[path].loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + path))
				{
					std::cout << "image: " + path + " not found.";
				}
			}
		}
	}

	void GUI::setButtonBindings(std::string up, std::string down, std::string left, std::string right, std::string press)
	{
		bindingSelectUp = up;
		bindingSelectDown = down;
		bindingSelectLeft = left;
		bindingSelectRight = right;
		bindingSelectPress = press;
	}

	void GUI::setStyle(std::string boxPath, std::string buttonPath, std::string buttonPressPath, std::string buttonSelectPath, GUIFont& font)
	{
		spriteFont = font;
		if (!fontTexture.loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + spriteFont.texturePath))
		{

			std::cout << "font: " + spriteFont.texturePath + " not found.";
		}

		if (!boxTexture.loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + boxPath))
		{
			std::cout << "font: " + spriteFont.texturePath + " not found.";
		}
		if (!buttonTexture.loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + buttonPath))
		{
			std::cout << "font: " + spriteFont.texturePath + " not found.";
		}
		if (!buttonTexturePress.loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + buttonPressPath))
		{
			std::cout << "font: " + spriteFont.texturePath + " not found.";
		}
		if (!buttonTextureSelect.loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + buttonSelectPath))
		{
			std::cout << "font: " + spriteFont.texturePath + " not found.";
		}
	}

	void GUI::setStyle(std::string boxPath, std::string buttonPath, std::string buttonPressPath, std::string buttonSelectPath, std::string fontPath)
	{
		sfFont = new sf::Font();
		if (!sfFont->loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + fontPath))
		{
			std::cout << "font: " + spriteFont.texturePath + " not found.";
		}

		if (!boxTexture.loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + boxPath))
		{
			std::cout << "font: " + spriteFont.texturePath + " not found.";
		}
		if (!buttonTexture.loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + buttonPath))
		{
			std::cout << "font: " + spriteFont.texturePath + " not found.";
		}
		if (!buttonTexturePress.loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + buttonPressPath))
		{
			std::cout << "font: " + spriteFont.texturePath + " not found.";
		}
		if (!buttonTextureSelect.loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + buttonSelectPath))
		{
			std::cout << "font: " + spriteFont.texturePath + " not found.";
		}
	}

	void GUI::box(GUIBox& item)
	{
		boxes.push_back(item);
	}

	void GUI::image(GUIImage& item)
	{
		images.push_back(item);
		if (textures.count(item.path) == 0)
		{
			textures[item.path] = sf::Texture();
			if (!textures[item.path].loadFromFile("../assets/" + Engine::getSettings().assetsFolder + "/" + item.path))
			{
				std::cout << "Image: " + spriteFont.texturePath + " not found.";
			}
			std::cout << "Warning! Sprite \"" + item.path + "\" was not preloaded to GUI" << std::endl;
		}
	}

	bool GUI::button(GUIButton& item)
	{
		buttons.push_back(item);
		if (bPressOn && item.selectionIndex == selection)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void GUI::update()
	{
		if (bindingSelectUp != "")
		{
			if (InputManager::getButtonDown(bindingSelectUp))
			{
				bool match = false;
				for (GUIButton button : buttons)
				{
					if (button.selectionIndex == Vector2(selection.x, selection.y - 1))
					{
						match = true;
					}
				}
				if (match)
				{
					selection.y--;
				}
			}
		}
		if (bindingSelectDown != "")
		{
			if (InputManager::getButtonDown(bindingSelectDown))
			{
				bool match = false;
				for (GUIButton button : buttons)
				{
					if (button.selectionIndex == Vector2(selection.x, selection.y + 1))
					{
						match = true;
					}
				}
				if (match)
				{
					selection.y++;
				}
			}
		}
		if (bindingSelectLeft != "")
		{
			if (InputManager::getButtonDown(bindingSelectLeft))
			{
				bool match = false;
				for (GUIButton button : buttons)
				{
					if (button.selectionIndex == Vector2(selection.x - 1, selection.y))
					{
						match = true;
					}
				}
				if (match)
				{
					selection.x--;
				}
			}
		}
		if (bindingSelectRight != "")
		{
			if (InputManager::getButtonDown(bindingSelectRight))
			{
				bool match = false;
				for (GUIButton button : buttons)
				{
					if (button.selectionIndex == Vector2(selection.x + 1, selection.y))
					{
						match = true;
					}
				}
				if (match)
				{
					selection.x++;
				}
			}
		}
		if (bindingSelectPress != "")
		{
			if (InputManager::getButtonDown(bindingSelectPress))
			{
				bPressOn = true;
			}
			else
			{
				bPressOn = false;
			}
		}
		else
		{
			bPressOn = false;
		}

		if (dontUseMouse)
		{
			return;
		}

		Vector2 mousePos = InputManager::getMousePositionFixed();
		hovering = false;
		for (GUIButton button : buttons)
		{
			if (mousePos.x >= button.rect.x && mousePos.x < button.rect.x + button.rect.w && mousePos.y >= button.rect.y && mousePos.y < button.rect.y + button.rect.h)
			{
				hovering = true;
				selection = button.selectionIndex;
				if (InputManager::getButtonDown("Fire"))
				{
					bPressOn = true;
				}
			}
		}
	}

	void GUI::renderGUI()
	{
		for (auto image : images)
		{
			sprites.push_back(sf::Sprite(textures[image.path]));
			sf::FloatRect wh = sprites.back().getGlobalBounds();
			if (image.rect.w == 0)
			{
				image.rect.w = wh.width;
				if (image.center)
				{
					image.rect.x -= wh.width / 2;
				}
			}
			if (image.rect.h == 0)
			{
				image.rect.h = wh.height;
				if (image.center)
				{
					image.rect.y -= wh.height / 2;
				}
			}
			if (image.cropElseZoom)
			{
				sprites.back().setTextureRect(sf::IntRect(image.rectxy.x, image.rectxy.y, image.rect.w, image.rect.h));
			}
			else
			{
				sprites.back().setScale(sf::Vector2f(image.rect.w / textures[image.path].getSize().x, image.rect.h / textures[image.path].getSize().y));
			}
			sprites.back().setPosition(RenderManager::window.mapPixelToCoords(sf::Vector2i(image.rect.x, image.rect.y)));
			sprites.back().setRotation(RenderManager::window.getView().getRotation());
			sprites.back().setColor(sf::Color(image.color.r, image.color.g, image.color.b, image.opacity));
			RenderManager::window.draw(sprites.back());
			if (sfFont != nullptr)
			{
				sf::Text text;
				text.setFont(*sfFont);
				text.setString(image.value);
				text.setCharacterSize(32);
				text.setOrigin(text.getLocalBounds().width / 2, text.getLocalBounds().height / 2);
				text.setPosition(image.rect.x + image.rect.w / 2, image.rect.y + image.rect.h / 2);
				text.setPosition(RenderManager::window.mapPixelToCoords(static_cast<sf::Vector2i>(text.getPosition())));
				text.setColor(sf::Color(image.color.r, image.color.g, image.color.b, image.opacity));
				text.setRotation(RenderManager::window.getView().getRotation());
				RenderManager::window.draw(text);
			}
			else
			{
				renderFont(image.rect, image.value);
			}
		}
		for (auto box : boxes)
		{
			sprites.push_back(sf::Sprite(boxTexture));
			sprites.back().setPosition(sf::Vector2f(box.rect.x, box.rect.y));
			sf::Vector2f rect = sf::Vector2f( box.rect.w, box.rect.h );
			sprites.back().setScale({ rect.x / boxTexture.getSize().x, rect.y / boxTexture.getSize().y });
			sprites.back().setPosition(RenderManager::window.mapPixelToCoords(static_cast<sf::Vector2i>(sprites.back().getPosition())));
			sprites.back().setRotation(RenderManager::window.getView().getRotation());
			RenderManager::window.draw(sprites.back());
			if (sfFont != nullptr)
			{
				sf::Text text;
				text.setFont(*sfFont);
				text.setString(box.value);
				text.setCharacterSize(32);
				text.setOrigin(text.getLocalBounds().width / 2, text.getLocalBounds().height / 2);
				text.setPosition(box.rect.x + box.rect.w / 2, box.rect.y + box.rect.h / 2);
				text.setPosition(RenderManager::window.mapPixelToCoords(static_cast<sf::Vector2i>(text.getPosition())));
				text.setRotation(RenderManager::window.getView().getRotation());
				RenderManager::window.draw(text);
			}
			else
			{
				renderFont(box.rect, box.value);
			}
		}
		for (auto button : buttons)
		{
			if (button.selectionIndex == selection)
			{
				if (InputManager::getButtonPressed(bindingSelectPress))
				{
					sprites.push_back(sf::Sprite(buttonTexturePress));
					sprites.back().setPosition(sf::Vector2f(button.rect.x, button.rect.y));
					sf::Vector2f rect = sf::Vector2f(button.rect.w, button.rect.h);
					sprites.back().setScale({ rect.x / buttonTexturePress.getSize().x, rect.y / buttonTexturePress.getSize().y });
					sprites.back().setPosition(RenderManager::window.mapPixelToCoords(static_cast<sf::Vector2i>(sprites.back().getPosition())));
					sprites.back().setRotation(RenderManager::window.getView().getRotation());
					RenderManager::window.draw(sprites.back());
				}
				else
				{
					sprites.push_back(sf::Sprite(buttonTextureSelect));
					sprites.back().setPosition(sf::Vector2f(button.rect.x, button.rect.y));
					sf::Vector2f rect = sf::Vector2f(button.rect.w, button.rect.h);
					sprites.back().setScale({ rect.x / buttonTextureSelect.getSize().x, rect.y / buttonTextureSelect.getSize().y });
					sprites.back().setPosition(RenderManager::window.mapPixelToCoords(static_cast<sf::Vector2i>(sprites.back().getPosition())));
					sprites.back().setRotation(RenderManager::window.getView().getRotation());
					RenderManager::window.draw(sprites.back());
				}
			}
			else
			{
				sprites.push_back(sf::Sprite(buttonTexture));
				sprites.back().setPosition(sf::Vector2f(button.rect.x, button.rect.y));
				sf::Vector2f rect = sf::Vector2f(button.rect.w, button.rect.h);
				sprites.back().setScale({ rect.x / buttonTexture.getSize().x, rect.y / buttonTexture.getSize().y });
				sprites.back().setPosition(RenderManager::window.mapPixelToCoords(static_cast<sf::Vector2i>(sprites.back().getPosition())));
				sprites.back().setRotation(RenderManager::window.getView().getRotation());
				RenderManager::window.draw(sprites.back());
			}
			if (sfFont != nullptr)
			{
				sf::Text text;
				text.setFont(*sfFont);
				text.setString(button.value);
				text.setCharacterSize(32);
				text.setOrigin(text.getLocalBounds().width / 2, text.getLocalBounds().height / 2);
				text.setPosition(button.rect.x + button.rect.w / 2, button.rect.y + button.rect.h / 2);
				text.setPosition(RenderManager::window.mapPixelToCoords(static_cast<sf::Vector2i>(text.getPosition())));
				text.setRotation(RenderManager::window.getView().getRotation());
				RenderManager::window.draw(text);
			}
			else
			{
				renderFont(button.rect, button.value);
			}
		}
		boxes.clear();
		buttons.clear();
		images.clear();
		dontUseMouse = false;
	}
}
