#pragma once

#include "stdafx.h"

using namespace GameEngine;

class Turret : public GameObject
{
	private:
		Vector2 pos;
		virtual void update() override;

	public:
		Turret();
};
