#include "stdafx.h"
#include "Minimap.h"

using namespace GameEngine;

Minimap::Minimap() : GameObject("Minimap")
{

}

void Minimap::update()
{
	GUI::image(GUIImage{ "Minimapunder.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
	double currentRotation = -static_cast<Camera*>(GameObject::find("Camera"))->currentRotation;
	if (currentRotation + 45 > 0)
	{
		currentRotation = fmod(currentRotation + 45, 360);
	}
	else
	{
		currentRotation = 360 + fmod(currentRotation + 45, 360);
	}



	GameObject* sector6tmp = GameObject::find("Sector: 6");
	GameObject* sector4tmp = GameObject::find("Sector: 4");
	GameObject* sector5tmp = GameObject::find("Sector: 5");
	GameObject* sector7tmp = GameObject::find("Sector: 7");
	if (sector6tmp != nullptr)
	{
		GUI::image(GUIImage{ "location4black.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
	}
	if (sector5tmp != nullptr)
	{
		GUI::image(GUIImage{ "location1black.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
	}
	if (sector4tmp != nullptr)
	{
		GUI::image(GUIImage{ "location2black.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
	}
	if (sector7tmp != nullptr)
	{
		GUI::image(GUIImage{ "location3black.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
	}



	if (currentRotation >= 0 && currentRotation <= 180)
	{
		if (currentRotation >= 90 && currentRotation <= 270)
		{
			
			{
				GUI::image(GUIImage{ "location2.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
			}

		}
		else
		{
			GUI::image(GUIImage{ "location1.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
		}

	}
	else
	{
		if (currentRotation >= 90 && currentRotation <= 270)
		{
			
			{
				GUI::image(GUIImage{ "location3.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
			}
		}
		else 
		{
			GUI::image(GUIImage{ "location4.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
		}
	}

	for(int i = 0; i < spawnPositions.size(); i++)
	{
		if (spawnPositions[i].x > 0)
		{
			if (spawnPositions[i].y > 0)
			{
				time1 = 0.5;
			}
			else
			{
				time2 = 0.5;
			}
				
		}
		else
		{
			if (spawnPositions[i].y > 0)
			{
				time3 = 0.5;
			}
			else
			{
				time4 = 0.5;
			}
		}
	}
	
	spawnPositions.clear();

	if (Engine::clock.getElapsedTime().asMilliseconds() % 1000 >= 500)
	{
		if (time1 > 0)
		{
			GUI::image(GUIImage{ "Danger1.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
			time1 -= Engine::smootherDeltaTime;
		}
		if (time2 > 0)
		{
			GUI::image(GUIImage{ "Danger2.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
			time2 -= Engine::smootherDeltaTime;
		}
		if (time3 > 0)
		{
			GUI::image(GUIImage{ "Danger3.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
			time3 -= Engine::smootherDeltaTime;
		}
		if (time4 > 0)
		{
			GUI::image(GUIImage{ "Danger4.png", Vector2(Engine::getLogicalResolution().x - 256, Engine::getLogicalResolution().y - 254) });
			time4 -= Engine::smootherDeltaTime;
		}
	}

	GameObject* sector3tmp = GameObject::find("Sector Shield: 3");
	GameObject* sector1tmp = GameObject::find("Sector Shield: 1");
	GameObject* sector2tmp = GameObject::find("Sector Shield: 2");
	if (sector3tmp != nullptr)
	{
		GUI::image(GUIImage{ "MinimapFront3.png", Vector2(Engine::getLogicalResolution().x - 388, Engine::getLogicalResolution().y - 376) });
	}
	else if (sector2tmp != nullptr)
	{
		GUI::image(GUIImage{ "MinimapFront2.png", Vector2(Engine::getLogicalResolution().x - 388, Engine::getLogicalResolution().y - 376) });
	}
	else if (sector1tmp != nullptr)
	{
		GUI::image(GUIImage{ "MinimapFront1.png", Vector2(Engine::getLogicalResolution().x - 388, Engine::getLogicalResolution().y - 376) });
	}
	else
	{
		GUI::image(GUIImage{ "MinimapFront.png", Vector2(Engine::getLogicalResolution().x - 388, Engine::getLogicalResolution().y - 376) });
	}
}
