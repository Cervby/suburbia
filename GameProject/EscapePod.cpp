#include "stdafx.h"

using namespace GameEngine;

EscapePod::EscapePod(Vector2& toPos, std::string spriteName, int health) : AlienShip(toPos, spriteName, health)
{
}

void EscapePod::shipMovement()
{

	if (BalloonsOut == 0)
	{
		Counter = 0;
		BalloonsOut = false;
		animatedSprite->setSprite("balloons spritesheet first.png");
		animatedSprite->setAnimation(Vector2(105, 172), 0.0625, 15);
	}
	else if (BalloonsOut == false && Counter > 1)
	{
		BalloonsOut = true;
		animatedSprite->setSprite("BalloonsSpriteSheet normal.png");
		animatedSprite->setAnimation(Vector2(177, 172), 0.0625, 19);
	}

	Counter += Engine::smootherDeltaTime;
	moveTo(toPos, Engine::smootherDeltaTime * 150);
}

void EscapePod::deathMovement()
{

	moveTo(toPos, Engine::smootherDeltaTime * -300);

	if (deathAnimationTrigger == false)
	{
		if (WhichSounds == 1)
		{
			AudioManager::playSound("Escape Pod expl C.wav");
		}

		else if (WhichSounds == 2)
		{
			AudioManager::playSound("Escape Pod expl E.wav");
		}

		else if (WhichSounds == 3)
		{
			AudioManager::playSound("Escape Pod expl G.wav");
		}

		deathAnimationTrigger = true;
		animatedSprite->setSprite("EscapePodDeathSpritesheet177-231.png");
		animatedSprite->setAnimation(Vector2(177, 231), 0.06, 9);
	}
}
