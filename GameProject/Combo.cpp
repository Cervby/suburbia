#include "stdafx.h"
#include "Combo.h"

using namespace GameEngine;



Combo::Combo(Vector2& position) : GameObject("Combo")
{

	setPosition(position);
	static_cast<ComboMeter*>(GameObject::find("Combo Meter"))->comboCount += static_cast<ComboMeter*>(GameObject::find("Combo Meter"))->comboCurrent + 1;

	static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.score += static_cast<ComboMeter*>(GameObject::find("Combo Meter"))->comboCurrent + 1;

	static_cast<ComboMeter*>(GameObject::find("Combo Meter"))->comboCurrent = static_cast<ComboMeter*>(GameObject::find("Combo Meter"))->comboCurrent * 2 + 1;
	comboThis = static_cast<ComboMeter*>(GameObject::find("Combo Meter"))->comboCurrent;
	combotimer = 2;

	if (static_cast<ComboMeter*>(GameObject::find("Combo Meter"))->comboCurrent == 3)
	{
		AudioManager::playSound("combo1.wav");
	}
	else if (static_cast<ComboMeter*>(GameObject::find("Combo Meter"))->comboCurrent == 4)
	{
		AudioManager::playSound("combo2.wav");
	}
	else if (static_cast<ComboMeter*>(GameObject::find("Combo Meter"))->comboCurrent == 5)
	{
		AudioManager::playSound("combo3.wav");
	}
}

void Combo::update()
{

	Vector2 pos = Utility::screenToWorld(getPosition());
	if (combotimer >= 1)
	{
		GUI::image(GUIImage{ "null.png", Rect(pos.x - 32, pos.y - 128, 64, 256), std::to_string(comboThis) });
	}
	combotimer -= Engine::smootherDeltaTime;
	if (combotimer <= 0)
	{
		for (GameObject* goit : GameObject::objectList)
		{
			if (goit->name == "Combo" && goit != static_cast<GameObject*>(this))
			{
				static_cast<ComboMeter*>(GameObject::find("Combo Meter"))->comboCurrent = 0;
			}
		}
		destroy();
	}
};
