#include "stdafx.h"

using namespace GameEngine;

MainMenu::MainMenu(std::string lastMasterName) : Master("Main Menu")
{
	addSprite();
	animatedSprite->setSprite("TextUnmarked.png");
	setPosition(Engine::getLogicalResolution() / 2);
	animatedSprite->setAboveAll(true);

	AudioManager::stop();
	AudioManager::playMusic("main menu theme 1a.ogg");


	RenderManager::window.setView(RenderManager::window.getDefaultView());

	std::cout << "Main Menu start" << std::endl;

	GUI::selection = Vector2(0, 0);
}

void MainMenu::update()
{
	GUI::dontUseMouse = true;
	GUI::hovering = false;

	if (GUI::selection.y != 0)
	{
		GUI::selection = Vector2(0, 0);
	}

	GUI::image(GUIImage{ "Bakgrund.png", Rect(Engine::getLogicalResolution().x / 2, Engine::getLogicalResolution().y / 2, 0, 0) });
	GUI::image(GUIImage{ "Kugghjul.png", Rect(Engine::getLogicalResolution().x / 2, Engine::getLogicalResolution().y / 2, 0, 0) });

	GUI::image(GUIImage{ "logofinal.png", Rect(Engine::getLogicalResolution().x / 2 - 750, Engine::getLogicalResolution().y / 2 + 400, 0, 0) });

	Vector2 res20 = Engine::getLogicalResolution() / 20;
	if (GUI::selection == Vector2(0, 0))
	{
		if (LastSelection == Vector2(1, 0))
		{
			setRotation(-90);
		}
		setRotation(Utility::lerp(getRotation(), 0, Engine::smootherDeltaTime * 7));
		animatedSprite->setSprite("Play.png");
	}
	else if (GUI::selection == Vector2(1, 0))
	{
		if (LastSelection == Vector2(0, 0))
		{
			setRotation(360);
		}
		setRotation(Utility::lerp(getRotation(), 270, Engine::smootherDeltaTime * 7));
		animatedSprite->setSprite("Options.png");
	}
	else if (GUI::selection == Vector2(2, 0))
	{
		setRotation(Utility::lerp(getRotation(), 180, Engine::smootherDeltaTime * 7));
		animatedSprite->setSprite("Exit.png");
	}
	else if (GUI::selection == Vector2(3, 0))
	{
		setRotation(Utility::lerp(getRotation(), 90, Engine::smootherDeltaTime * 7));
		animatedSprite->setSprite("HighScore.png");
	}
	else if (GUI::selection == Vector2(4, 0))
	{
		GUI::selection = Vector2(0, 0);
	}
	else if (GUI::selection == Vector2(-1, 0))
	{
		GUI::selection = Vector2(3, 0);
	}

	GUI::button(GUIButton{ Rect(0, 0, 0, 0), "", { -1, 0 } });
	GUI::button(GUIButton{ Rect(0, 0, 0, 0), "", { 4, 0 } });

	if (GUI::button(GUIButton{ Rect(res20.x * 8, res20.y * 4, res20.x * 4, res20.y * 3), "", Vector2(0, 0) }))
	{
		AudioManager::playSound("click menu.wav");
		destroy();
		Master::master->changeMaster<Intro>();
	}
	if (GUI::button(GUIButton{ Rect(res20.x * 12, res20.y * 7, res20.x * 2, res20.y * 7), "", { 1, 0 } }))
	{

		AudioManager::playSound("click menu.wav");
		Master::master->changeMaster<Options>();
	}
	if (GUI::button(GUIButton{ Rect(res20.x * 8, res20.y * 14, res20.x * 4, res20.y * 3), "", { 2, 0 } }))
	{
		AudioManager::playSound("click menu.wav");
		Engine::bMainGameLoop = false;
	}
	if (GUI::button(GUIButton{ Rect(res20.x * 6, res20.y * 7, res20.x * 2, res20.y * 7), "", { 3, 0 } }))
	{
		Master::master->changeMaster<HighScore>();
	}

	if (InputManager::getQuit() || InputManager::getButtonDown("Exit"))
	{
		destroy();
		GameObject::find("Cursor")->destroy();
		Engine::bMainGameLoop = false;
	}

	if (GUI::selection != LastSelection)
	{
		LastSelection = GUI::selection;
		AudioManager::playSound("hover menu.wav");
		
	}
	

};

MainMenu::~MainMenu()
{
	for (GameObject* gameObject : GameObject::objectList)
	{
		if (gameObject->name != "Main Menu" && gameObject->name != "Cursor")
		{
			gameObject->destroy();
		}
	}
	if (Master::master->nextMasterName == "Game Master")
	{
		AudioManager::stop();
		AudioManager::playMusic("gameplaytheme.ogg");
	}
}

