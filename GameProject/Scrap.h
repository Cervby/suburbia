#pragma once 
#include "stdafx.h"

using namespace GameEngine;

class Scrap : public GameObject
{
	double rotationalFactor;
	Vector2 movementFactor;

	virtual void update() override;

public:
	Scrap();
};