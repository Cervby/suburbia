#pragma once

#include "stdafx.h"

using namespace GameEngine;

class President : public GameObject
{
private:
	virtual void update() override;

public:
	bool moveOut;

	President();
};
