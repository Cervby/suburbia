#pragma once

#include "stdafx.h"

using namespace GameEngine;

class Combo : public GameObject
{
private:
	int comboThis;
	double combotimer;
	virtual void update() override;
public:
	Combo(Vector2& position);
};
