#include "stdafx.h"

using namespace GameEngine;

Cyclos::Cyclos(Vector2& toPos, std::string spriteName, int health, PlayerPowerUp powerUp) : AlienShip(toPos, spriteName, health, powerUp)
{
	EMPcounter = 2;
}

void Cyclos::shipMovement()
{

	if (EMPcounter <= 1.7)
	{
		animatedSprite->setSprite("CyclosSparks.png");
		animatedSprite->setAnimation(Vector2(200, 200), 0.06, 10);
	}

	moveTo(toPos, Engine::smootherDeltaTime * SlowTime::speedFactor * 150);
	Angle += sin(Engine::clock.getElapsedTime().asSeconds() * 5) * SlowTime::speedFactor;
	setPositionAround(toPos, getPosition().distance(toPos), Angle);
}

void Cyclos::deathMovement()
{

	moveTo(toPos, Engine::smootherDeltaTime * SlowTime::speedFactor * -300);

	if (deathAnimationTrigger == 0)
	{
		WhichSounds = Utility::getRandInt(1, 3);

		if (WhichSounds == 1)
		{
			AudioManager::playSound("Cyclos expl A.wav");
		}

		else if (WhichSounds == 2)
		{
			AudioManager::playSound("Cyclos expl F.wav");
		}

		else if (WhichSounds == 3)
		{
			AudioManager::playSound("Cyclos expl D.wav");
		}


		deathAnimationTrigger = true;
		animatedSprite->setSprite("CyclosDeathSprite.png");
		animatedSprite->setAnimation(Vector2(128, 128), 0.05, 8);
	}
}
