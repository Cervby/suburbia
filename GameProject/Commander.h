#pragma once

#include "stdafx.h"

using namespace GameEngine;

class Commander : public GameObject
{
private:
	std::string imagePath;
	double alphaB;

	virtual void update() override;

public:
	Commander();
};
