#pragma once

#include "stdafx.h"

using namespace GameEngine;

class EscapePod : public AlienShip
{
private:
	double Counter;
	bool BalloonsOut;

	virtual void shipMovement() override;
	virtual void deathMovement() override;

public:
	EscapePod(Vector2& toPos, std::string spriteName, int health);
};
