#include "stdafx.h"
#include "Planet.h"

using namespace GameEngine;




Planet::Planet() : GameObject("Planet")
{
	addSprite();
	addCollider();
	isDying = false;
	dyingCounter = 2;
	health = MAXPLAYERHEALTH / 4;
	sectorHealth = { { MAXPLAYERHEALTH / 4, MAXPLAYERHEALTH / 4, MAXPLAYERHEALTH / 4, MAXPLAYERHEALTH / 4 } };
	new PlanetShieldCollision(getPosition());

	animatedSprite->setSprite("PlanetEarth.png");
	setRotation(45);
	collider->set(ColliderType::COLLIDER_CIRCLE);
	collider->radius *= 0.85;
	collider->colliderTag = "Friendly";
}
void Planet::onCollision(GameObject* other)
	{
		if (other->name == "AlienShip" && static_cast<AlienShip*>(other)->EnemyHit == false)
		{
			bool isCollision = false;
			Vector2 otherPos = other->getPosition();
			if (otherPos.x > 0 && otherPos.y <= 0)
			{
				isCollision = true;
				if (sectorHealth[0] > 0)
				{

					sectorHealth[0]--;
				}
				if (sectorHealth[0] <= MAXPLAYERHEALTH / 4 * 0.35)
				{
					if (GameObject::find("Sector: 0") == nullptr)
					{
						new Sector(0);
					}
				}
				if (sectorHealth[0] == 0)
				{

					if (GameObject::find("Sector: 4") == nullptr)
					{
						new Sector(4);
					}
				}
			}
			else if (otherPos.x > 0 && otherPos.y > 0)
			{
				isCollision = true;
				if (sectorHealth[3] > 0)
				{

					sectorHealth[3]--;
				}
				if (sectorHealth[3] <= MAXPLAYERHEALTH / 4 * 0.35)
				{
					if (GameObject::find("Sector: 3") == nullptr)
					{
						new Sector(3);
					}
				}
				if (sectorHealth[3] == 0)
				{

					if (GameObject::find("Sector: 7") == nullptr)
					{
						new Sector(7);
					}
				}
			}
			else if (otherPos.x <= 0 && otherPos.y <= 0)
			{
				isCollision = true;
				if (sectorHealth[1] > 0)
				{
					sectorHealth[1]--;
				}
				if (sectorHealth[1] <= MAXPLAYERHEALTH / 4 * 0.35)
				{
					if (GameObject::find("Sector: 1") == nullptr)
					{
						new Sector(1);
					}

				}
				if (sectorHealth[1] == 0)
				{

					if (GameObject::find("Sector: 5") == nullptr)
					{
						new Sector(5);
					}
				}
			}
			else if (otherPos.x <= 0 && otherPos.y > 0)
			{
				isCollision = true;
				if (sectorHealth[2] > 0)
				{
					sectorHealth[2]--;
				}
				if (sectorHealth[2] <= MAXPLAYERHEALTH / 4 * 0.35)
				{
					if (GameObject::find("Sector: 2") == nullptr)
					{
						new Sector(2);
					}

				}
				if (sectorHealth[2] == 0)
				{

					if (GameObject::find("Sector: 6") == nullptr)
					{
						new Sector(6);
					}
				}
			}



			static_cast<AlienShip*>(other)->deathSource = AlienShipDeathSource::ALIENSHIPDEATHSOURCE_PLANET;
			static_cast<AlienShip*>(other)->EnemyHit = true;
			static_cast<AlienShip*>(other)->deathCounter = 0;
		}
	};


void Planet::update()
{

	std::array<unsigned long long, 4> sectorUsed;
	if (GameObject::find("Sector Shield: 0") == nullptr)
	{
		sectorUsed[0] = true;
	}
	else
	{
		sectorUsed[0] = false;
	}
	if (GameObject::find("Sector Shield: 1") == nullptr)
	{
		sectorUsed[1] = true;
	}
	else
	{
		sectorUsed[1] = false;
	}
	if (GameObject::find("Sector Shield: 2") == nullptr)
	{
		sectorUsed[2] = true;
	}
	else
	{
		sectorUsed[2] = false;
	}
	if (GameObject::find("Sector Shield: 3") == nullptr)
	{
		sectorUsed[3] = true;
	}
	else
	{
		sectorUsed[3] = false;
	}
	health = (sectorUsed[0] ? sectorHealth[0] : 0) + (sectorUsed[1] ? sectorHealth[1] : 0) + (sectorUsed[2] ? sectorHealth[2] : 0) + (sectorUsed[3] ? sectorHealth[3] : 0);
	if (health <= 0)
	{
		isDying = true;
	}

	animatedSprite->setTint(sf::Color(255, dyingCounter * 127, dyingCounter * 127));
	if (isDying)
	{
		dyingCounter -= Engine::smootherDeltaTime;
		if (dyingCounter <= 0)
		{
			Master::master->changeMaster<GameOver>(static_cast<GameMaster*>(Master::master)->gameData.score);
		}
	}
};

PlanetShieldCollision::PlanetShieldCollision(Vector2& planetPos) : GameObject("Planet Shield Collision")
{
	addCollider();
	collider->set(ColliderType::COLLIDER_CIRCLE);
	collider->bNoCollision = true;
	collider->radius = collider->radius / 0.9;
	collider->colliderTag = "Friendly";
	new SectorShield(1, planetPos);
	new SectorShield(2, planetPos);
	new SectorShield(3, planetPos);
}
void PlanetShieldCollision::onOverlap(GameObject* other)
	{
		if (other->name == "AlienShip" && static_cast<AlienShip*>(other)->EnemyHit == false && static_cast<AlienShip*>(other)->checkedForShieldCollision == false)
		{
			static_cast<AlienShip*>(other)->checkedForShieldCollision = true;
			bool isCollision = false;
			Vector2 otherPos = other->getPosition();
			if (otherPos.x > 0 && otherPos.y <= 0)
			{
				if (GameObject::find("Sector shield: 0") != nullptr)
				{
					isCollision = true;

				}
			}
			else if (otherPos.x <= 0 && otherPos.y > 0)
			{
				if (GameObject::find("Sector Shield: 1") != nullptr)
				{
					isCollision = true;
				}
			}
			else if (otherPos.x <= 0 && otherPos.y <= 0)
			{
				if (GameObject::find("Sector Shield: 2") != nullptr)
				{
					isCollision = true;
				}
			}
			else if (otherPos.x > 0 && otherPos.y > 0)
			{
				if (GameObject::find("Sector Shield: 3") != nullptr)
				{
					isCollision = true;
				}
			}
			if (isCollision)
			{
				static_cast<AlienShip*>(other)->deathSource = AlienShipDeathSource::ALIENSHIPDEATHSOURCE_PLANET;
				static_cast<AlienShip*>(other)->EnemyHit = true;
				static_cast<AlienShip*>(other)->deathCounter = 0;

				new ShieldSparks(other);
			}

		}
	};







ShieldSparks::ShieldSparks(GameObject* other) : GameObject("Shield spark")
{
	addSprite();
	setPosition(other->getPosition());
	setRotation(other->getRotation());
	animatedSprite->setSprite("bouncesprite.png");
	AudioManager::playSound("shield hit.wav");
	Counter = 0;
}
void ShieldSparks::update()
{
	Counter += Engine::smootherDeltaTime;
	if (Counter > 0.5)
	{
		destroy();
	}
}
