

#pragma once

#include "stdafx.h"

enum class DisplayPosition
{
	DISPLAYPOSITION_RELATIVE,
	DISPLAYPOSITION_FIXED
};

namespace GameEngine
{
	class AnimatedSprite;
	class Collider;
	class Vector2;

	class GameObject
	{
	private:
		bool isToBeDestroyed;

		Vector2 position;
		double rotation;
		Vector2 scale;

		static std::vector<GameObject*> pauseObjects;
		bool isPaused = false;

	public:
		virtual void update() {};
		virtual void onCollision(GameObject* other) {};
		virtual void onOverlap(GameObject* other) {};

		std::string name;
		DisplayPosition displayPosition;
		AnimatedSprite* animatedSprite;
		Collider* collider;
		static std::vector<GameObject*> objectList;
		static std::vector<GameObject*> addObjectList;
		static void quit();
		static GameObject* find(std::string name);
		GameObject(std::string name, DisplayPosition displayPosition = DisplayPosition::DISPLAYPOSITION_RELATIVE);
		virtual ~GameObject();
		void destroy();
		static void updateGameObjects();
		void addSprite();
		void addCollider();

		Vector2 _revertBackPosition;


		void pause(std::vector<GameObject*>& gameObjects);
		void unpause();


		void setPosition(Vector2& pos);
		Vector2 getPosition();

		void setRotation(const double rot);
		const double getRotation() { return rotation; };

		void setScale(Vector2& scale);
		Vector2 getScale();

		void translate(Vector2& relativePos);
		void moveTo(Vector2& toPos, double speed);
		void moveDir(Vector2& toDir, double speed);
		void lookAt(Vector2& v2);
		void setPositionAround(Vector2& pos, double distance, double angle);
		void setPositionAround(Vector2& pos, double angle);

	};
}
