#pragma once

#include "stdafx.h"



namespace GameEngine
{
	class AudioManager
	{
	private:
		static std::unordered_map<std::string, sf::SoundBuffer*> sounds;
		static std::unordered_map<std::string, sf::Music*> musics;

		static std::deque<sf::Sound*> sfSounds;
		static std::string currentMusic;
		static bool mutedMusic;
		static bool mutedSound;

	public:
		static bool getAllMusicMuted() { return mutedMusic; };
		static void muteAllMusic(bool setMuted);
		static bool getAllSoundMuted() { return mutedSound; };
		static void muteAllSound(bool setMuted);
		static void init();
		static void playSound(std::string path, double pitch = 1);
		static void AudioManager::pitchAll(double pitch);
		static void playMusic(std::string path);
		static double getVolumeMusic(std::string path);
		static void setVolumeMusic(std::string path, double volume);
		static void stopMusic(std::string path);
		static void pitchMusic(std::string path, double pitch);
		static void pauseMusic(std::string path);
		static void stop();
		static void quit();
	};
}
