#pragma once

#include "stdafx.h"

using namespace GameEngine;

class ComboMeter : public GameObject
{
private:


	double showTime;

	

public:

	ComboMeter();
	virtual void update() override;
	unsigned long long comboCurrent;
	unsigned long long comboCount;
	unsigned long long comboCountShow;
};
