#pragma once

#include "stdafx.h"



namespace GameEngine
{
	typedef void(*VoidFunctionGameObjectGameObjectVar)(GameObject*, GameObject*);

	enum class ColliderType
	{
		INVALID_COLLIDER_TYPE,
		COLLIDER_SQUARE,
		COLLIDER_CIRCLE,
		INVALID_COLLIDER_TYPE2
	};

	class Collider
	{
	private:
		Rect region;

	public:
		GameObject* gameObject;
		ColliderType colliderType;
		bool bNoCollision;
		std::string colliderTag;
		double radius;
		bool isClicked;
		bool isHovering;

		Collider(GameObject* gameObject);
		bool overlap(Collider* other);
		void setRegionFromSprite();
		void set(ColliderType type, Rect region = Rect());
		void set(ColliderType type, double radius);
		static void updateCollisions();
	};
}
