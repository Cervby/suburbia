#include "stdafx.h"
#include "Camera.h"

using namespace GameEngine;

Camera::Camera() : GameObject("Camera")
{
	view = sf::View(sf::FloatRect(0, 0, Engine::getLogicalResolution().x, Engine::getLogicalResolution().y));
	
	toRotation = 100000 * 90;
	pos = 325;
	currentRotation = toRotation;
	lastRotation = currentRotation;
	RenderManager::window.setView(view);
}

void Camera::update()
{

	lastRotation = currentRotation;
	GameMaster* gmaster = static_cast<GameMaster*>(GameObject::find("Game Master"));
	int itn = gmaster->gameData.waveIterator;
	WaveStep wave = gmaster->listOfTheEnemyWaves[itn];
	if (wave.unlock == -1)
	{
		if (InputManager::getButtonDown("Right"))
		{
			AudioManager::playSound("sector rotation.wav");
			toRotation -= 90;
		}
		if (InputManager::getButtonDown("Left"))
		{
			AudioManager::playSound("sector rotation.wav");
			toRotation += 90;
		}
	}
	if (static_cast<GameMaster*>(GameObject::find("Game Master"))->listOfTheEnemyWaves[static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.waveIterator].unlock == -1)
	{
		currentRotation = Utility::lerp(currentRotation, toRotation, Engine::smootherDeltaTime * 10);
	}
	else
	{
		currentRotation = Utility::lerp(currentRotation, toRotation, Engine::smootherDeltaTime * 4);
	}
	setPositionAround(GameObject::find("Planet")->getPosition(), 1100, 45 + currentRotation);
	lookAt(GameObject::find("Planet")->getPosition());
	static_cast<Cursor*>(GameObject::find("Cursor"))->update();
	if (static_cast<Planet*>(GameObject::find("Planet"))->isDying)
	{
		setPosition(getPosition() + Vector2(Utility::getRandInt(-10, 10), Utility::getRandInt(-10, 10)));
	}

	view.setCenter(getPosition().to_sfF());
	view.setRotation(getRotation());
	RenderManager::window.setView(view);
}
