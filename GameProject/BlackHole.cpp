#include "stdafx.h"
#include "BlackHole.h"

using namespace GameEngine;

BlackHole::BlackHole(Vector2& toPos) : GameObject("BlackHole")
{
	addSprite();
	animatedSprite->setSprite("blackholeprojectile.png");
	Vector2 pos = GameObject::find("Barrel")->getPosition();
	setPosition(pos);


	toDir = toPos - pos;
	cooldown = 60;
	deathCounter = 5;
	explodeCounter = 2.6;
	exploding = false;
	explodingTwo = false;

	lookAt(toPos);
}

void BlackHole::update()
{
		if (exploding == true)
		{
			deathCounter -= Engine::smootherDeltaTime;
			for (GameObject* other : GameObject::objectList)
			{
				if (other->name == "AlienShip")
				{
					double mov = 1000 - getPosition().distance(other->getPosition());
					other->moveTo(getPosition(), mov <= 0 ? 0 : (Engine::smootherDeltaTime * mov));
				}
			}
			if (deathCounter > 2.5)
			{
				animatedSprite->setTint(sf::Color(255, 255, 255, 255 - (255 / 2.5) * (deathCounter - 2.5)));
			}
			else
			{
				animatedSprite->setTint(sf::Color(255, 255, 255, (255 / 2.5) * deathCounter));
			}
		}
			else if (explodingTwo == true )
			{
				explodeCounter -= Engine::smootherDeltaTime;
				if (explodeCounter <= 0)
				{
					AudioManager::playSound("BlackHole.wav", static_cast<float>(Utility::getRandInt(80, 130)) / 100);
					exploding = true;
					animatedSprite->setSprite("image1.png");
					animatedSprite->setAnimation(Vector2(512, 512), 0.1, 40);
					animatedSprite->setTint(sf::Color(255, 255, 255, 0));
				}
			}
		else
		{
			moveDir(toDir, Engine::smootherDeltaTime * 200);

			explodeCounter -= Engine::smootherDeltaTime;
			if (explodeCounter < 0)
			{
				
				animatedSprite->setSprite("BLACKHOLEPurpleSparkSpritesheet.png");
				animatedSprite->setAnimation(Vector2(200, 200), 0.05, 9);
				explodingTwo = true;
				explodeCounter = 0.1;
			}
		}

		if (deathCounter <= 0)
		{
			destroy();
		}
}
