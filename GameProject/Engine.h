

#pragma once

#include "stdafx.h"

namespace GameEngine
{
	struct Settings
	{
		Vector2 logicalResolution;
		Vector2 scale;
		bool fullscreen;
		Vector2 targetResolution;
		std::string assetsFolder;
		std::string title;
	};

	class Engine
	{
	private:
		static uint32_t lastTime;
		static std::deque<double> lastDeltaTimes;
		static Settings settings; 
		static double lastDrawTime;

		static void update();
		static void updateDeltaTime();


	public:
		static bool bMainGameLoop;
		static double deltaTime;
		static double smootherDeltaTime;
		static sf::Clock clock;

		static void setScale(Vector2& scale);
		static Settings& getSettings() { return settings; };
		static void init(std::string title, Settings& ssettings);
		static void run();
		static void setFullscreen(bool set);
		static Vector2& getLogicalResolution() { return settings.logicalResolution;  };
		static void quit();
	};
}
