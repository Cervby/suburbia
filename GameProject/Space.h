#pragma once

#include "stdafx.h"

using namespace GameEngine;

class Atmo : GameObject
{
private:
	virtual void update() override;

public:
	Atmo();
};

class Atmo2 : GameObject
{
private:
	std::array<std::array<double, 4>, 4> atmoColors;
	virtual void update() override;

public:
	Atmo2();
};

class Space : public GameObject
{
private:
	Atmo* atmo;
	Atmo2* atmo2;

public:
	Space();
};
