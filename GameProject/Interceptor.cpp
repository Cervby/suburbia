#include "stdafx.h"

using namespace GameEngine;

Interceptor::Interceptor(Vector2& toPos, std::string spriteName, int health) : AlienShip(toPos, spriteName, health)
{
}

void Interceptor::shipMovement()
{

	deathCounter = 1;

	if (accY == 0)
	{
		K = 0.08;
		J = 1, 5;
		accY = 0;
		accX = 0.3;
	}
	accY += Engine::smootherDeltaTime * SlowTime::speedFactor * J;
	if (accX >= 0)
	{
		accX -= Engine::smootherDeltaTime * SlowTime::speedFactor * K;
		if (static_cast<GameMaster*>(GameObject::find("Game Master"))->listOfTheEnemyWaves[static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.waveIterator].enemyType == EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT)
		{
			Angle -= Engine::smootherDeltaTime * SlowTime::speedFactor * accX * 40;
		}
		else
		{
			Angle += Engine::smootherDeltaTime * SlowTime::speedFactor * accX * 40;
		}
	}

	if (EMPcounter <= 1.7)
	{
		animatedSprite->setSprite("InterceptorSparks.png");
		animatedSprite->setAnimation(Vector2(200, 200), 0.06, 10);
	}

	moveTo(toPos, accY * Engine::smootherDeltaTime * SlowTime::speedFactor * 40);
	setPositionAround(toPos, getPosition().distance(toPos), Angle);
}

void Interceptor::deathMovement()
{


	if (deathAnimationTrigger == 0)
	{
		WhichSounds = Utility::getRandInt(1, 3);

		if (WhichSounds == 1)
		{
			AudioManager::playSound("INTER explshort A.wav");
		}

		else if (WhichSounds == 2)
		{
			AudioManager::playSound("INTER explshort G.wav");
		}

		else if (WhichSounds == 3)
		{
			AudioManager::playSound("INTER explshort D.wav");
		}

		deathAnimationTrigger = true;
		animatedSprite->setSprite("InterceptorDeathSpritesheet.png");
		animatedSprite->setAnimation(Vector2(128, 128), 0.05, 10);
	}

	if (A == 0)
	{
		A = 0;
		K = 0.08;
		J = 4;
		accY = 0;
		accX = 0.3;
	}
	accY += Engine::smootherDeltaTime * SlowTime::speedFactor * J;
	if (accX >= 0)
	{
		accX -= Engine::smootherDeltaTime * SlowTime::speedFactor * K;
		if (static_cast<GameMaster*>(GameObject::find("Game Master"))->listOfTheEnemyWaves[static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.waveIterator].enemyType == EnemyType::ENEMYTYPE_INTERCEPTOR_RIGHT)
		{
			Angle += Engine::smootherDeltaTime * SlowTime::speedFactor * accX * 40;
		}
		else
		{
			Angle -= Engine::smootherDeltaTime * SlowTime::speedFactor * accX * 40;
		}
	}

	moveTo(toPos, accY * Engine::smootherDeltaTime * SlowTime::speedFactor * 40);
	setPositionAround(toPos, getPosition().distance(toPos), Angle);
}
