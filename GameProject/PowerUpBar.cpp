#include "stdafx.h"
#include "PowerUpBar.h"

using namespace GameEngine;

void PowerUpBar::update()
{
	switch (static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.currentPowerUp)
	{
	case PlayerPowerUp::POWER_UP_BLACK_HOLE:
		GUI::image(GUIImage{ "PowerUpReady1.png", Vector2(165, Engine::getLogicalResolution().y - 256 - 36) });
		GUI::image(GUIImage{ "PowerUpReady2.png", Vector2(165, Engine::getLogicalResolution().y - 256 - 36) , false, "", 127 + sin(Engine::clock.getElapsedTime().asSeconds() * 4) * 127
	});
		GUI::image(GUIImage{ "GPBReady.png", Vector2(165, Engine::getLogicalResolution().y - 256 - 36) });
		break;
	case PlayerPowerUp::POWER_UP_EMP:
		GUI::image(GUIImage{ "PowerUpReady1.png", Vector2(165, Engine::getLogicalResolution().y - 256 - 36) });
		GUI::image(GUIImage{ "PowerUpReady2.png", Vector2(65, Engine::getLogicalResolution().y - 256 - 36), false, "", 127 + sin(Engine::clock.getElapsedTime().asSeconds() * 4) * 127
	});
		GUI::image(GUIImage{ "EmpReady.png", Vector2(165, Engine::getLogicalResolution().y - 256 - 36) });
		break;
	default:
		GUI::image(GUIImage{ "NoPowerUp.png", Vector2(165, Engine::getLogicalResolution().y - 256 - 36) });
		break;
	}
}
