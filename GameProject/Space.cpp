#include "stdafx.h"
#include "Space.h"

using namespace GameEngine;

Space::Space() : GameObject("Space back", DisplayPosition::DISPLAYPOSITION_FIXED)
{
	addSprite();
	animatedSprite->setSprite("BackgroundBack.png");
	animatedSprite->setZIndex(-1000);
	setPosition(Vector2{ 960, 540 });
	atmo = new Atmo;
	atmo2 = new Atmo2;
}

Atmo::Atmo() : GameObject("Space atmosphere", DisplayPosition::DISPLAYPOSITION_FIXED)
{
	addSprite();
	animatedSprite->setSprite("Whiteatmosphere2.png");
	animatedSprite->setZIndex(-1000);
	setPosition(Vector2{ 960, 540 });
}

Atmo2::Atmo2() : GameObject("Space atmosphere2", DisplayPosition::DISPLAYPOSITION_FIXED)
{
	addSprite();
	animatedSprite->setSprite("Whiteatmosphere2.png");
	animatedSprite->setZIndex(-1000);
	setPosition(Vector2{ 960, 540 });
}

void Atmo::update()
{
	int sect = ((int)static_cast<Camera*>(GameObject::find("Camera"))->toRotation / 90) % 4;
	if (sect < 0)
	{
		sect = 4 + sect;
	}

	animatedSprite->setTint(sf::Color(0x5E, 0xE9, 0xFF, 255));
}

void Atmo2::update()
{
	int sect = ((int)static_cast<Camera*>(GameObject::find("Camera"))->toRotation / 90) % 4;
	if (sect < 0)
	{
		sect = 4 + sect;
	}

	atmoColors[sect][3] = Utility::lerp(atmoColors[sect][3], 255 - static_cast<Planet*>(GameObject::find("Planet"))->sectorHealth[sect] * 25.5, Engine::smootherDeltaTime * 3);
	animatedSprite->setTint(sf::Color(255, 0, 0, atmoColors[sect][3]));
}