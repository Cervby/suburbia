#pragma once
#include "stdafx.h"

using namespace GameEngine;

class MainMenu : public Master
{
private:
	Vector2 LastSelection;

	virtual void update() override;

public:
	MainMenu(std::string lastMasterName);
	virtual ~MainMenu();
};
