#pragma once 
#include "stdafx.h"

using namespace GameEngine;

struct WaveStep;

class Intro : public Master
{
	int textIterator;

	virtual void update() override;

public:
	static std::vector<WaveStep> listOfTheIntroText;

	Intro(std::string lastMasterName);
	~Intro();
};
