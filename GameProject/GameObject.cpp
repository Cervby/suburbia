#include "stdafx.h"

namespace GameEngine
{
	std::vector<GameObject*> GameObject::objectList;
	std::vector<GameObject*> GameObject::addObjectList;
	std::vector<GameObject*> GameObject::pauseObjects;

	GameObject* GameObject::find(std::string name)
	{
		std::vector<GameObject*>::iterator result = std::find_if(objectList.begin(), objectList.end(), [&](GameObject* gameObject){ return gameObject->name == name; });
		if (result == objectList.end())
		{
			return nullptr;
		}
		return *result;

	}

	GameObject::GameObject(std::string name, DisplayPosition displayPosition)
	{
		this->name = name;
		isToBeDestroyed = false;
		this->displayPosition = displayPosition;

		addObjectList.push_back(this);
		animatedSprite = nullptr;
		collider = nullptr;

		position = Vector2();
		scale = Vector2(1, 1);
	}

	void GameObject::addSprite()
	{ 
		if (animatedSprite == nullptr)
		{
			animatedSprite = new AnimatedSprite(this);
		}
	}
	
	void GameObject::addCollider()
	{
		if (collider == nullptr)
		{
			collider = new Collider(this);
		}
	}

	void GameObject::destroy()
	{
		isToBeDestroyed = true;
	}

	void GameObject::updateGameObjects()
	{
		std::string masterName = Master::master->name;
		for (GameObject* gameObject : objectList)
		{
			if (gameObject->isPaused == false)
			{
				gameObject->update();
				if (masterName != Master::master->name)
				{
					break;
				}

			}
		}

		bool done = false;
		while (!done)
		{
			done = true;
			std::vector<GameObject*>::iterator it = objectList.begin();
			while (it != objectList.end())
			{
				if ((*it)->isToBeDestroyed)
				{
					delete *it;
					objectList.erase(it);
					done = false;
					break;
				}
				else
				{
					it++;
				}
			}
		}

		while (addObjectList.begin() != addObjectList.end())
		{
			objectList.push_back(addObjectList.back());
			addObjectList.pop_back();
		}
	}

	GameObject::~GameObject()
	{
		if (animatedSprite != nullptr)
		{
			delete animatedSprite;
			animatedSprite = nullptr;
		}
		if (collider != nullptr)
		{
			delete collider;
			collider = nullptr;
		}
	}

	void GameObject::quit()
	{
		while (objectList.size() != 0)
		{
			delete objectList.back();
		}
	}

	void GameObject::setPosition(Vector2& pos)
	{
		position = pos;
		
	}

	Vector2 GameObject::getPosition()
	{
		return position;
	}

	void GameObject::setScale(Vector2& scale)
	{
		this->scale = scale;
	}

	Vector2 GameObject::getScale()
	{
		return scale;
	}

	void GameObject::setRotation(const double rot)
	{
		rotation = rot;
	}

	void GameObject::translate(Vector2& relativePos)
	{
		if (collider != nullptr)
		{
			if (collider->bNoCollision == false)
			{
				_revertBackPosition = position;
			}
		}
		setPosition(position + relativePos);
	}

	void GameObject::moveTo(Vector2& toPos, double speed)
	{
		translate((toPos - position).normalize() * speed);
	}

	void GameObject::moveDir(Vector2& toDir, double speed)
	{
		translate(toDir.normalize() * speed);
	}

	void GameObject::lookAt(Vector2& v2)
	{
		Vector2 delta = { position.x - v2.x, v2.y - position.y };
		setRotation(atan2(delta.x, delta.y) * (180 / M_PI));
		AnimatedSprite* sprite;
		sprite = animatedSprite;
		if (sprite != nullptr)
		{
			sprite->getSfSprite()->setRotation(rotation);
		}
		
	
	}

	void GameObject::pause(std::vector<GameObject*>& gameObjects)
	{
		for (GameObject* gameObject : objectList)
		{
			if (std::find(gameObjects.begin(), gameObjects.end(), gameObject) == gameObjects.end())
			{
				gameObject->isPaused = true;
			}
			else
			{
				gameObject->isPaused = false;
			}
		}
	}

	void GameObject::unpause()
	{
		for (GameObject* gameObject : objectList)
		{
			gameObject->isPaused = false;
		}
	}

	void GameObject::setPositionAround(Vector2& pos, double distance, double angle)
	{
		angle *= (M_PI / 180);
		setPosition(Vector2(pos.x + cos(angle) * distance, pos.y + -sin(angle) * distance));
	}

}
