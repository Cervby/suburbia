#pragma once

#include "stdafx.h"

namespace GameEngine
{
	enum class GUIType
	{
		GUIBOX,
		GUIBUTTON
	};

	struct GUIBox
	{
		Rect rect;
		std::string value;
	};

	struct GUIButton
	{
		Rect rect;
		std::string value;
		Vector2 selectionIndex;
	};

	struct GUIImage
	{
		std::string path;
		Rect rect;
		std::string value;
		bool cropElseZoom;
		Vector2 rectxy;
		bool center;
		double opacity;
		sf::Color color;

		GUIImage(std::string path, Rect& rect, std::string value = "", double opacity = 0xFF, bool center = true, sf::Color& color = sf::Color(255, 255, 255)) : path(path), rect(rect), value(value), cropElseZoom(false), rectxy(Vector2()), opacity(opacity), center(center), color(color) {}
		GUIImage(std::string path, Rect& rect, Vector2& rectxy, std::string value = "", double opacity = 0xFF, sf::Color& color = sf::Color(255, 255, 255) ) : path(path), rect(rect), value(value), cropElseZoom(true), rectxy(rectxy), opacity(opacity), center(false), color(color) {}
		GUIImage(std::string path, Vector2& pos, bool center = false, std::string value = "", double opacity = 0xFF, sf::Color& color =  sf::Color(255, 255, 255)) : path(path), rect(Rect(pos.x, pos.y, 0, 0)), center(center), value(value), cropElseZoom(true), rectxy(Vector2()), opacity(opacity), color(color) {}
	};

	struct GUIFont
	{
		std::string texturePath;
		Vector2 fontSize;
		Vector2 fontPadding;
		std::string layout;
		Vector2 scale;
		Vector2 destinationPadding;
		bool isAllUpper;
	};

	class GUI
	{
	private:
		static sf::Texture boxTexture;
		static sf::Texture buttonTexture;
		static sf::Texture buttonTexturePress;
		static sf::Texture buttonTextureSelect;
		static sf::Texture fontTexture;
		static std::vector<sf::Sprite> sprites;
		static std::unordered_map<std::string, sf::Texture> textures;

		static GUIFont spriteFont;
		static sf::Font* sfFont;
		static std::vector<GUIBox> boxes;
		static std::vector<GUIButton> buttons;
		static std::vector<GUIImage> images;
		static std::string bindingSelectUp;
		static std::string bindingSelectDown;
		static std::string bindingSelectLeft;
		static std::string bindingSelectRight;
		static std::string bindingSelectPress;
		static bool bPressOn;
		static void renderFont(Rect& rect, std::string value);
	public:
		static Vector2 selection;
		static bool hovering;
		static bool dontUseMouse;
		static void preloadAssets(std::vector<std::string>& paths);
		static void setButtonBindings(std::string up = "", std::string down = "", std::string left = "", std::string right = "", std::string press = "");
		static void setStyle(std::string boxPath, std::string buttonPath, std::string buttonPressPath, std::string buttonSelectPath, GUIFont& font);
		static void setStyle(std::string boxPath, std::string buttonPath, std::string buttonPressPath, std::string buttonSelectPath, std::string fontPath);
		static void box(GUIBox& item);
		static bool button(GUIButton& item);
		static void image(GUIImage& item);
		static void update();
		static void renderGUI();
		static void clear() { sprites.clear(); };
		static void quit() { if (sfFont != nullptr) delete sfFont; };
	};
}
