#include "stdafx.h"
#include "Turret.h"

using namespace GameEngine;

Turret::Turret() : GameObject("Turret", DisplayPosition::DISPLAYPOSITION_FIXED)
{
	addSprite();
	animatedSprite->setSprite("CannonFront.png");
	animatedSprite->setZIndex(3000);
	pos = Utility::screenToWorld(Vector2(Engine::getLogicalResolution().x / 2, Engine::getLogicalResolution().y + 300));
}

void Turret::update()
{
	GameObject* sectortmp = GameObject::find("Sector Shield: " + std::to_string((int)fmod(static_cast<Camera*>(GameObject::find("Camera"))->toRotation / 90, 4)));
	if (sectortmp != nullptr)
	{
		pos = Vector2::lerp(pos, Vector2(Engine::getLogicalResolution().x / 2, Engine::getLogicalResolution().y + 300), Engine::smootherDeltaTime * 3);
	}
	else
	{
		pos = Vector2::lerp(pos, Vector2(Engine::getLogicalResolution().x / 2, Engine::getLogicalResolution().y - 25), Engine::smootherDeltaTime * 3);
	}
	setPosition(pos);
}