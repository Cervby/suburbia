#pragma once

using namespace GameEngine;

class Camera;

class SlowTime : public GameObject
{
private:
	Vector2 toDir;
	double cooldown;
	double deathCounter;
	double explodeCounter;
	double pitch;
	bool exploding;
	Camera& camera;

	virtual void update() override;

public:
	static double speedFactor;

	SlowTime(Vector2& toPos, Camera& camera);
};
