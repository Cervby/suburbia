#include "stdafx.h"

using namespace GameEngine;

PauseMenu::PauseMenu(std::string lastMasterName, GameData gameData) : Master("Pause Menu"), gameData(gameData)
{
	pause(std::vector<GameObject*>{ this, GameObject::find("Cursor") });
}

void PauseMenu::update()
{
	GUI::image(GUIImage{ "Bakgrund.png", Vector2(0, 0) });

	if (GUI::hovering)
	{
		if (GUI::selection == Vector2(0, 0))
		{
			GUI::image(GUIImage{ "PauseResume.png", Vector2(0, 0) });
		}
		else if (GUI::selection == Vector2(0, 1))
		{
			GUI::image(GUIImage{ "PauseOptions.png", Vector2(0, 0) });
		}
		else if (GUI::selection == Vector2(0, 2))
		{
			GUI::image(GUIImage{ "PauseQuit.png", Vector2(0, 0) });
		}
	}
	else
	{
		GUI::image(GUIImage{ "PauseMenu.png", Vector2(0, 0) });
	}

	Vector2 resD40 = Engine::getLogicalResolution() / 40;
	if (GUI::button(GUIButton{ Rect(resD40.x * 14, resD40.y * 0.6, resD40.x * 12, resD40.y * 6), "", Vector2(0, 0) }))
	{
		unpause();
		Master::master->changeMaster<GameMaster>(gameData);
	}
	if (GUI::button(GUIButton{ Rect(resD40.x * 14, resD40.y * 8.5, resD40.x * 12, resD40.y * 6), "", { 0, 1 } }))
	{
		Master::master->changeMaster<Options>(gameData);
	}
	if (GUI::button(GUIButton{ Rect(resD40.x * 11, resD40.y * 16.5, resD40.x * 18, resD40.y * 6), "", { 0, 2 } }))
	{
		Master::master->changeMaster<MainMenu>();
		for (GameObject* gameObject : GameObject::objectList)
		{
			if (gameObject->name != "Pause Menu" && gameObject->name != "Cursor")
			{
				gameObject->destroy();
			}
		}
	}
	if (InputManager::getQuit() || InputManager::getButtonDown("Exit"))
	{
		unpause();
		Master::master->changeMaster<GameMaster>(gameData);
	}
}