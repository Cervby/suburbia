#include "stdafx.h"
#include "PickUp.h"

using namespace GameEngine;


PickUp::PickUp(Vector2& startPos, PlayerPowerUp powerUp) : GameObject("PickUp")
{
	addSprite();
	addCollider();
	this->powerUp = powerUp;
	time = 0;
	gotten = false;
	gottenCancel = false;
	alpha = 255;
	position = Utility::screenToWorld(startPos);

	if (powerUp == PlayerPowerUp::POWER_UP_BLACK_HOLE)
	{
		animatedSprite->setSprite("PurpleAnimaitonPowerupSpritesheet.png");
		animatedSprite->setAnimation(Vector2(128, 128), 0.2, 1);
	}
	else if (powerUp == PlayerPowerUp::POWER_UP_EMP)
	{
		animatedSprite->setSprite("BlueEMPsprite.png");
		animatedSprite->setAnimation(Vector2(128, 128), 0.2, 1);
	}
	else
	{
		animatedSprite->setSprite("Linus_pickup.png");
	}
	setPosition(startPos);
	collider->set(ColliderType::COLLIDER_CIRCLE);
	collider->bNoCollision = true;
	collider->radius /= 2;
	animatedSprite->setAboveAll(true);


}
void PickUp::update()
{
	setRotation(getRotation() + Engine::smootherDeltaTime * 100);
	if (gottenCancel == true)
	{
		if (alpha <= 0)
		{
			destroy();
		}
		else if (time > 4)
		{
			alpha -= Engine::smootherDeltaTime * 0.4;
			animatedSprite->setTint(sf::Color(255, 255, 255, alpha));
		}
		Vector2 barPos = { 670, 1050 };
		animatedSprite->setTint(sf::Color(255, 255, 255, alpha));
		position = Vector2::lerp(position, barPos, Engine::smootherDeltaTime);
		setPosition(Utility::screenToWorld(position));
	}
	else if (gotten == true)
	{
		alpha = Utility::lerp(alpha, 255, Engine::smootherDeltaTime * 4);
		Vector2 barPos = { 670, 1050 };
		animatedSprite->setTint(sf::Color(255, 255, 255, alpha));
		position = Vector2::lerp(position, barPos, Engine::smootherDeltaTime);
		setPosition(Utility::screenToWorld(position));
		if (InputManager::getButtonDown("Power up"))
		{
			gottenCancel = true;
		}

	}
	else if (collider->isClicked)
	{
		AudioManager::playSound("powerup pickup.wav");
		static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.currentPowerUp = powerUp;
		for (GameObject* obit : GameObject::objectList)
		{
			if (obit->name == "PickUp")
			{
				if (static_cast<PickUp*>(obit)->gotten)
				{
					static_cast<PickUp*>(obit)->gottenCancel = true;
				}
			}
		}
		gotten = true;
		time = Engine::clock.getElapsedTime().asSeconds();
	}
	else if (alpha <= 0)
	{
		destroy();
	}
	else if (time > 4)
	{
		alpha -= Engine::smootherDeltaTime;
		animatedSprite->setTint(sf::Color(255, 255, 255, alpha));
	}
	time += Engine::smootherDeltaTime;
};
