#pragma once

#include "stdafx.h"

using namespace GameEngine;


class Tutorial : public GameObject
{
private:
	bool fadeOut;
	std::string button;
	double alpha;
	bool CheckPowerup;
	virtual void update() override;

public:
	Tutorial(std::string button);
};
