#pragma once

#include "stdafx.h"

using namespace GameEngine;

class SectorShield : public GameObject
{
public:
	double deathCounter;

	SectorShield(int num, Vector2& planetPos);
};
