#pragma once

#include "stdafx.h"

using namespace GameEngine;

class BlackHole : public GameObject
{
private:
	Vector2 toDir;
	double cooldown;
	double deathCounter;
	double explodeCounter;
	bool exploding;
	bool explodingTwo;

	virtual void update() override;

public:
	BlackHole(Vector2& toPos);
};
