#pragma once
#include "stdafx.h"
#include "GameMaster.h"

using namespace GameEngine;

class PauseMenu : public Master
{
	GameData gameData;

	virtual void update() override;

public:

	PauseMenu(std::string lastMasterName, GameData gameData);
};
