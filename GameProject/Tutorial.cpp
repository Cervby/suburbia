#include "stdafx.h"
#include "Tutorial.h"

using namespace GameEngine;

Tutorial::Tutorial(std::string button) : GameObject("Tutorial: " + button)
{
	CheckPowerup = false;
	fadeOut = false;
	this->button = button;
	double alpha = 0;
}

void Tutorial::update()
{
	if (Master::master->name != "Game Master")
	{
		return;
	}

	if (InputManager::getButtonDown(button))
	{
		fadeOut = true;
	}

	if ((button == "Combo" && static_cast<ComboMeter*>(GameObject::find("Combo Meter"))->comboCountShow + 127 + 120 < 512) == false || button != "Combo")
	{
		if (fadeOut)
		{
			alpha -= Engine::smootherDeltaTime * 500;
			if (alpha < 0)
			{
				alpha = 0;
				destroy();

			}
		}
		else
		{
			alpha += Engine::smootherDeltaTime * 200;
			if (alpha > 255)
			{
				alpha = 255;
			}
		}

	}

	

	if (button == "Fire")
	{
		GUI::image(GUIImage{ "FirePower.png", Rect(Engine::getLogicalResolution().x / 2, (Engine::getLogicalResolution().y/ 5) * 2 - 64, 0, 0), "", alpha });
	}
	else if (button == "Combo")
	{
			
		GUI::image(GUIImage{ "ComboPower.png", Rect(Engine::getLogicalResolution().x / 2, (Engine::getLogicalResolution().y / 5) * 2 - 64, 0, 0), "", alpha });
	}
	else if (button == "Power up")
	{
		if (GameObject::find("PickUp") == nullptr && CheckPowerup == false){
			new PickUp(Utility::screenToWorld(Vector2(Engine::getLogicalResolution().x / 2 - 100, Engine::getLogicalResolution().y / 2)), PlayerPowerUp::POWER_UP_EMP);
			new PickUp(Utility::screenToWorld(Vector2(Engine::getLogicalResolution().x / 2 + 100, Engine::getLogicalResolution().y / 2)), PlayerPowerUp::POWER_UP_BLACK_HOLE);
			CheckPowerup = true;
		}
		if (CheckPowerup == true)
		{
			fadeOut = false;
		}
		GameMaster* gmaster = static_cast<GameMaster*>(Master::master);
		if (gmaster->gameData.currentPowerUp == PlayerPowerUp::POWER_UP_EMP) {
			GUI::image(GUIImage{ "PowerUp.png", Rect(Engine::getLogicalResolution().x / 2, (Engine::getLogicalResolution().y / 3) * 2 - 64, 0, 0), "Spacebar", alpha });
		}
		if (static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.currentPowerUp == PlayerPowerUp::POWER_UP_BLACK_HOLE) {
			GUI::image(GUIImage{ "PowerUp.png", Rect(Engine::getLogicalResolution().x / 2, (Engine::getLogicalResolution().y / 3) * 2 - 64, 0, 0), "Spacebar", alpha });
		}

	}

	else if (button == "Left")
	{
		GUI::image(GUIImage{ "PlanetRoationButton.png", Rect(Engine::getLogicalResolution().x / 3, (Engine::getLogicalResolution().y / 5 - 64) * 3, 0, 0), "A", alpha });
	}
	else if (button == "Right")
	{
		GUI::image(GUIImage{ "PlanetRoationButton.png", Rect((Engine::getLogicalResolution().x / 3) * 2, (Engine::getLogicalResolution().y / 5 - 64) * 3, 0, 0), "D", alpha });
	}

	if (button == "Right" && GameObject::find("Tutorial: Fire") != nullptr && GameObject::find("Tutorial: Combo") != nullptr && GameObject::find("Tutorial: Power up") != nullptr && GameObject::find("Tutorial: Left") != nullptr && GameObject::find("Tutorial: Right") != nullptr)
	{
		static_cast<GameMaster*>(GameObject::find("Game Master"))->gameData.waveIterator--;
	}
}